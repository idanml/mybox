package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.Toolkit;

import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.ImageIcon;

import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.ActionListener;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.JList;

public class ManageFoldersGUI extends JFrame {

	private JPanel contentPane;
	private JButton btnHome;
	private JButton btnManageFiles;
	private JButton btnManageGroups;
	private JButton btnRenameFolder;
	private JButton btnAddNewFolder;
	private JButton btnDeleteFolder;
	private JButton btnChangeFolderLocation;
	private JList folderList;
	


	/**
	 * Create the frame. The Main Manage Folders Windows.
	 */
	public ManageFoldersGUI(Object F) {
		setResizable(false);
		setTitle("MyBox");
		setIconImage(Toolkit.getDefaultToolkit().getImage(ManageFoldersGUI.class.getResource("/images/logo2.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("New label");
		label.setBounds(0, 0, 0, 0);
		contentPane.add(label);
		
		JLabel lblLogo = new JLabel("Logo");
		lblLogo.setIcon(new ImageIcon(ManageFoldersGUI.class.getResource("/images/bar.png")));
		lblLogo.setBounds(0, 0, 794, 79);
		contentPane.add(lblLogo);
		
		btnHome = new JButton("Home");
		btnHome.setIcon(new ImageIcon(ManageFoldersGUI.class.getResource("/images/School_Building_with_Flag_32 (1).png")));
		btnHome.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnHome.setBounds(10, 81, 252, 79);
		contentPane.add(btnHome);
		
		btnManageFiles = new JButton("Manage Files");
		btnManageFiles.setForeground(SystemColor.control);
		btnManageFiles.setBackground(SystemColor.activeCaptionBorder);
		btnManageFiles.setIcon(new ImageIcon(ManageFoldersGUI.class.getResource("/images/Three_Document_Folders_32 (1).png")));
		btnManageFiles.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnManageFiles.setBounds(268, 81, 252, 79);
		contentPane.add(btnManageFiles);
		
		btnManageGroups = new JButton("Manage Groups");
		btnManageGroups.setIcon(new ImageIcon(ManageFoldersGUI.class.getResource("/images/College_Classroom_32.png")));
		btnManageGroups.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnManageGroups.setBounds(528, 81, 252, 79);
		contentPane.add(btnManageGroups);
		
		btnRenameFolder = new JButton("Rename Folder");
		btnRenameFolder.setIcon(new ImageIcon(ManageFoldersGUI.class.getResource("/images/Inclined_Pencil_with_Eraser_24.png")));
		btnRenameFolder.setBounds(10, 500, 360, 52);
		contentPane.add(btnRenameFolder);
		
		btnAddNewFolder = new JButton("Add New Folder");
		btnAddNewFolder.setIcon(new ImageIcon(ManageFoldersGUI.class.getResource("/images/Folder_with_Plus_Symbol_24.png")));
		btnAddNewFolder.setBounds(10, 435, 360, 52);
		contentPane.add(btnAddNewFolder);
		
		btnDeleteFolder = new JButton("Delete Folder");
		btnDeleteFolder.setIcon(new ImageIcon(ManageFoldersGUI.class.getResource("/images/Folder_with_Minus_Sign_24.png")));
		btnDeleteFolder.setBounds(420, 435, 360, 52);
		contentPane.add(btnDeleteFolder);
		
		btnChangeFolderLocation = new JButton("Change Folder Location");
		btnChangeFolderLocation.setIcon(new ImageIcon(ManageFoldersGUI.class.getResource("/images/Library_Bookcase_24.png")));
		btnChangeFolderLocation.setBounds(420, 500, 360, 52);
		contentPane.add(btnChangeFolderLocation);
		
		ArrayList<String> foldersss = new ArrayList<String>(Arrays.asList(((String) F).split(" ")));
		String folderss[] = new String[foldersss.size()];
		for (int i = 1; i < foldersss.size(); i++)
			folderss[i]=foldersss.get(i);
		
		folderList = new JList(folderss);
		folderList.setBounds(20, 203, 749, 219);
		contentPane.add(folderList);
		

		JLabel lblFolders = new JLabel("Folders:");
		lblFolders.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblFolders.setBounds(20, 173, 56, 16);
		contentPane.add(lblFolders);
		setLocationRelativeTo(null);
		setVisible(true);
	}
	
	// Action Listeners
	


	public void addbtnHomeActionListener(ActionListener e)
	{
		btnHome.addActionListener(e);
	}
	
	public void addbtnManageFilesActionListener(ActionListener e)
	{
		btnManageFiles.addActionListener(e);
	}
	
	public void addbtnManageGroupsActionListener(ActionListener e)
	{
		btnManageGroups.addActionListener(e);
	}
	
	public void addbtnRenameFolderActionListener(ActionListener e)
	{
		btnRenameFolder.addActionListener(e);
	}
	
	public void addbtnAddNewFolderActionListener(ActionListener e)
	{
		btnAddNewFolder.addActionListener(e);
	}
	
	public void addbtnChangeFolderLocationActionListener(ActionListener e)
	{
		btnChangeFolderLocation.addActionListener(e);
	}
	
	public void addbtnbtnDeleteFolderActionListener(ActionListener e)
	{
		btnDeleteFolder.addActionListener(e);
	}
	
	public JList getFolderList() {
		return folderList;
	}

	public void setFolderList(JList fileList) {
		this.folderList = fileList;
	}
	
	public String getSelectedFolder() {
		return folderList.getSelectedValue().toString();
	}
	
	public boolean isFolderFieldEmpty() {
		return (folderList.isSelectionEmpty());

	}
	
	
	
}
