package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.Toolkit;

import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.ActionListener;
import java.awt.Color;

import javax.swing.JTextField;

import java.awt.FlowLayout;
import java.awt.List;
import java.awt.Checkbox;
import java.awt.Choice;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.JList;

public class WatchApproachableFileListGUI extends JFrame {

	private JPanel contentPane;
	private JButton btnHome;
	private JButton btnManageFiles;
	private JButton btnManageGroups;
	private JButton btnUpdateFile;
	private JButton btnCancel;
	private JLabel lblFiles;
	private JButton btnReadFile;
	private JButton btnAddToPersonal;
	private JList list;
	
	/**
	 * Create the frame. In this window User Can see his approachable files.
	 */
	public WatchApproachableFileListGUI(Object F) {
		setResizable(false);
		setTitle("MyBox");
		setIconImage(Toolkit.getDefaultToolkit().getImage(WatchApproachableFileListGUI.class.getResource("/images/logo2.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("New label");
		label.setBounds(0, 0, 0, 0);
		contentPane.add(label);
		
		JLabel lblLogo = new JLabel("Logo");
		lblLogo.setIcon(new ImageIcon(WatchApproachableFileListGUI.class.getResource("/images/bar.png")));
		lblLogo.setBounds(0, 0, 794, 79);
		contentPane.add(lblLogo);
		
		btnHome = new JButton("Home");
		btnHome.setIcon(new ImageIcon(WatchApproachableFileListGUI.class.getResource("/images/School_Building_with_Flag_32 (1).png")));
		btnHome.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnHome.setBounds(10, 81, 252, 79);
		contentPane.add(btnHome);
		
		btnManageFiles = new JButton("Manage Files");
		btnManageFiles.setForeground(SystemColor.control);
		btnManageFiles.setBackground(SystemColor.activeCaptionBorder);
		btnManageFiles.setIcon(new ImageIcon(WatchApproachableFileListGUI.class.getResource("/images/Three_Document_Folders_32 (1).png")));
		btnManageFiles.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnManageFiles.setBounds(268, 81, 252, 79);
		contentPane.add(btnManageFiles);
		
		btnManageGroups = new JButton("Manage Groups");
		btnManageGroups.setIcon(new ImageIcon(WatchApproachableFileListGUI.class.getResource("/images/College_Classroom_32.png")));
		btnManageGroups.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnManageGroups.setBounds(528, 81, 252, 79);
		contentPane.add(btnManageGroups);
		
		btnUpdateFile = new JButton("Update File");
		btnUpdateFile.setIcon(new ImageIcon(WatchApproachableFileListGUI.class.getResource("/images/Pen_tip_24.png")));
		btnUpdateFile.setBounds(302, 430, 146, 33);
		contentPane.add(btnUpdateFile);
		
		btnCancel = new JButton("Cancel");
		btnCancel.setIcon(new ImageIcon(WatchApproachableFileListGUI.class.getResource("/images/cancel.PNG")));
		btnCancel.setBounds(476, 430, 146, 33);
		contentPane.add(btnCancel);
		
		lblFiles = new JLabel("Approachable Files:");
		lblFiles.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblFiles.setBounds(63, 173, 183, 16);
		contentPane.add(lblFiles);
		
		btnReadFile = new JButton("Read File");
		btnReadFile.setIcon(new ImageIcon(WatchApproachableFileListGUI.class.getResource("/images/Exam_with_Three_Questions_24.png")));
		btnReadFile.setBounds(132, 430, 146, 33);
		contentPane.add(btnReadFile);
		
		
		
		ArrayList<String> AppFiles = new ArrayList<String>(Arrays.asList(((String) F).split("@@")));
		AppFiles.remove(0);

       
		list = new JList(AppFiles.toArray());
		list.setBounds(63, 198, 664, 228);
		contentPane.add(list);
	
		
		btnAddToPersonal = new JButton("Add To Personal Box");
		btnAddToPersonal.setIcon(new ImageIcon(WatchApproachableFileListGUI.class.getResource("/images/Worker_loading_boxes_24.png")));
		btnAddToPersonal.setBounds(132, 476, 490, 33);
		contentPane.add(btnAddToPersonal);
		setLocationRelativeTo(null);
		setVisible(true);
	}
	
	// Action Listeners
	
	public void addbtnHomeActionListener(ActionListener e)
	{
		btnHome.addActionListener(e);
	}
	
	public void addbtnManageFilesActionListener(ActionListener e)
	{
		btnManageFiles.addActionListener(e);
	}
	
	public void addbtnManageGroupsActionListener(ActionListener e)
	{
		btnManageGroups.addActionListener(e);
	}
	
	public void addbtnUpdateFileActionListener(ActionListener e)
	{
		btnUpdateFile.addActionListener(e);
	}
	
	public void addbtnCancelActionListener(ActionListener e)
	{
		btnCancel.addActionListener(e);
	}
	
	public void addbtnReadFileActionListener(ActionListener e)
	{
		btnReadFile.addActionListener(e);
	}
	
	public void addbtnAddToPersonalActionListener(ActionListener e)
	{
		btnAddToPersonal.addActionListener(e);
	}
	
	// Getters

	public JList getList() {
		return list;
	}

	public void setList(JList list) {
		this.list = list;
	}

	public String getFileField() {
		return (list.getSelectedValue()).toString();

	}
	
	
	public boolean isFileFieldEmpty() {
		return (list.isSelectionEmpty());

	}
	
	
	
}
