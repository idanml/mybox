package view;

import javafx.scene.input.MouseEvent;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JSpinner;
import javax.swing.JScrollBar;

import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.ImageIcon;

import models.LoginMod;
import controllers.LoginController;

public class ExplorerGUI extends JPanel {

	private MouseEvent event;
	private Object RootfoldersNames;
	private Object RootfilesNames;
	private JButton folderButtons[];
	private JButton fileButtons[];
	private int nFolders;
	private int nFiles;
	
	
	
	/**
	 * Create the panel. The Main Explorer Window.
	 */
	public ExplorerGUI(Object Fi,Object Fo) {
		

		if(Fi==(null) && Fo==(null))	
			GUIroot();
		else
			GUInotRoot(Fo,Fi);

	}

	public void GUIroot(){
		
		
		
		
	RootfoldersNames = LoginController.getFilesInRoot();
	RootfilesNames = LoginController.getFoldersInRoot();
	ArrayList<String> foNames = new ArrayList<String>(Arrays.asList(((String) RootfoldersNames).split(" ")));
	ArrayList<String> fiNames = new ArrayList<String>(Arrays.asList(((String) RootfilesNames).split(" ")));
	nFolders = foNames.size(); // Save Sizes
	nFiles = fiNames.size();


	 
	setLayout(new FlowLayout(FlowLayout.LEFT, 1, 1));
	folderButtons = new JButton[foNames.size()];		// folder Buttons Array 
	fileButtons = new JButton[fiNames.size()];		// file Buttons Array 
	
	for (int i=0 ; i<foNames.size() ; i++) {	// Adding all the folders
	folderButtons[i] = new JButton(foNames.get(i));
	folderButtons[i].setIcon(new ImageIcon(ExplorerGUI.class.getResource("/images/text-file-icon.png")));
	folderButtons[i].setBorderPainted(false);
	folderButtons[i].setOpaque(false);
	folderButtons[i].setContentAreaFilled(false);
	folderButtons[i].setBorderPainted(false);
	add(folderButtons[i]);
	}
	
	for (int i=0 ; i<fiNames.size() ; i++) {	// Adding all the folders   
	fileButtons[i] = new JButton(fiNames.get(i));
	fileButtons[i].setIcon(new ImageIcon(ExplorerGUI.class.getResource("/images/folder-logo.png")));
	fileButtons[i].setBorderPainted(false);
	fileButtons[i].setOpaque(false);
	fileButtons[i].setContentAreaFilled(false);
	fileButtons[i].setBorderPainted(false);
	add(fileButtons[i]);
	}
}
	
	
	
	public void GUInotRoot(Object Fo,Object Fi){
	
		ArrayList<String> foNames = new ArrayList<String>(Arrays.asList(((String) Fo).split(" ")));
		ArrayList<String> fiNames = new ArrayList<String>(Arrays.asList(((String) Fi).split(" ")));
	
	nFolders = foNames.size(); // Save Sizes
	nFiles = fiNames.size();
	
	setLayout(new FlowLayout(FlowLayout.LEFT, 1, 1));
	
	folderButtons = new JButton[foNames.size()];		// folder Buttons Array 

	fileButtons = new JButton[fiNames.size()];		// file Buttons Array 

		
	for (int i=0 ; i<foNames.size() ; i++) {	// Adding all the folders
	folderButtons[i] = new JButton(foNames.get(i));
	folderButtons[i].setIcon(new ImageIcon(ExplorerGUI.class.getResource("/images/text-file-icon.png")));
	folderButtons[i].setBorderPainted(false);
	folderButtons[i].setOpaque(false);
	folderButtons[i].setContentAreaFilled(false);
	folderButtons[i].setBorderPainted(false);
	add(folderButtons[i]);
	}

	for (int i=0 ; i<fiNames.size() ; i++) {	// Adding all the folders   
	fileButtons[i] = new JButton(fiNames.get(i));
	fileButtons[i].setIcon(new ImageIcon(ExplorerGUI.class.getResource("/images/folder-logo.png")));
	fileButtons[i].setBorderPainted(false);
	fileButtons[i].setOpaque(false);
	fileButtons[i].setContentAreaFilled(false);
	fileButtons[i].setBorderPainted(false);
	add(fileButtons[i]);
	}
}
	
public void addFolderButtonsActionListener(ActionListener e)
	{
		
	for (int i=0 ; i<nFolders ; i++) 
		folderButtons[i].addActionListener(e);
	

	}
	
public void addFilesButtonsActionListener(ActionListener e)
{

	  for (int i=0 ; i<nFiles ; i++) 
	fileButtons[i].addActionListener(e);



}
	

	
	
		
	
}
