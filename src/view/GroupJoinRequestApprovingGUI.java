package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.Toolkit;

import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.ListModel;

import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.ActionListener;
import java.awt.Color;

import javax.swing.JTextField;
import javax.swing.JList;
import javax.swing.JCheckBox;

import models.Request;

import java.awt.Choice;
import java.util.ArrayList;
import java.util.Arrays;

public class GroupJoinRequestApprovingGUI extends JFrame {

	private JPanel contentPane;
	private JButton btnManageFiles;
	private JButton btnManageGroups;
	private JButton btnApprove;
	private JButton btnCancel;
	private Choice requests;
	private JButton btnReject;
	private JList list;
	private DefaultListModel model;
	
	/**
	 * Launch the application. In this window admin Approve/Reject users request to join/leave groups.
	 */
	/*public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GroupJoinRequestApprovingGUI frame = new GroupJoinRequestApprovingGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the frame.
	 */
	public GroupJoinRequestApprovingGUI(Object F) {
		setResizable(false);
		setTitle("MyBox");
		setIconImage(Toolkit.getDefaultToolkit().getImage(GroupJoinRequestApprovingGUI.class.getResource("/images/logo2.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("New label");
		label.setBounds(0, 0, 0, 0);
		contentPane.add(label);
		
		JLabel lblLogo = new JLabel("Logo");
		lblLogo.setIcon(new ImageIcon(GroupJoinRequestApprovingGUI.class.getResource("/images/ba2r.png")));
		lblLogo.setBounds(0, 0, 794, 79);
		contentPane.add(lblLogo);
		
		btnManageFiles = new JButton("Manage Files");
		btnManageFiles.setIcon(new ImageIcon(ManageGroupsGUI.class.getResource("/images/Three_Document_Folders_32.png")));
		btnManageFiles.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnManageFiles.setBounds(10, 81, 365, 79);
		contentPane.add(btnManageFiles);
		
		btnManageGroups = new JButton("Manage Groups");
		btnManageGroups.setForeground(SystemColor.control);
		btnManageGroups.setBackground(SystemColor.activeCaptionBorder);
		btnManageGroups.setIcon(new ImageIcon(ManageGroupsGUI.class.getResource("/images/College_Classroom_32 (1).png")));
		btnManageGroups.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnManageGroups.setBounds(417, 81, 365, 79);
		contentPane.add(btnManageGroups);
		
		btnApprove = new JButton("Approve");
		btnApprove.setIcon(new ImageIcon(GroupJoinRequestApprovingGUI.class.getResource("/images/Add_Friend_24.png")));
		btnApprove.setBounds(151, 429, 168, 33);
		contentPane.add(btnApprove);
		
		btnCancel = new JButton("Cancel");
		btnCancel.setIcon(new ImageIcon(GroupJoinRequestApprovingGUI.class.getResource("/images/cancel.PNG")));
		btnCancel.setBounds(511, 429, 168, 33);
		contentPane.add(btnCancel);
		
		


		
		
		
	/*	requests = new Choice();
		requests.setBounds(306, 262, 250, 22);
		contentPane.add(requests);*/
		String T = null;
	/*	String FF = (String) F;
		
		 String [] arr = FF.split(" ", 2);

		 String FFF = arr[1] ;
		
		
		ArrayList<String> Requests = new ArrayList<String>(Arrays.asList(((String) FFF).split("@@")));*/
		
		ArrayList<Request> req = new ArrayList<Request>();
		req=(ArrayList<Request>) F;

		
		list = new JList();
		model = new DefaultListModel();
		list.setModel(model);
		for (int i = 0; i < req.size(); i++) {
			if( req.get(i).getRequestType()==1)
				 T="Join";
			else
				 T="Leave";
			//requests.add("Group : "+ req.get(i).getGroupName()+ "  -  Type : "+ T + "   -  User : " + req.get(i).getUserName());}
			 model.addElement("Group : "+ req.get(i).getGroupName()+ "  -  Type : "+ T + "  -  User : " + req.get(i).getUserName());}
		
		
		
	
		list.setBounds(63, 198, 664, 228);
		contentPane.add(list);
		
		
		
		JLabel lblRequests = new JLabel("Requests:");
		lblRequests.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblRequests.setBounds(223, 262, 106, 16);
		contentPane.add(lblRequests);
		
		btnReject = new JButton("Reject");
		btnReject.setIcon(new ImageIcon(GroupJoinRequestApprovingGUI.class.getResource("/images/Delete_full_goup_24.png")));
		btnReject.setBounds(331, 429, 168, 33);
		contentPane.add(btnReject);
		setLocationRelativeTo(null);
		setVisible(true);
	}
	
	// Action Listeners
	
	
	public void addbtnManageFilesActionListener(ActionListener e)
	{
		btnManageFiles.addActionListener(e);
	}
	

	public void addbtnManageGroupsActionListener(ActionListener e)
	{
		btnManageGroups.addActionListener(e);
	}
	
	public void addbtnAprroveActionListener(ActionListener e)
	{
		btnApprove.addActionListener(e);
	}
	
	public void addbtnRejectActionListener(ActionListener e)
	{
		btnReject.addActionListener(e);
	}
	
	public void addbtnCancelActionListener(ActionListener e)
	{
		btnCancel.addActionListener(e);
	}

	
	// Getters

	public Choice getRequests() {
		return requests;
	}

	public void setRequests(Choice requests) {
		this.requests = requests;
	}
	
	
	public String getRequestField() {
		return (list.getSelectedValue()).toString();

	}
	
	
	public boolean isFileFieldEmpty() {
		return (list.isSelectionEmpty());

	}
	
	
	
	
}
