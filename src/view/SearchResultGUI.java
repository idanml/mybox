package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.Toolkit;

import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import javax.swing.JList;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.ActionListener;
import java.awt.Color;

import javax.swing.JTextField;

import java.awt.List;
import java.awt.Checkbox;
import java.awt.Choice;
import java.util.ArrayList;
import java.util.Arrays;

public class SearchResultGUI extends JFrame {

	private JPanel contentPane;
	private JButton btnHome;
	private JButton btnManageFiles;
	private JButton btnManageGroups;
	private JButton btnUpdateFile;
	private JButton btnCancel;
	private JLabel lblFiles;
	private Choice files;
	private JButton btnReadFile;
	private JList list;
	
	/**
	 * Create the frame. This window shows the search result.
	 */
	public SearchResultGUI(Object F) {
		setResizable(false);
		setTitle("MyBox");
		setIconImage(Toolkit.getDefaultToolkit().getImage(SearchResultGUI.class.getResource("/images/logo2.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("New label");
		label.setBounds(0, 0, 0, 0);
		contentPane.add(label);
		
		JLabel lblLogo = new JLabel("Logo");
		lblLogo.setIcon(new ImageIcon(SearchResultGUI.class.getResource("/images/bar.png")));
		lblLogo.setBounds(0, 0, 794, 79);
		contentPane.add(lblLogo);
		
		btnHome = new JButton("Home");
		btnHome.setIcon(new ImageIcon(SearchResultGUI.class.getResource("/images/School_Building_with_Flag_32 (1).png")));
		btnHome.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnHome.setBounds(10, 81, 252, 79);
		contentPane.add(btnHome);
		
		btnManageFiles = new JButton("Manage Files");
		btnManageFiles.setForeground(SystemColor.control);
		btnManageFiles.setBackground(SystemColor.activeCaptionBorder);
		btnManageFiles.setIcon(new ImageIcon(SearchResultGUI.class.getResource("/images/Three_Document_Folders_32 (1).png")));
		btnManageFiles.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnManageFiles.setBounds(268, 81, 252, 79);
		contentPane.add(btnManageFiles);
		
		btnManageGroups = new JButton("Manage Groups");
		btnManageGroups.setIcon(new ImageIcon(SearchResultGUI.class.getResource("/images/College_Classroom_32.png")));
		btnManageGroups.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnManageGroups.setBounds(528, 81, 252, 79);
		contentPane.add(btnManageGroups);
		
		btnUpdateFile = new JButton("Update File");
		btnUpdateFile.setIcon(new ImageIcon(SearchResultGUI.class.getResource("/images/Pen_tip_24.png")));
		btnUpdateFile.setBounds(302, 430, 146, 33);
		contentPane.add(btnUpdateFile);
		
		btnCancel = new JButton("Cancel");
		btnCancel.setIcon(new ImageIcon(SearchResultGUI.class.getResource("/images/cancel.PNG")));
		btnCancel.setBounds(476, 430, 146, 33);
		contentPane.add(btnCancel);
		
		
		lblFiles = new JLabel("Files Found:");
		lblFiles.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblFiles.setBounds(63, 173, 183, 16);
		contentPane.add(lblFiles);
		
		
		/*lblFiles = new JLabel("Files Match Your Search:");
		lblFiles.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblFiles.setBounds(303, 254, 183, 16);
		contentPane.add(lblFiles);*/
		
		/*files = new Choice();
		files.setBounds(268, 276, 220, 79);
		contentPane.add(files);*/
		
		
		
		ArrayList<String> SeFiles = new ArrayList<String>(Arrays.asList(((String) F).split(" ")));
		SeFiles.remove(0);
		if(SeFiles.isEmpty()==true)
			SeFiles.add("No Files Were Found");
	
		
		list = new JList(SeFiles.toArray());
		list.setBounds(63, 198, 664, 228);
		contentPane.add(list);
	
		
		
		btnReadFile = new JButton("Read File");
		btnReadFile.setIcon(new ImageIcon(SearchResultGUI.class.getResource("/images/Exam_with_Three_Questions_24.png")));
		btnReadFile.setBounds(132, 430, 146, 33);
		contentPane.add(btnReadFile);
		setLocationRelativeTo(null);
		setVisible(true);
	}
	
	// Action Listeners
	
	public void addbtnHomeActionListener(ActionListener e)
	{
		btnHome.addActionListener(e);
	}
	
	public void addbtnManageFilesActionListener(ActionListener e)
	{
		btnManageFiles.addActionListener(e);
	}
	
	public void addbtnManageGroupsActionListener(ActionListener e)
	{
		btnManageGroups.addActionListener(e);
	}
	
	public void addbtnUpdateFileActionListener(ActionListener e)
	{
		btnUpdateFile.addActionListener(e);
	}
	
	public void addbtnCancelActionListener(ActionListener e)
	{
		btnCancel.addActionListener(e);
	}
	
	public void addbtnReadFileActionListener(ActionListener e)
	{
		btnReadFile.addActionListener(e);
	}


	
	// Getters
	
	public Choice getFiles() {
		return files;
	}

	public void setFiles(Choice files) {
		this.files = files;
	}
	
	
	
	public String getFileField() {
		return (list.getSelectedValue()).toString();

	}
	
	
	public boolean isFileFieldEmpty() {
		return (list.isSelectionEmpty());

	}
	
	
	
	
	
	
	
	
	
}
