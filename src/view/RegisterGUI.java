package view;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionListener;

public class RegisterGUI extends JFrame{
		
		
		private JPanel contentPane;
		private JTextField userField;
		private JTextField passwordField;
		private JTextField FnameField;
		private JTextField LnameField;
		private JTextField emailField;
		private JButton btnRegister;
		
		/**
		 * Create the frame. Registration Window.
		 */
	
	
	public RegisterGUI(){
		
		setTitle("Register");
		setIconImage(Toolkit.getDefaultToolkit().getImage(RegisterGUI.class.getResource("/images/logo2.png")));
		
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 400, 360);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		
		JLabel lblUsername = new JLabel("Username :");
		lblUsername.setFont(new Font("Arial", Font.BOLD, 16));
		lblUsername.setBounds(40,20, 100, 16);
		contentPane.add(lblUsername);
		
		JLabel lblPassword = new JLabel("Password :");
		lblPassword.setFont(new Font("Arial", Font.BOLD, 16));
		lblPassword.setBounds(40, 60, 87, 16);
		contentPane.add(lblPassword);
		
		JLabel lblFname = new JLabel("First Name :");
		lblFname.setFont(new Font("Arial", Font.BOLD, 16));
		lblFname.setBounds(40, 100, 100, 16);
		contentPane.add(lblFname);
		
		
		JLabel lblLname = new JLabel("Last Name :");
		lblLname.setFont(new Font("Arial", Font.BOLD, 16));
		lblLname.setBounds(40, 140, 128, 16);
		contentPane.add(lblLname);
		
		JLabel lblEmail = new JLabel("Email :");
		lblEmail.setFont(new Font("Arial", Font.BOLD, 16));
		lblEmail.setBounds(40, 180, 87, 16);
		contentPane.add(lblEmail);
		
		
		userField = new JTextField();
		userField.setBounds(180, 20, 139, 22);
		contentPane.add(userField);
		userField.setColumns(10);
		
		passwordField = new JTextField();
		passwordField.setColumns(10);
		passwordField.setBounds(180, 60, 139, 22);
		contentPane.add(passwordField);
		
		FnameField = new JTextField();
		FnameField.setColumns(10);
		FnameField.setBounds(180, 100, 139, 22);
		contentPane.add(FnameField);

		LnameField = new JTextField();
		LnameField.setColumns(10);
		LnameField.setBounds(180, 140, 139, 22);
		contentPane.add(LnameField);
		
		emailField = new JTextField();
		emailField.setColumns(10);
		emailField.setBounds(180, 180, 139, 22);
		contentPane.add(emailField);
		
		JLabel label = new JLabel("New label");
		label.setBounds(120,120, 156, 150);
		label.setIcon(new ImageIcon(LoginGUI.class.getResource("/images/logo2.png")));
		contentPane.add(label);
		
		btnRegister = new JButton("Register");
		btnRegister.setIcon(new ImageIcon(LoginGUI.class.getResource("/images/user.PNG")));
		btnRegister.setBounds(140, 260, 120, 25);
		contentPane.add(btnRegister);
		
		setLocationRelativeTo(null);
		setVisible(true);
		
		
	}
	
	
	
	public void addbtnRegisterActionListener(ActionListener e)
	{
		btnRegister.addActionListener(e);
	}
	
	
	
	public String getUserField() {
		return userField.getText();
	}

	public String getPasswordField() {
		return passwordField.getText();
	}
	
	
	public String getFnameField() {
		return FnameField.getText();
	}

	public String getLnameField() {
		return LnameField.getText();
	}
	
	public String getEmailField() {
		return emailField.getText();
	}
	
	
	
	
	
}
	
	
	
	
	
	
	
	
	

