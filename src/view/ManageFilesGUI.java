package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.Toolkit;

import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.ImageIcon;

import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.ActionListener;
import java.awt.Color;
import java.awt.event.ActionEvent;

public class ManageFilesGUI extends JFrame {

	private JPanel contentPane;
	private JButton btnHome;
	private JButton btnManageFiles;
	private JButton btnManageGroups;
	private JButton btnAddFile;
	private JButton btnDeleteFile;
	private JButton btnSearchFiles;
	private JButton btnFileInfoEdit;
	private JButton btnManageFolders;
	private JButton btnWatchApprochableFiles;
	
	/**
	 * Create the frame. The main manage Files Window.
	 */
	public ManageFilesGUI() {
		setResizable(false);
		setTitle("MyBox");
		setIconImage(Toolkit.getDefaultToolkit().getImage(ManageFilesGUI.class.getResource("/images/logo2.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("New label");
		label.setBounds(0, 0, 0, 0);
		contentPane.add(label);
		
		JLabel lblLogo = new JLabel("Logo");
		lblLogo.setIcon(new ImageIcon(ManageFilesGUI.class.getResource("/images/bar.png")));
		lblLogo.setBounds(0, 0, 794, 79);
		contentPane.add(lblLogo);
		
		btnHome = new JButton("Home");
		btnHome.setIcon(new ImageIcon(ManageFilesGUI.class.getResource("/images/School_Building_with_Flag_32 (1).png")));
		btnHome.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnHome.setBounds(10, 81, 252, 79);
		contentPane.add(btnHome);
		
		btnManageFiles = new JButton("Manage Files");
		btnManageFiles.setForeground(SystemColor.control);
		btnManageFiles.setBackground(SystemColor.activeCaptionBorder);
		btnManageFiles.setIcon(new ImageIcon(ManageFilesGUI.class.getResource("/images/Three_Document_Folders_32 (1).png")));
		btnManageFiles.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnManageFiles.setBounds(268, 81, 252, 79);
		contentPane.add(btnManageFiles);
		
		btnManageGroups = new JButton("Manage Groups");
		btnManageGroups.setIcon(new ImageIcon(ManageFilesGUI.class.getResource("/images/College_Classroom_32.png")));
		btnManageGroups.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnManageGroups.setBounds(528, 81, 252, 79);
		contentPane.add(btnManageGroups);
		
		btnAddFile = new JButton("Add File");
		btnAddFile.setIcon(new ImageIcon(ManageFilesGUI.class.getResource("/images/Paper_Clip_24.png")));
		btnAddFile.setBounds(197, 173, 399, 52);
		contentPane.add(btnAddFile);
		
		btnDeleteFile = new JButton("Delete File");
		btnDeleteFile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnDeleteFile.setIcon(new ImageIcon(ManageFilesGUI.class.getResource("/images/Erasing_Text_24.png")));
		btnDeleteFile.setBounds(197, 238, 399, 52);
		contentPane.add(btnDeleteFile);
		
		btnSearchFiles = new JButton("Search Files");
		btnSearchFiles.setIcon(new ImageIcon(ManageFilesGUI.class.getResource("/images/Inclined_Magnifying_Glass_24.png")));
		btnSearchFiles.setBounds(197, 303, 399, 52);
		contentPane.add(btnSearchFiles);
		
		btnFileInfoEdit = new JButton("File Info Edit");
		btnFileInfoEdit.setIcon(new ImageIcon(ManageFilesGUI.class.getResource("/images/Class_Notes_24 (1).png")));
		btnFileInfoEdit.setBounds(197, 368, 399, 52);
		contentPane.add(btnFileInfoEdit);
		
		btnManageFolders = new JButton("Manage Folders");
		btnManageFolders.setIcon(new ImageIcon(ManageFilesGUI.class.getResource("/images/Library_Bookcase_24.png")));
		btnManageFolders.setBounds(197, 500, 399, 52);
		contentPane.add(btnManageFolders);
		
		btnWatchApprochableFiles = new JButton("Watch Approachable Files");
		btnWatchApprochableFiles.setIcon(new ImageIcon(ManageFilesGUI.class.getResource("/images/Student_Glasses_24.png")));
		btnWatchApprochableFiles.setBounds(197, 433, 399, 52);
		contentPane.add(btnWatchApprochableFiles);
		setLocationRelativeTo(null);
		setVisible(true);
	}
	
	// Action Listeners
	
	public void addbtnHomeActionListener(ActionListener e)
	{
		btnHome.addActionListener(e);
	}
	
	public void addbtnManageFilesActionListener(ActionListener e)
	{
		btnManageFiles.addActionListener(e);
	}
	
	public void addbtnManageGroupsActionListener(ActionListener e)
	{
		btnManageGroups.addActionListener(e);
	}
	
	public void addbtnAddFileActionListener(ActionListener e)
	{
		btnAddFile.addActionListener(e);
	}
	
	public void addbtnDeleteFileActionListener(ActionListener e)
	{
		btnDeleteFile.addActionListener(e);
	}
	
	public void addbtnFileInfoEditActionListener(ActionListener e)
	{
		btnFileInfoEdit.addActionListener(e);
	}
	
	public void addbtnSearchFilesActionListener(ActionListener e)
	{
		btnSearchFiles.addActionListener(e);
	}
	
	public void addbtnWatchApprochableFilesActionListener(ActionListener e)
	{
		btnWatchApprochableFiles.addActionListener(e);
	}
	
	public void addbtnManageFoldersActionListener(ActionListener e)
	{
		btnManageFolders.addActionListener(e);
	}
	


}
