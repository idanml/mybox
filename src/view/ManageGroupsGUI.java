package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.Toolkit;

import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.ImageIcon;

import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.ActionListener;
import java.awt.Color;
import javax.swing.UIManager;

public class ManageGroupsGUI extends JFrame {

	private JPanel contentPane;
	private JButton btnHome;
	private JButton btnManageFiles;
	private JButton btnManageGroups;
	private JButton btnJoinGroups;
	private JButton btnLeaveGroups;
	private JButton btnSeeGroupMembership;
	

	/**
	 * Create the frame. Main Manage Groups Window.
	 */
	public ManageGroupsGUI() {
		setResizable(false);
		setTitle("MyBox");
		setIconImage(Toolkit.getDefaultToolkit().getImage(ManageGroupsGUI.class.getResource("/images/logo2.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("New label");
		label.setBounds(0, 0, 0, 0);
		contentPane.add(label);
		
		JLabel lblLogo = new JLabel("Logo");
		lblLogo.setIcon(new ImageIcon(ManageGroupsGUI.class.getResource("/images/bar.png")));
		lblLogo.setBounds(0, 0, 794, 79);
		contentPane.add(lblLogo);
		
		btnHome = new JButton("Home");
		btnHome.setIcon(new ImageIcon(ManageGroupsGUI.class.getResource("/images/School_Building_with_Flag_32 (1).png")));
		btnHome.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnHome.setBounds(10, 81, 252, 79);
		contentPane.add(btnHome);
		
		btnManageFiles = new JButton("Manage Files");

		btnManageFiles.setIcon(new ImageIcon(ManageGroupsGUI.class.getResource("/images/Three_Document_Folders_32.png")));
		btnManageFiles.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnManageFiles.setBounds(268, 81, 252, 79);
		contentPane.add(btnManageFiles);
		
		btnManageGroups = new JButton("Manage Groups");
		btnManageGroups.setForeground(SystemColor.control);
		btnManageGroups.setBackground(SystemColor.activeCaptionBorder);
		btnManageGroups.setIcon(new ImageIcon(ManageGroupsGUI.class.getResource("/images/College_Classroom_32 (1).png")));
		btnManageGroups.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnManageGroups.setBounds(528, 81, 252, 79);
		contentPane.add(btnManageGroups);
		
		btnJoinGroups = new JButton("Join Groups");
		btnJoinGroups.setIcon(new ImageIcon(ManageGroupsGUI.class.getResource("/images/Add_Friend_24.png")));
		btnJoinGroups.setBounds(197, 201, 399, 52);
		contentPane.add(btnJoinGroups);
		
		btnLeaveGroups = new JButton("Leave Groups");
		btnLeaveGroups.setIcon(new ImageIcon(ManageGroupsGUI.class.getResource("/images/Delete_full_goup_24.png")));
		btnLeaveGroups.setBounds(197, 266, 399, 52);
		contentPane.add(btnLeaveGroups);
		
		btnSeeGroupMembership = new JButton("See Group Membership");
		btnSeeGroupMembership.setIcon(new ImageIcon(ManageGroupsGUI.class.getResource("/images/Mathematical_Symbols_24.png")));
		btnSeeGroupMembership.setBounds(197, 331, 399, 52);
		contentPane.add(btnSeeGroupMembership);
		setLocationRelativeTo(null);
		setVisible(true);
	}
	
	// Action Listeners
	
	public void addbtnHomeActionListener(ActionListener e)
	{
		btnHome.addActionListener(e);
	}
	
	public void addbtnManageFilesActionListener(ActionListener e)
	{
		btnManageFiles.addActionListener(e);
	}
	
	public void addbtnManageGroupsActionListener(ActionListener e)
	{
		btnManageGroups.addActionListener(e);
	}
	
	public void addbtnJoinGroupsActionListener(ActionListener e)
	{
		btnJoinGroups.addActionListener(e);
	}
	
	public void addbtnLeaveGroupsActionListener(ActionListener e)
	{
		btnLeaveGroups.addActionListener(e);
	}
	
	
	public void addbtnSeeGroupMembershipActionListener(ActionListener e)
	{
		btnSeeGroupMembership.addActionListener(e);
	}
	


}
