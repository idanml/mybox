package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.Toolkit;

import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.ImageIcon;

import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.ActionListener;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.JList;
import java.awt.event.ActionEvent;

public class AdminManageFilesGUI extends JFrame {

	private JPanel contentPane;
	private JButton btnOpenFile;
	private JButton btnRename;
	private JButton btnChangeDescription;
	private JButton btnUpdateFile;
	private JButton btnDeleteFile;
	private JList fileList;
	private JLabel lblFiles;
	private JButton btnManageFiles;
	private JButton btnManageGroups;
	private JButton btnGetFileAssociatedUsers;
	

	/**
	 * Create the frame. Admin can see all the files.
	 */
	public AdminManageFilesGUI(Object F) {
		
		setResizable(false);
		setTitle("MyBox");
		setIconImage(Toolkit.getDefaultToolkit().getImage(AdminManageFilesGUI.class.getResource("/images/logo2.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("New label");
		label.setBounds(0, 0, 0, 0);
		contentPane.add(label);
		
		JLabel lblLogo = new JLabel("Logo");
		lblLogo.setIcon(new ImageIcon(AdminManageFilesGUI.class.getResource("/images/bar.png")));
		lblLogo.setBounds(0, 0, 794, 79);
		contentPane.add(lblLogo);
		
		btnOpenFile = new JButton("Open File");
		btnOpenFile.setIcon(new ImageIcon(AdminManageFilesGUI.class.getResource("/images/Text_Line_Form_24.png")));
		btnOpenFile.setBounds(12, 435, 246, 52);
		contentPane.add(btnOpenFile);
		
		btnRename = new JButton("Rename");
		btnRename.setIcon(new ImageIcon(AdminManageFilesGUI.class.getResource("/images/Mathematical_Symbols_24.png")));
		btnRename.setBounds(12, 500, 360, 52);
		contentPane.add(btnRename);
		
		btnChangeDescription = new JButton("Change Description");
		btnChangeDescription.setBounds(409, 500, 360, 52);
		contentPane.add(btnChangeDescription);
		
		btnUpdateFile = new JButton("Update File");
		btnUpdateFile.setIcon(new ImageIcon(AdminManageFilesGUI.class.getResource("/images/Class_Notes_24 (1).png")));
		btnUpdateFile.setBounds(268, 435, 246, 52);
		contentPane.add(btnUpdateFile);
		
		btnDeleteFile = new JButton("Delete File");
		btnDeleteFile.setIcon(new ImageIcon(AdminManageFilesGUI.class.getResource("/images/Wastepaper_basket_24.png")));
		btnDeleteFile.setBounds(523, 435, 246, 52);
		contentPane.add(btnDeleteFile);
		
		ArrayList<String> filesss = new ArrayList<String>(Arrays.asList(((String) F).split(" ")));
		String filess[] = new String[filesss.size()];
		for (int i = 0; i < filesss.size(); i++)
			filess[i]=filesss.get(i);

		
		fileList = new JList(filess);
		fileList.setBounds(20, 203, 749, 190);
		contentPane.add(fileList);
		
		lblFiles = new JLabel("Files:");
		lblFiles.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblFiles.setBounds(20, 173, 56, 16);
		contentPane.add(lblFiles);
		
		btnManageFiles = new JButton("Manage Files");
		btnManageFiles.setForeground(SystemColor.menu);
		btnManageFiles.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnManageFiles.setBackground(SystemColor.activeCaptionBorder);
		btnManageFiles.setBounds(10, 81, 365, 79);
		contentPane.add(btnManageFiles);
		
		btnManageGroups = new JButton("Manage Groups");
		btnManageGroups.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnManageGroups.setBounds(417, 81, 365, 79);
		contentPane.add(btnManageGroups);
		
		
	//	btnGetFileAssociatedUsers = new JButton("Get File Associated Users");
	//	btnGetFileAssociatedUsers.setBounds(218, 404, 328, 23);
	//	contentPane.add(btnGetFileAssociatedUsers);
		
		
		setLocationRelativeTo(null);
		setVisible(true);
	}
	
	// Action Listeners
	


	
	public void addbtnManageFilesActionListener(ActionListener e)
	{
		btnManageFiles.addActionListener(e);
	}
	
	public void addbtnManageGroupsActionListener(ActionListener e)
	{
		btnManageGroups.addActionListener(e);
	}
	
	public void addbtnOpenFileActionListener(ActionListener e)
	{
		btnOpenFile.addActionListener(e);
	}
	
	public void addbtnRenameActionListener(ActionListener e)
	{
		btnRename.addActionListener(e);
	}
	
	public void addbtnChangeDescriptionActionListener(ActionListener e)
	{
		btnChangeDescription.addActionListener(e);
	}
	
	public void addbtneleteFileActionListener(ActionListener e)
	{
		btnDeleteFile.addActionListener(e);
	}
	
	public void addbtnUpdateFileActionListener(ActionListener e)
	{
		btnUpdateFile.addActionListener(e);
	}
	
	public void addbtnGetFileAssociatedUsersActionListener(ActionListener e)
	{
		btnGetFileAssociatedUsers.addActionListener(e);
	}
	
	public JList getFileList() {
		return fileList;
	}
	
	public String getFilefield() {
		return fileList.getSelectedValue().toString();
	}

	public void setFileList(JList fileList) {
		this.fileList = fileList;
	}
	
	
	public boolean isFileFieldEmpty() {
		return (fileList.isSelectionEmpty());

	}
}
