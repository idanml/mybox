package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionListener;

public class ForgotPasswordGUI extends JFrame {


	private JPanel contentPane;
	private JTextField usernameField;
	private JButton btnRecoverMyPassword;

	/**
	 * Create the frame. In this window user can reset his password.
	 */
	public ForgotPasswordGUI() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(ForgotPasswordGUI.class.getResource("/images/logo2.png")));
		setTitle("MyBox Forgot Password");
		setResizable(false);
		//setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 300, 100);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblUsername = new JLabel("Username:");
		lblUsername.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblUsername.setBounds(12, 13, 95, 16);
		contentPane.add(lblUsername);
		
		btnRecoverMyPassword = new JButton("Recover my password!");
		btnRecoverMyPassword.setBounds(37, 40, 221, 25);
		contentPane.add(btnRecoverMyPassword);
		
		usernameField = new JTextField();
		usernameField.setBounds(102, 5, 156, 32);
		contentPane.add(usernameField);
		usernameField.setColumns(10);
		setLocationRelativeTo(null);
		setResizable(false);
		setVisible(true);
		
	}
	
	public void addRecoverMyPasswordActionListener(ActionListener e)
	{
		btnRecoverMyPassword.addActionListener(e);
	}

	public String getUserField() {
		return usernameField.getText();
	}
	
	
	
	
	
	

}
