package view;

import java.awt.BorderLayout;
import java.awt.Choice;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.Toolkit;

import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.ImageIcon;

import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.ActionListener;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.JTextField;

import models.Folders;

public class AddFileGUI extends JFrame {

	private JPanel contentPane;
	private JButton btnHome;
	private JButton btnManageFiles;
	private JButton btnManageGroups;
	private JButton btnAddFile;
	private JButton btnCancel;
	private JLabel lblFileDescription;
	private JTextField fileDescriptionField;
	private JLabel lblFiles;
	private Choice files;
	private JButton btnChooseFile;
	private JLabel selectedFileName;
	
	/**
	 * Create the frame - Windows that allow you to add new File to your box.
	 */
	public AddFileGUI(Object F) {
		setResizable(false);
		setTitle("MyBox");
		setIconImage(Toolkit.getDefaultToolkit().getImage(AddFileGUI.class.getResource("/images/logo2.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("New label");
		label.setBounds(0, 0, 0, 0);
		contentPane.add(label);
		
		JLabel lblLogo = new JLabel("Logo");
		lblLogo.setIcon(new ImageIcon(AddFileGUI.class.getResource("/images/bar.png")));
		lblLogo.setBounds(0, 0, 794, 79);
		contentPane.add(lblLogo);
		
		btnHome = new JButton("Home");
		btnHome.setIcon(new ImageIcon(AddFileGUI.class.getResource("/images/School_Building_with_Flag_32 (1).png")));
		btnHome.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnHome.setBounds(10, 81, 252, 79);
		contentPane.add(btnHome);
		
		btnManageFiles = new JButton("Manage Files");
		btnManageFiles.setForeground(SystemColor.control);
		btnManageFiles.setBackground(new Color(102, 102, 102));
		btnManageFiles.setIcon(new ImageIcon(AddFileGUI.class.getResource("/images/Three_Document_Folders_32 (1).png")));
		btnManageFiles.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnManageFiles.setBounds(268, 81, 252, 79);
		contentPane.add(btnManageFiles);
		
		btnManageGroups = new JButton("Manage Groups");
		btnManageGroups.setIcon(new ImageIcon(AddFileGUI.class.getResource("/images/College_Classroom_32.png")));
		btnManageGroups.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnManageGroups.setBounds(528, 81, 252, 79);
		contentPane.add(btnManageGroups);
		
		btnAddFile = new JButton("Add File");
		btnAddFile.setIcon(new ImageIcon(AddFileGUI.class.getResource("/images/Paper_Clip_24.png")));
		btnAddFile.setBounds(229, 430, 146, 33);
		contentPane.add(btnAddFile);
		
		btnCancel = new JButton("Cancel");
		btnCancel.setIcon(new ImageIcon(AddFileGUI.class.getResource("/images/cancel.PNG")));
		btnCancel.setBounds(403, 430, 146, 33);
		contentPane.add(btnCancel);
		
		lblFiles = new JLabel("Choose Location:");
		lblFiles.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblFiles.setBounds(180, 360, 138, 16);
		contentPane.add(lblFiles);
		
		files = new Choice();
		files.setBounds(320, 360, 250, 79);
		contentPane.add(files);
		ArrayList<String> folders = new ArrayList<String>(Arrays.asList(((String) F).split(" ")));
		if( folders.size()==1)
			files.add("No Folders");
		else
		for (int i = 1; i < folders.size(); i++) 
			files.add((folders.get(i)).toString());
	
		
		
		
		lblFileDescription = new JLabel("File Description:");
		lblFileDescription.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblFileDescription.setBounds(180, 245, 107, 16);
		contentPane.add(lblFileDescription);
		
		fileDescriptionField = new JTextField();
		fileDescriptionField.setColumns(10);
		fileDescriptionField.setBounds(288, 245, 300, 97);
		contentPane.add(fileDescriptionField);
		
		btnChooseFile = new JButton("Choose File");
		btnChooseFile.setBounds(288, 202, 138, 25);
		contentPane.add(btnChooseFile);
		
		selectedFileName = new JLabel("");
		selectedFileName.setForeground(Color.RED);
		selectedFileName.setBounds(459, 207, 129, 14);
		contentPane.add(selectedFileName);
		setLocationRelativeTo(null);
		setVisible(true);
	}
	
	// Action Listeners
	
	public void addbtnHomeActionListener(ActionListener e)
	{
		btnHome.addActionListener(e);
	}
	
	public void addbtnManageFilesActionListener(ActionListener e)
	{
		btnManageFiles.addActionListener(e);
	}
	
	public void addbtnManageGroupsActionListener(ActionListener e)
	{
		btnManageGroups.addActionListener(e);
	}
	
	public void addbtnChooseFileActionListener(ActionListener e)
	{
		btnChooseFile.addActionListener(e);
	}
	
	public void addbtnCancelActionListener(ActionListener e)
	{
		btnCancel.addActionListener(e);
	}
	
	public void addbtnAddFileActionListener(ActionListener e)
	{
		btnAddFile.addActionListener(e);
	}
	

	
	
	// Getters
	public void setSelectedFileName(String name)
	{
		selectedFileName.setText(name);
	}

	public String getFileDescriptionField() {
		return fileDescriptionField.getText();
	}
	
	public void setFileDescriptionField(String text){
		fileDescriptionField.setText(text);
	}

	public String getFileLocationField() {
		if(files.getItem(files.getSelectedIndex())=="No Folders")
			return "Empty";	
		else
		return files.getItem(files.getSelectedIndex());

	}
	
	public Choice getFiles(){
		return files;
	}
	
    public void clickAddButton(){
        this.btnAddFile.doClick();
    }
}
