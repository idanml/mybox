package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.Toolkit;

import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.ImageIcon;

import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.ActionListener;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.JList;

public class FileInfoEditGUI extends JFrame {

	private JPanel contentPane;
	private JButton btnHome;
	private JButton btnManageFiles;
	private JButton btnManageGroups;
	private JButton btnRename;
	private JButton btnChangeDescription;
	private JButton btnChangePermissions;
	private JButton btnChangeFileLocation;
	private JList fileList;
	private JLabel lblFiles;
	
	/**
	 * Create the frame. In this window user can see all the files and choose what actions to do with them (rename, change location, permission or description)
	 */

	public FileInfoEditGUI(Object F) {
		setResizable(false);
		setTitle("MyBox");
		setIconImage(Toolkit.getDefaultToolkit().getImage(FileInfoEditGUI.class.getResource("/images/logo2.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("New label");
		label.setBounds(0, 0, 0, 0);
		contentPane.add(label);
		
		JLabel lblLogo = new JLabel("Logo");
		lblLogo.setIcon(new ImageIcon(FileInfoEditGUI.class.getResource("/images/bar.png")));
		lblLogo.setBounds(0, 0, 794, 79);
		contentPane.add(lblLogo);
		
		btnHome = new JButton("Home");
		btnHome.setIcon(new ImageIcon(FileInfoEditGUI.class.getResource("/images/School_Building_with_Flag_32 (1).png")));
		btnHome.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnHome.setBounds(10, 81, 252, 79);
		contentPane.add(btnHome);
		
		btnManageFiles = new JButton("Manage Files");
		btnManageFiles.setForeground(SystemColor.control);
		btnManageFiles.setBackground(SystemColor.activeCaptionBorder);
		btnManageFiles.setIcon(new ImageIcon(FileInfoEditGUI.class.getResource("/images/Three_Document_Folders_32 (1).png")));
		btnManageFiles.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnManageFiles.setBounds(268, 81, 252, 79);
		contentPane.add(btnManageFiles);
		
		btnManageGroups = new JButton("Manage Groups");
		btnManageGroups.setIcon(new ImageIcon(FileInfoEditGUI.class.getResource("/images/College_Classroom_32.png")));
		btnManageGroups.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnManageGroups.setBounds(528, 81, 252, 79);
		contentPane.add(btnManageGroups);
		
		btnRename = new JButton("Rename");
		btnRename.setIcon(new ImageIcon(FileInfoEditGUI.class.getResource("/images/Inclined_Pencil_with_Eraser_24.png")));
		btnRename.setBounds(12, 435, 360, 52);
		contentPane.add(btnRename);
		
		btnChangeDescription = new JButton("Change Description");
		btnChangeDescription.setIcon(new ImageIcon(FileInfoEditGUI.class.getResource("/images/Class_Notes_24 (1).png")));
		btnChangeDescription.setBounds(12, 500, 360, 52);
		contentPane.add(btnChangeDescription);
		
		btnChangePermissions = new JButton("Change Permissions");
		btnChangePermissions.setIcon(new ImageIcon(FileInfoEditGUI.class.getResource("/images/Mathematical_Symbols_24.png")));
		btnChangePermissions.setBounds(420, 435, 360, 52);
		contentPane.add(btnChangePermissions);
		
		btnChangeFileLocation = new JButton("Change File Location");
		btnChangeFileLocation.setIcon(new ImageIcon(FileInfoEditGUI.class.getResource("/images/Library_Bookcase_24.png")));
		btnChangeFileLocation.setBounds(420, 500, 360, 52);
		contentPane.add(btnChangeFileLocation);
		
		ArrayList<String> filesss = new ArrayList<String>(Arrays.asList(((String) F).split(" ")));
		String filess[] = new String[filesss.size()];
		for (int i = 1; i < filesss.size(); i++)
			filess[i]=filesss.get(i);

		
		fileList = new JList(filess);
		fileList.setBounds(20, 203, 749, 219);
		contentPane.add(fileList);
		
		lblFiles = new JLabel("Files:");
		lblFiles.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblFiles.setBounds(20, 173, 56, 16);
		contentPane.add(lblFiles);
		
		
		setLocationRelativeTo(null);
		setVisible(true);
	}
	
	// Action Listeners
	


	public void addbtnHomeActionListener(ActionListener e)
	{
		btnHome.addActionListener(e);
	}
	
	public void addbtnManageFilesActionListener(ActionListener e)
	{
		btnManageFiles.addActionListener(e);
	}
	
	public void addbtnManageGroupsActionListener(ActionListener e)
	{
		btnManageGroups.addActionListener(e);
	}
	
	public void addbtnRenameActionListener(ActionListener e)
	{
		btnRename.addActionListener(e);
	}
	
	public void addbtnChangeDescriptionActionListener(ActionListener e)
	{
		btnChangeDescription.addActionListener(e);
	}
	
	public void addbtnChangeFileLocationActionListener(ActionListener e)
	{
		btnChangeFileLocation.addActionListener(e);
	}
	
	public void addbtnChangePermissionsActionListener(ActionListener e)
	{
		btnChangePermissions.addActionListener(e);
	}
	
	public JList getFileList() {
		return fileList;
	}
	
	public String getFilefield() {
		return fileList.getSelectedValue().toString();
	}

	public void setFileList(JList fileList) {
		this.fileList = fileList;
	}
	
	
	public boolean isFileFieldEmpty() {
		return (fileList.isSelectionEmpty());

	}
	
	
}
