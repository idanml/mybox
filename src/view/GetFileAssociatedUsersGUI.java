package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.Toolkit;

import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.ImageIcon;

import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.ActionListener;
import java.awt.Color;

import javax.swing.JTextField;
import javax.swing.JList;
import javax.swing.JCheckBox;
import java.awt.Choice;

public class GetFileAssociatedUsersGUI extends JFrame {

	private JPanel contentPane;
	private JButton btnManageFiles;
	private JButton btnManageGroups;
	private JButton btnSendNotification;
	private JButton btnCancel;
	private JLabel lblMeassage;
	private JTextField Message;
	
	/**
	 * Launch the application. In this window admin can see files associated to users (may be not in use).
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GetFileAssociatedUsersGUI frame = new GetFileAssociatedUsersGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GetFileAssociatedUsersGUI() {
		setResizable(false);
		setTitle("MyBox");
		setIconImage(Toolkit.getDefaultToolkit().getImage(GetFileAssociatedUsersGUI.class.getResource("/images/logo2.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("New label");
		label.setBounds(0, 0, 0, 0);
		contentPane.add(label);
		
		JLabel lblLogo = new JLabel("Logo");
		lblLogo.setIcon(new ImageIcon(GetFileAssociatedUsersGUI.class.getResource("/images/ba2r.png")));
		lblLogo.setBounds(0, 0, 794, 79);
		contentPane.add(lblLogo);
		
		btnManageFiles = new JButton("Manage Files");
		btnManageFiles.setIcon(new ImageIcon(GetFileAssociatedUsersGUI.class.getResource("/images/Three_Document_Folders_32 (1).png")));
		btnManageFiles.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnManageFiles.setBounds(10, 81, 365, 79);
		contentPane.add(btnManageFiles);
		
		btnManageGroups = new JButton("Manage Groups");
		btnManageFiles.setForeground(SystemColor.control);
		btnManageFiles.setBackground(SystemColor.activeCaptionBorder);
		btnManageGroups.setIcon(new ImageIcon(GetFileAssociatedUsersGUI.class.getResource("/images/College_Classroom_32.png")));
		btnManageGroups.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnManageGroups.setBounds(417, 81, 365, 79);
		contentPane.add(btnManageGroups);
		
		btnSendNotification = new JButton("Send Notification");
		btnSendNotification.setIcon(new ImageIcon(GetFileAssociatedUsersGUI.class.getResource("/images/Notification_bell_24.png")));
		btnSendNotification.setBounds(227, 430, 168, 33);
		contentPane.add(btnSendNotification);
		
		btnCancel = new JButton("Cancel");
		btnCancel.setIcon(new ImageIcon(GetFileAssociatedUsersGUI.class.getResource("/images/cancel.PNG")));
		btnCancel.setBounds(407, 430, 168, 33);
		contentPane.add(btnCancel);
		
		lblMeassage = new JLabel("Meassage:");
		lblMeassage.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblMeassage.setBounds(164, 247, 106, 22);
		contentPane.add(lblMeassage);
		
		Message = new JTextField();
		Message.setBounds(261, 245, 294, 133);
		contentPane.add(Message);
		Message.setColumns(10);
		setLocationRelativeTo(null);
		setVisible(true);
	}
	
	// Action Listeners
	
	
	public void addbtnManageFilesActionListener(ActionListener e)
	{
		btnManageFiles.addActionListener(e);
	}
	

	public void addbtnManageGroupsActionListener(ActionListener e)
	{
		btnManageGroups.addActionListener(e);
	}
	
	public void addbtnAprroveActionListener(ActionListener e)
	{
		btnSendNotification.addActionListener(e);
	}
	
	
	public void addbtnCancelActionListener(ActionListener e)
	{
		btnCancel.addActionListener(e);
	}



	
	// Getters


	public JTextField getMessage() {
		return Message;
	}

	public void setMessage(JTextField message) {
		Message = message;
	}
}
