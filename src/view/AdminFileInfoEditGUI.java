package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.Toolkit;

import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.ImageIcon;

import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.ActionListener;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.JList;

public class AdminFileInfoEditGUI extends JFrame {

	private JPanel contentPane;
	private JButton btnRename;
	private JButton btnChangeDescription;
	private JButton btnManageFiles;
	private JButton btnManageGroups;
	private JButton btnGetFileAssociatedUsers;
	
	/**
	 * Create the frame. Admin can choose between rename and change description.
	 */
	public AdminFileInfoEditGUI(Object F) {
		
		setResizable(false);
		setTitle("MyBox");
		setIconImage(Toolkit.getDefaultToolkit().getImage(AdminFileInfoEditGUI.class.getResource("/images/logo2.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("New label");
		label.setBounds(0, 0, 0, 0);
		contentPane.add(label);
		
		JLabel lblLogo = new JLabel("Logo");
		lblLogo.setIcon(new ImageIcon(AdminFileInfoEditGUI.class.getResource("/images/ba2r.png")));
		lblLogo.setBounds(0, 0, 794, 79);
		contentPane.add(lblLogo);
		
		btnRename = new JButton("Rename");
		btnRename.setIcon(new ImageIcon(AdminFileInfoEditGUI.class.getResource("/images/Inclined_Pencil_with_Eraser_24.png")));
		btnRename.setBounds(223, 217, 360, 52);
		contentPane.add(btnRename);
		
		btnChangeDescription = new JButton("Change Description");
		btnChangeDescription.setIcon(new ImageIcon(AdminFileInfoEditGUI.class.getResource("/images/Class_Notes_24 (1).png")));
		btnChangeDescription.setBounds(223, 282, 360, 52);
		contentPane.add(btnChangeDescription);
		
		ArrayList<String> filesss = new ArrayList<String>(Arrays.asList(((String) F).split(" ")));
		String filess[] = new String[filesss.size()];
		for (int i = 0; i < filesss.size(); i++)
			filess[i]=filesss.get(i);
		
		btnManageFiles = new JButton("Manage Files");
		btnManageFiles.setIcon(new ImageIcon(AdminFileInfoEditGUI.class.getResource("/images/Three_Document_Folders_32 (1).png")));
		btnManageFiles.setForeground(SystemColor.menu);
		btnManageFiles.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnManageFiles.setBackground(SystemColor.activeCaptionBorder);
		btnManageFiles.setBounds(10, 81, 365, 79);
		contentPane.add(btnManageFiles);
		
		btnManageGroups = new JButton("Manage Groups");
		btnManageGroups.setIcon(new ImageIcon(AdminFileInfoEditGUI.class.getResource("/images/College_Classroom_32.png")));
		btnManageGroups.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnManageGroups.setBounds(417, 81, 365, 79);
		contentPane.add(btnManageGroups);
		

		setLocationRelativeTo(null);
		setVisible(true);
	}
	
	// Action Listeners
	


	
	public void addbtnManageFilesActionListener(ActionListener e)
	{
		btnManageFiles.addActionListener(e);
	}
	
	public void addbtnManageGroupsActionListener(ActionListener e)
	{
		btnManageGroups.addActionListener(e);
	}
	
	public void addbtnRenameActionListener(ActionListener e)
	{
		btnRename.addActionListener(e);
	}

	
	public void addbtnChangeDescriptionActionListener(ActionListener e)
	{
		btnChangeDescription.addActionListener(e);
	}
	

	
	

	
	
}
