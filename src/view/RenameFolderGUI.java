package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.Toolkit;

import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.ImageIcon;

import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.ActionListener;
import java.awt.Color;
import javax.swing.JTextField;

public class RenameFolderGUI extends JFrame {

	private JPanel contentPane;
	private JButton btnHome;
	private JButton btnManageFiles;
	private JButton btnManageGroups;
	private JButton btnRenameFolder;
	private JButton btnCancel;
	private JLabel lblNewFolderName;
	private JTextField folderNameField;
	/**
	 * Create the frame.  In this window user can rename a folder.
	 */
	public RenameFolderGUI() {
		setResizable(false);
		setTitle("MyBox");
		setIconImage(Toolkit.getDefaultToolkit().getImage(RenameFolderGUI.class.getResource("/images/logo2.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("New label");
		label.setBounds(0, 0, 0, 0);
		contentPane.add(label);
		
		JLabel lblLogo = new JLabel("Logo");
		lblLogo.setIcon(new ImageIcon(RenameFolderGUI.class.getResource("/images/bar.png")));
		lblLogo.setBounds(0, 0, 794, 79);
		contentPane.add(lblLogo);
		
		btnHome = new JButton("Home");
		btnHome.setIcon(new ImageIcon(RenameFolderGUI.class.getResource("/images/School_Building_with_Flag_32 (1).png")));
		btnHome.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnHome.setBounds(10, 81, 252, 79);
		contentPane.add(btnHome);
		
		btnManageFiles = new JButton("Manage Files");
		btnManageFiles.setForeground(SystemColor.control);
		btnManageFiles.setBackground(SystemColor.activeCaptionBorder);
		btnManageFiles.setIcon(new ImageIcon(RenameFolderGUI.class.getResource("/images/Three_Document_Folders_32 (1).png")));
		btnManageFiles.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnManageFiles.setBounds(268, 81, 252, 79);
		contentPane.add(btnManageFiles);
		
		btnManageGroups = new JButton("Manage Groups");
		btnManageGroups.setIcon(new ImageIcon(RenameFolderGUI.class.getResource("/images/College_Classroom_32.png")));
		btnManageGroups.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnManageGroups.setBounds(528, 81, 252, 79);
		contentPane.add(btnManageGroups);
		
		btnRenameFolder = new JButton("Rename");
		btnRenameFolder.setIcon(new ImageIcon(RenameFolderGUI.class.getResource("/images/Inclined_Pencil_with_Eraser_24.png")));
		btnRenameFolder.setBounds(229, 430, 146, 33);
		contentPane.add(btnRenameFolder);
		
		btnCancel = new JButton("Cancel");
		btnCancel.setIcon(new ImageIcon(RenameFolderGUI.class.getResource("/images/cancel.PNG")));
		btnCancel.setBounds(403, 430, 146, 33);
		contentPane.add(btnCancel);
		
		lblNewFolderName = new JLabel("New Folder Name:");
		lblNewFolderName.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewFolderName.setBounds(149, 214, 139, 16);
		contentPane.add(lblNewFolderName);
		
		folderNameField = new JTextField();
		folderNameField.setBounds(268, 207, 300, 33);
		contentPane.add(folderNameField);
		folderNameField.setColumns(10);
		setLocationRelativeTo(null);
		setVisible(true);
	}
	
	// Action Listeners
	
	public void addbtnHomeActionListener(ActionListener e)
	{
		btnHome.addActionListener(e);
	}
	
	public void addbtnManageFilesActionListener(ActionListener e)
	{
		btnManageFiles.addActionListener(e);
	}
	
	public void addbtnManageGroupsActionListener(ActionListener e)
	{
		btnManageGroups.addActionListener(e);
	}
	
	public void addbtnRenameFolderActionListener(ActionListener e)
	{
		btnRenameFolder.addActionListener(e);
	}
	
	public void addbtnCancelActionListener(ActionListener e)
	{
		btnCancel.addActionListener(e);
	}

	public String getFolderNameField() {
		return folderNameField.getText();
	}
	
	// Getters




}
