package view;

import java.awt.BorderLayout;
import java.awt.Choice;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.Toolkit;

import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.ImageIcon;

import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.ActionListener;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.JTextField;
import javax.swing.JList;

public class ChangeFolderLocationGUI extends JFrame {

	private JPanel contentPane;
	private JButton btnHome;
	private JButton btnManageFiles;
	private JButton btnManageGroups;
	private JButton btnChangeFolderLocation;
	private JButton btnCancel;
	private JLabel lblFolders;
	private Choice folders;


	/**
	 * Create the frame. In this window user can change new folder location.
	 */
	public ChangeFolderLocationGUI(Object F) {
		setResizable(false);
		setTitle("MyBox");
		setIconImage(Toolkit.getDefaultToolkit().getImage(ChangeFolderLocationGUI.class.getResource("/images/logo2.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("New label");
		label.setBounds(0, 0, 0, 0);
		contentPane.add(label);
		
		JLabel lblLogo = new JLabel("Logo");
		lblLogo.setIcon(new ImageIcon(ChangeFolderLocationGUI.class.getResource("/images/bar.png")));
		lblLogo.setBounds(0, 0, 794, 79);
		contentPane.add(lblLogo);
		
		btnHome = new JButton("Home");
		btnHome.setIcon(new ImageIcon(ChangeFolderLocationGUI.class.getResource("/images/School_Building_with_Flag_32 (1).png")));
		btnHome.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnHome.setBounds(10, 81, 252, 79);
		contentPane.add(btnHome);
		
		btnManageFiles = new JButton("Manage Files");
		btnManageFiles.setForeground(SystemColor.control);
		btnManageFiles.setBackground(SystemColor.activeCaptionBorder);
		btnManageFiles.setIcon(new ImageIcon(ChangeFolderLocationGUI.class.getResource("/images/Three_Document_Folders_32 (1).png")));
		btnManageFiles.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnManageFiles.setBounds(268, 81, 252, 79);
		contentPane.add(btnManageFiles);
		
		btnManageGroups = new JButton("Manage Groups");
		btnManageGroups.setIcon(new ImageIcon(ChangeFolderLocationGUI.class.getResource("/images/College_Classroom_32.png")));
		btnManageGroups.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnManageGroups.setBounds(528, 81, 252, 79);
		contentPane.add(btnManageGroups);
		
		btnChangeFolderLocation = new JButton("Change Location");
		btnChangeFolderLocation.setIcon(new ImageIcon(ChangeFolderLocationGUI.class.getResource("/images/Library_Bookcase_24.png")));
		btnChangeFolderLocation.setBounds(207, 430, 168, 33);
		contentPane.add(btnChangeFolderLocation);
		
		btnCancel = new JButton("Cancel");
		btnCancel.setIcon(new ImageIcon(ChangeFolderLocationGUI.class.getResource("/images/cancel.PNG")));
		btnCancel.setBounds(403, 430, 168, 33);
		contentPane.add(btnCancel);
		
		lblFolders = new JLabel("Folders:");
		lblFolders.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblFolders.setBounds(20, 183, 55, 16);
		contentPane.add(lblFolders);
		
		folders = new Choice();
		folders.setBounds(320, 360, 250, 79);
		contentPane.add(folders);
		ArrayList<String> folder = new ArrayList<String>(Arrays.asList(((String) F).split(" ")));
		for (int i = 1; i < folder.size(); i++) 
			folders.add((folder.get(i)).toString());
		
		setLocationRelativeTo(null);
		setVisible(true);
	}
	
	// Action Listeners
	
	public void addbtnHomeActionListener(ActionListener e)
	{
		btnHome.addActionListener(e);
	}
	
	public void addbtnManageFilesActionListener(ActionListener e)
	{
		btnManageFiles.addActionListener(e);
	}
	
	public void addbtnManageGroupsActionListener(ActionListener e)
	{
		btnManageGroups.addActionListener(e);
	}
	
	public void addbtnChangeLocationActionListener(ActionListener e)
	{
		btnChangeFolderLocation.addActionListener(e);
	}
	
	public void addbtnCancelActionListener(ActionListener e)
	{
		btnCancel.addActionListener(e);
	}

	public String getSelectedFolder()
	{
		return folders.getSelectedItem();
	}
}
