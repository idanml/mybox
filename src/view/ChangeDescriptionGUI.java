package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.Toolkit;

import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.ImageIcon;

import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.ActionListener;
import java.awt.Color;

import javax.swing.JTextField;

public class ChangeDescriptionGUI extends JFrame {

	private JPanel contentPane;
	private JButton btnHome;
	private JButton btnManageFiles;
	private JButton btnManageGroups;
	private JButton btnChangeDescription;
	private JButton btnCancel;
	private JLabel lblNewFileName;
	private JTextField DescriptionField;
	
	/**
	 * Launch the application. In this window user can enter the new file description.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ChangeDescriptionGUI frame = new ChangeDescriptionGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ChangeDescriptionGUI() {
		setResizable(false);
		setTitle("MyBox");
		setIconImage(Toolkit.getDefaultToolkit().getImage(ChangeDescriptionGUI.class.getResource("/images/logo2.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("New label");
		label.setBounds(0, 0, 0, 0);
		contentPane.add(label);
		
		JLabel lblLogo = new JLabel("Logo");
		lblLogo.setIcon(new ImageIcon(ChangeDescriptionGUI.class.getResource("/images/bar.png")));
		lblLogo.setBounds(0, 0, 794, 79);
		contentPane.add(lblLogo);
		
		btnHome = new JButton("Home");
		btnHome.setIcon(new ImageIcon(ChangeDescriptionGUI.class.getResource("/images/School_Building_with_Flag_32 (1).png")));
		btnHome.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnHome.setBounds(10, 81, 252, 79);
		contentPane.add(btnHome);
		
		btnManageFiles = new JButton("Manage Files");
		btnManageFiles.setForeground(SystemColor.control);
		btnManageFiles.setBackground(SystemColor.activeCaptionBorder);
		btnManageFiles.setIcon(new ImageIcon(ChangeDescriptionGUI.class.getResource("/images/Three_Document_Folders_32 (1).png")));
		btnManageFiles.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnManageFiles.setBounds(268, 81, 252, 79);
		contentPane.add(btnManageFiles);
		
		btnManageGroups = new JButton("Manage Groups");
		btnManageGroups.setIcon(new ImageIcon(ChangeDescriptionGUI.class.getResource("/images/College_Classroom_32.png")));
		btnManageGroups.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnManageGroups.setBounds(528, 81, 252, 79);
		contentPane.add(btnManageGroups);
		
		btnChangeDescription = new JButton("Change Description");
		btnChangeDescription.setIcon(new ImageIcon(ChangeDescriptionGUI.class.getResource("/images/Inclined_Pencil_with_Eraser_24.png")));
		btnChangeDescription.setBounds(204, 430, 171, 33);
		contentPane.add(btnChangeDescription);
		
		btnCancel = new JButton("Cancel");
		btnCancel.setIcon(new ImageIcon(ChangeDescriptionGUI.class.getResource("/images/cancel.PNG")));
		btnCancel.setBounds(403, 430, 171, 33);
		contentPane.add(btnCancel);
		
		lblNewFileName = new JLabel("New File Description:");
		lblNewFileName.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewFileName.setBounds(130, 214, 146, 16);
		contentPane.add(lblNewFileName);
		
		DescriptionField = new JTextField();
		DescriptionField.setBounds(268, 207, 300, 136);
		contentPane.add(DescriptionField);
		DescriptionField.setColumns(10);
		setLocationRelativeTo(null);
		setVisible(true);
	}
	
	// Action Listeners
	
	public void addbtnHomeActionListener(ActionListener e)
	{
		btnHome.addActionListener(e);
	}
	
	public void addbtnManageFilesActionListener(ActionListener e)
	{
		btnManageFiles.addActionListener(e);
	}
	
	public void addbtnManageGroupsActionListener(ActionListener e)
	{
		btnManageGroups.addActionListener(e);
	}
	
	public void addbtnChangeDescriptionActionListener(ActionListener e)
	{
		btnChangeDescription.addActionListener(e);
	}
	
	public void addbtnCancelActionListener(ActionListener e)
	{
		btnCancel.addActionListener(e);
	}


	
	// Getters

	public String getDescriptionField() {
		return DescriptionField.getText();
	}


}
