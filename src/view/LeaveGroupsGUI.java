package view;

import java.awt.BorderLayout;
import java.awt.Choice;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.Toolkit;

import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.ImageIcon;

import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.ActionListener;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.JTextField;
import javax.swing.JList;

public class LeaveGroupsGUI extends JFrame {

	private JPanel contentPane;
	private JButton btnHome;
	private JButton btnManageFiles;
	private JButton btnManageGroups;
	private JButton btnLeave;
	private JButton btnCancel;
	private JLabel lblGroupsYouIn;
	private Choice GroupsList;
	

	/**
	 * Create the frame. In this window user can request to leave groups.
	 */
	public LeaveGroupsGUI(Object groups) {
		setResizable(false);
		setTitle("MyBox");
		setIconImage(Toolkit.getDefaultToolkit().getImage(LeaveGroupsGUI.class.getResource("/images/logo2.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("New label");
		label.setBounds(0, 0, 0, 0);
		contentPane.add(label);
		
		JLabel lblLogo = new JLabel("Logo");
		lblLogo.setIcon(new ImageIcon(LeaveGroupsGUI.class.getResource("/images/bar.png")));
		lblLogo.setBounds(0, 0, 794, 79);
		contentPane.add(lblLogo);
		
		btnHome = new JButton("Home");
		btnHome.setIcon(new ImageIcon(LeaveGroupsGUI.class.getResource("/images/School_Building_with_Flag_32 (1).png")));
		btnHome.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnHome.setBounds(10, 81, 252, 79);
		contentPane.add(btnHome);
		
		btnManageFiles = new JButton("Manage Files");
		btnManageFiles.setIcon(new ImageIcon(ManageGroupsGUI.class.getResource("/images/Three_Document_Folders_32.png")));
		btnManageFiles.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnManageFiles.setBounds(268, 81, 252, 79);
		contentPane.add(btnManageFiles);
		
		btnManageGroups = new JButton("Manage Groups");
		btnManageGroups.setForeground(SystemColor.control);
		btnManageGroups.setBackground(SystemColor.activeCaptionBorder);
		btnManageGroups.setIcon(new ImageIcon(ManageGroupsGUI.class.getResource("/images/College_Classroom_32 (1).png")));
		btnManageGroups.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnManageGroups.setBounds(528, 81, 252, 79);
		contentPane.add(btnManageGroups);
		
		btnLeave = new JButton("Leave");
		btnLeave.setIcon(new ImageIcon(LeaveGroupsGUI.class.getResource("/images/Delete_full_goup_24.png")));
		btnLeave.setBounds(207, 430, 168, 33);
		contentPane.add(btnLeave);
		
		btnCancel = new JButton("Cancel");
		btnCancel.setIcon(new ImageIcon(LeaveGroupsGUI.class.getResource("/images/cancel.PNG")));
		btnCancel.setBounds(403, 430, 168, 33);
		contentPane.add(btnCancel);
		
		lblGroupsYouIn = new JLabel("Groups you in:");
		lblGroupsYouIn.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblGroupsYouIn.setBounds(20, 183, 119, 16);
		contentPane.add(lblGroupsYouIn);
		
		GroupsList = new Choice();
		GroupsList.setBounds(268, 296, 250, 79);
		ArrayList<String> gr = new ArrayList<String>(Arrays.asList(((String) groups).split(" ")));
		for (int i = 1; i < gr.size(); i++) 
			GroupsList.add((gr.get(i)).toString());
		contentPane.add(GroupsList);
		setLocationRelativeTo(null);
		setVisible(true);
	}
	
	// Action Listeners
	
	public void addbtnHomeActionListener(ActionListener e)
	{
		btnHome.addActionListener(e);
	}
	
	public void addbtnManageFilesActionListener(ActionListener e)
	{
		btnManageFiles.addActionListener(e);
	}
	
	public void addbtnManageGroupsActionListener(ActionListener e)
	{
		btnManageGroups.addActionListener(e);
	}
	
	public void addbtnLeaveActionListener(ActionListener e)
	{
		btnLeave.addActionListener(e);
	}
	
	public void addbtnCancelActionListener(ActionListener e)
	{
		btnCancel.addActionListener(e);
	}
	
	public String getGroupChoice()
	{
		return GroupsList.getSelectedItem();
	}

}
