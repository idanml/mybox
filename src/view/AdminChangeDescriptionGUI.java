package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.Toolkit;

import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.ImageIcon;

import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.ActionListener;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.JList;
import javax.swing.JTextField;

public class AdminChangeDescriptionGUI extends JFrame {

	private JPanel contentPane;
	private JButton btnManageFiles;
	private JButton btnManageGroups;
	private JButton btnGetFileAssociatedUsers;
	private JButton btnCancel;
	private JLabel label_1;
	private JTextField newDescription;
	private JButton btnChangeDescription;


	/**
	 * Create the frame. In This Windows you can enter the new file description.
	 */
	public AdminChangeDescriptionGUI() {
		
		setResizable(false);
		setTitle("MyBox");
		setIconImage(Toolkit.getDefaultToolkit().getImage(AdminChangeDescriptionGUI.class.getResource("/images/logo2.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("New label");
		label.setBounds(0, 0, 0, 0);
		contentPane.add(label);
		
		JLabel lblLogo = new JLabel("Logo");
		lblLogo.setIcon(new ImageIcon(AdminChangeDescriptionGUI.class.getResource("/images/ba2r.png")));
		lblLogo.setBounds(0, 0, 794, 79);
		contentPane.add(lblLogo);
		
		btnManageFiles = new JButton("Manage Files");
		btnManageFiles.setIcon(new ImageIcon(AdminChangeDescriptionGUI.class.getResource("/images/Three_Document_Folders_32 (1).png")));
		btnManageFiles.setForeground(SystemColor.menu);
		btnManageFiles.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnManageFiles.setBackground(SystemColor.activeCaptionBorder);
		btnManageFiles.setBounds(10, 81, 365, 79);
		contentPane.add(btnManageFiles);
		
		btnManageGroups = new JButton("Manage Groups");
		btnManageGroups.setIcon(new ImageIcon(AdminChangeDescriptionGUI.class.getResource("/images/College_Classroom_32.png")));
		btnManageGroups.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnManageGroups.setBounds(417, 81, 365, 79);
		contentPane.add(btnManageGroups);
		
		btnCancel = new JButton("Cancel");
		btnCancel.setIcon(new ImageIcon(AdminChangeDescriptionGUI.class.getResource("/images/cancel.PNG")));
		btnCancel.setBounds(403, 430, 171, 33);
		contentPane.add(btnCancel);
		
		label_1 = new JLabel("New File Description:");
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_1.setBounds(136, 215, 146, 16);
		contentPane.add(label_1);
		
		newDescription = new JTextField();
		newDescription.setColumns(10);
		newDescription.setBounds(274, 208, 300, 136);
		contentPane.add(newDescription);
		
		btnChangeDescription = new JButton("Change Description");
		btnChangeDescription.setIcon(new ImageIcon(AdminChangeDescriptionGUI.class.getResource("/images/Inclined_Pencil_with_Eraser_24.png")));
		btnChangeDescription.setBounds(204, 430, 171, 33);
		contentPane.add(btnChangeDescription);
		

		setLocationRelativeTo(null);
		setVisible(true);
	}
	
	// Action Listeners
	


	
	public void addbtnManageFilesActionListener(ActionListener e)
	{
		btnManageFiles.addActionListener(e);
	}
	
	public void addbtnManageGroupsActionListener(ActionListener e)
	{
		btnManageGroups.addActionListener(e);
	}
	
	public void addbtnCancelActionListener(ActionListener e)
	{
		btnCancel.addActionListener(e);
	}

	
	public void addbtnChangeDescriptionActionListener(ActionListener e)
	{
		btnChangeDescription.addActionListener(e);
	}
	
	//Getters and Setters


	public String getNewDescription() {
		return newDescription.getText();
	}



	
}
