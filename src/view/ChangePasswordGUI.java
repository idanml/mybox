package view;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;

import java.awt.Window.Type;

import javax.swing.JLabel;
import javax.swing.ImageIcon;

import java.awt.Font;
import java.awt.Color;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import java.awt.Toolkit;


public class ChangePasswordGUI extends JFrame {

	private JPanel contentPane;
	private JButton btnCancel;
	private JButton btnChangePassword;
	  private JLabel lblwarningMessage = null;
	  private JTextField oldPassword;
	  private JLabel lblOldPassword;
	  private JLabel lblNewPassword;
	  private JTextField newPassword;
	  private JTextField confrimPassword;
	  private JLabel lblConfirmPassword;
	  private JTextField userName;
	  private JLabel lblUsername;
	  
	/**
	 * Launch the application. In this window user can change his password.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ChangePasswordGUI frame = new ChangePasswordGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ChangePasswordGUI() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(ChangePasswordGUI.class.getResource("/images/logo2.png")));
		setTitle("MyBox Login");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 400, 360);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("New label");
		label.setBounds(120, 0, 156, 150);
		label.setIcon(new ImageIcon(ChangePasswordGUI.class.getResource("/images/logo2.png")));
		contentPane.add(label);
		
		btnChangePassword = new JButton("Change Password");
		btnChangePassword.setIcon(new ImageIcon(ChangePasswordGUI.class.getResource("/images/Password_protection_symbol_on_monitor_screen_24.png")));
		btnChangePassword.setBounds(12, 287, 170, 25);
		contentPane.add(btnChangePassword);
		
		btnCancel = new JButton("Cancel");
		btnCancel.setIcon(new ImageIcon(ChangePasswordGUI.class.getResource("/images/cancel.PNG")));
		btnCancel.setBounds(212, 287, 170, 25);
		contentPane.add(btnCancel);
		
		oldPassword = new JTextField();
		oldPassword.setColumns(10);
		oldPassword.setBounds(167, 178, 194, 22);
		contentPane.add(oldPassword);
		
		lblOldPassword = new JLabel("Old Password:");
		lblOldPassword.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblOldPassword.setBounds(29, 180, 126, 16);
		contentPane.add(lblOldPassword);
		
		lblNewPassword = new JLabel("New Password:");
		lblNewPassword.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewPassword.setBounds(29, 214, 126, 16);
		contentPane.add(lblNewPassword);
		
		newPassword = new JTextField();
		newPassword.setColumns(10);
		newPassword.setBounds(167, 212, 194, 22);
		contentPane.add(newPassword);
		
		confrimPassword = new JTextField();
		confrimPassword.setColumns(10);
		confrimPassword.setBounds(167, 247, 194, 22);
		contentPane.add(confrimPassword);
		
		lblConfirmPassword = new JLabel("Confirm Password:");
		lblConfirmPassword.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblConfirmPassword.setBounds(29, 249, 126, 16);
		contentPane.add(lblConfirmPassword);
		
		userName = new JTextField();
		userName.setColumns(10);
		userName.setBounds(167, 141, 194, 22);
		contentPane.add(userName);
		
		lblUsername = new JLabel("Username:");
		lblUsername.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblUsername.setBounds(29, 143, 126, 16);
		contentPane.add(lblUsername);
		setLocationRelativeTo(null);
		setVisible(true);
	}
	
	// Action Listeners
	
	
	public JLabel getLblwarningMessage() {
		if(lblwarningMessage == null){
			lblwarningMessage = new JLabel("user name or password is wrong");
			lblwarningMessage.setForeground(Color.RED);
			lblwarningMessage.setBounds(10, 165, 200, 30);
			lblwarningMessage.setVisible(false);
		}
		return lblwarningMessage;
	}
	
	
	public void setWarningMessageVisibleTrue() {
		lblwarningMessage.setVisible(true);	
	}
	
	public void setWarningMessageVisibleTrue(String st) {
		lblwarningMessage.setText(st);
		lblwarningMessage.setForeground(Color.RED);
		lblwarningMessage.setBounds(10, 165, 245, 30);
		lblwarningMessage.setVisible(true);	
		
	}
	
	
	
	public void undisplayWarningMessage() {
		lblwarningMessage.setVisible(false);
		
	}
	

	
	
	
	
	
	
	
	public void addbtnChangePasswordActionListener(ActionListener e)
	{
		btnChangePassword.addActionListener(e);
	}
	
	public void addCancelActionListener(ActionListener e)
	{
		btnCancel.addActionListener(e);
	}

	
	// Getters
	public JTextField getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(JTextField oldPassword) {
		this.oldPassword = oldPassword;
	}

	public JTextField getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(JTextField newPassword) {
		this.newPassword = newPassword;
	}

	public JTextField getConfirmPassword() {
		return confrimPassword;
	}

	public void setLblConfirmPassword(JLabel lblConfirmPassword) {
		this.lblConfirmPassword = lblConfirmPassword;
	}

	public JTextField getUsername() {
		return userName;
	}

	public void setLblUsername(JLabel lblUsername) {
		this.lblUsername = lblUsername;
	}
	
	
}
	
	
	

