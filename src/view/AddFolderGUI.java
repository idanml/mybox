package view;

import java.awt.BorderLayout;
import java.awt.Choice;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.Toolkit;

import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.ImageIcon;

import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.ActionListener;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.JTextField;

public class AddFolderGUI extends JFrame {

	private JPanel contentPane;
	private JButton btnHome;
	private JButton btnManageFiles;
	private JButton btnManageGroups;
	private JButton btnAddFolder;
	private JButton btnCancel;
	private JLabel lblFolderName;
	private JTextField folderNameField;
	private Choice folders;
	

	/**
	 * Create the frame. in this window user can add new folder.
	 */
	public AddFolderGUI(Object F) {
		setResizable(false);
		setTitle("MyBox");
		setIconImage(Toolkit.getDefaultToolkit().getImage(AddFolderGUI.class.getResource("/images/logo2.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("New label");
		label.setBounds(0, 0, 0, 0);
		contentPane.add(label);
		
		JLabel lblLogo = new JLabel("Logo");
		lblLogo.setIcon(new ImageIcon(AddFolderGUI.class.getResource("/images/bar.png")));
		lblLogo.setBounds(0, 0, 794, 79);
		contentPane.add(lblLogo);
		
		btnHome = new JButton("Home");
		btnHome.setIcon(new ImageIcon(AddFolderGUI.class.getResource("/images/School_Building_with_Flag_32 (1).png")));
		btnHome.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnHome.setBounds(10, 81, 252, 79);
		contentPane.add(btnHome);
		
		btnManageFiles = new JButton("Manage Files");
		btnManageFiles.setForeground(SystemColor.control);
		btnManageFiles.setBackground(new Color(102, 102, 102));
		btnManageFiles.setIcon(new ImageIcon(AddFolderGUI.class.getResource("/images/Three_Document_Folders_32 (1).png")));
		btnManageFiles.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnManageFiles.setBounds(268, 81, 252, 79);
		contentPane.add(btnManageFiles);
		
		btnManageGroups = new JButton("Manage Groups");
		btnManageGroups.setIcon(new ImageIcon(AddFolderGUI.class.getResource("/images/College_Classroom_32.png")));
		btnManageGroups.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnManageGroups.setBounds(528, 81, 252, 79);
		contentPane.add(btnManageGroups);
		
		btnAddFolder = new JButton("Add Folder");
		btnAddFolder.setIcon(new ImageIcon(AddFolderGUI.class.getResource("/images/Folder_with_Plus_Symbol_24.png")));
		btnAddFolder.setBounds(229, 430, 146, 33);
		contentPane.add(btnAddFolder);
		
		btnCancel = new JButton("Cancel");
		btnCancel.setIcon(new ImageIcon(AddFolderGUI.class.getResource("/images/cancel.PNG")));
		btnCancel.setBounds(403, 430, 146, 33);
		contentPane.add(btnCancel);
		
		lblFolderName = new JLabel("Folder Name:");
		lblFolderName.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblFolderName.setBounds(180, 214, 82, 16);
		contentPane.add(lblFolderName);
		
		folders = new Choice();
		folders.setBounds(320, 360, 250, 79);
		contentPane.add(folders);
		ArrayList<String> folder = new ArrayList<String>(Arrays.asList(((String) F).split(" ")));
		for (int i = 1; i < folder.size(); i++) 
			folders.add((folder.get(i)).toString());
		
		folderNameField = new JTextField();
		folderNameField.setBounds(288, 207, 300, 33);
		contentPane.add(folderNameField);
		folderNameField.setColumns(10);
		setLocationRelativeTo(null);
		setVisible(true);
	}
	
	// Action Listeners
	
	public void addbtnHomeActionListener(ActionListener e)
	{
		btnHome.addActionListener(e);
	}
	
	public void addbtnManageFilesActionListener(ActionListener e)
	{
		btnManageFiles.addActionListener(e);
	}
	
	public void addbtnManageGroupsActionListener(ActionListener e)
	{
		btnManageGroups.addActionListener(e);
	}
	
	public void addbtnAddFolderActionListener(ActionListener e)
	{
		btnAddFolder.addActionListener(e);
	}
	
	public void addbtnCancelActionListener(ActionListener e)
	{
		btnCancel.addActionListener(e);
	}

	
	// Getters

	public String getFolderNameField() {
		return folderNameField.getText();
	}
	
	public String getFolderLoc() {
		return folders.getSelectedItem();
	}

	
	public boolean isFolderFieldEmpty() {
		if( folderNameField.getText().equals(""))
			return true;
		else
			return false;

	}
	

}
