package view;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;

import java.awt.Window.Type;

import javax.swing.JLabel;
import javax.swing.ImageIcon;

import java.awt.Font;
import java.awt.Color;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;


public class LoginGUI extends JFrame {

	private JPanel contentPane;
	private JTextField userField;
	private JTextField passwordField;
	private JButton btnCancel;
	private JButton btnLogin;
	private JButton btnForgotYourPassword;
	  private JLabel lblwarningMessage = null;
	  private JButton btnChangePassword;
	  private JButton btnRegister;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoginGUI frame = new LoginGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public LoginGUI() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(LoginGUI.class.getResource("/images/logo2.png")));
		setTitle("MyBox Login");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 400, 360);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("New label");
		label.setBounds(120, 0, 156, 150);
		label.setIcon(new ImageIcon(LoginGUI.class.getResource("/images/logo2.png")));
		contentPane.add(label);
		
		JLabel lblUsername = new JLabel("Username:");
		lblUsername.setFont(new Font("Arial", Font.BOLD, 16));
		lblUsername.setBounds(76, 146, 87, 16);
		contentPane.add(lblUsername);
		
		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setFont(new Font("Arial", Font.BOLD, 16));
		lblPassword.setBounds(76, 177, 87, 16);
		contentPane.add(lblPassword);
		
		userField = new JTextField();
		userField.setBounds(163, 144, 139, 22);
		contentPane.add(userField);
		userField.setColumns(10);
		
		passwordField = new JTextField();
		passwordField.setColumns(10);
		passwordField.setBounds(163, 175, 139, 22);
		contentPane.add(passwordField);
		
		btnLogin = new JButton("Login");
		btnLogin.setIcon(new ImageIcon(LoginGUI.class.getResource("/images/login.PNG")));
		btnLogin.setBounds(94, 235, 97, 25);
		contentPane.add(btnLogin);
		
		btnCancel = new JButton("Cancel");
		btnCancel.setIcon(new ImageIcon(LoginGUI.class.getResource("/images/cancel.PNG")));
		btnCancel.setBounds(203, 235, 97, 25);
		contentPane.add(btnCancel);
		
		btnForgotYourPassword = new JButton("Forgot your password?");
		btnForgotYourPassword.setIcon(new ImageIcon(LoginGUI.class.getResource("/images/update.png")));
		btnForgotYourPassword.setForeground(Color.RED);
		btnForgotYourPassword.setBounds(94, 265, 206, 25);
		contentPane.add(btnForgotYourPassword);
		
		btnChangePassword = new JButton("Change Password");
		btnChangePassword.setIcon(new ImageIcon(LoginGUI.class.getResource("/images/Password_protection_symbol_on_monitor_screen_24.png")));
		btnChangePassword.setForeground(Color.BLACK);
		btnChangePassword.setBounds(94, 295, 206, 25);
		contentPane.add(btnChangePassword);
		
		btnRegister = new JButton("Register");

		btnRegister.setBounds(10, 11, 87, 22);
		contentPane.add(btnRegister);
		setLocationRelativeTo(null);
		setVisible(true);
	
	}
	// Action Listeners
	
	
	public JLabel getLblwarningMessage() {
		if(lblwarningMessage == null){
			lblwarningMessage = new JLabel("user name or password is wrong");
			lblwarningMessage.setForeground(Color.RED);
			lblwarningMessage.setBounds(10, 165, 200, 30);
			lblwarningMessage.setVisible(false);
		}
		return lblwarningMessage;
	}
	public void setWarningMessageVisibleTrue() {
		lblwarningMessage.setVisible(true);	
	}
	
	public void setWarningMessageVisibleTrue(String st) {
		lblwarningMessage.setText(st);
		lblwarningMessage.setForeground(Color.RED);
		lblwarningMessage.setBounds(10, 165, 245, 30);
		lblwarningMessage.setVisible(true);	
		
	}
	
	
	
	public void undisplayWarningMessage() {
		lblwarningMessage.setVisible(false);
		
	}
	
	
	public void addRegisterActionListener(ActionListener e)
	{
		btnRegister.addActionListener(e);
	}
	
	
	public void addLoginActionListener(ActionListener e)
	{
		btnLogin.addActionListener(e);
	}
	
	public void addCancelActionListener(ActionListener e)
	{
		btnCancel.addActionListener(e);
	}
	
	public void addbtnForgotYourPasswordActionListener(ActionListener e)
	{
		btnForgotYourPassword.addActionListener(e);
	}

	public void addbtnChangePasswordActionListener(ActionListener e)
	{
		btnChangePassword.addActionListener(e);
	}
	
	// Getters
	
	public String getUserField() {
		return userField.getText();
	}

	public String getPasswordField() {
		return passwordField.getText();
	}
}
