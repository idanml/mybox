package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.Toolkit;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.ImageIcon;

import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.ActionListener;
import java.awt.Color;

import javax.swing.JTextField;
import javax.swing.JCheckBox;
import javax.swing.JRadioButton;

public class ChangePermissionsGUI extends JFrame {

	private JPanel contentPane;
	private JButton btnHome;
	private JButton btnManageFiles;
	private JButton btnManageGroups;
	private JButton btnUpdatePermissions;
	private JButton btnCancel;
	private JLabel lblNewFileName;
	private JRadioButton rdbtnjustme;
	private JRadioButton rdbtngruops;
	private JRadioButton rdbtneveryone;
	private String selected;
	private ButtonGroup group;
	
	/**
	 * Launch the application. In this window user can change the file permission.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ChangePermissionsGUI frame = new ChangePermissionsGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ChangePermissionsGUI() {
		setResizable(false);
		setTitle("MyBox");
		setIconImage(Toolkit.getDefaultToolkit().getImage(ChangePermissionsGUI.class.getResource("/images/logo2.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("New label");
		label.setBounds(0, 0, 0, 0);
		contentPane.add(label);
		
		JLabel lblLogo = new JLabel("Logo");
		lblLogo.setIcon(new ImageIcon(ChangePermissionsGUI.class.getResource("/images/bar.png")));
		lblLogo.setBounds(0, 0, 794, 79);
		contentPane.add(lblLogo);
		
		btnHome = new JButton("Home");
		btnHome.setIcon(new ImageIcon(ChangePermissionsGUI.class.getResource("/images/School_Building_with_Flag_32 (1).png")));
		btnHome.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnHome.setBounds(10, 81, 252, 79);
		contentPane.add(btnHome);
		
		btnManageFiles = new JButton("Manage Files");
		btnManageFiles.setForeground(SystemColor.control);
		btnManageFiles.setBackground(SystemColor.activeCaptionBorder);
		btnManageFiles.setIcon(new ImageIcon(ChangePermissionsGUI.class.getResource("/images/Three_Document_Folders_32 (1).png")));
		btnManageFiles.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnManageFiles.setBounds(268, 81, 252, 79);
		contentPane.add(btnManageFiles);
		
		btnManageGroups = new JButton("Manage Groups");
		btnManageGroups.setIcon(new ImageIcon(ChangePermissionsGUI.class.getResource("/images/College_Classroom_32.png")));
		btnManageGroups.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnManageGroups.setBounds(528, 81, 252, 79);
		contentPane.add(btnManageGroups);
		
		btnUpdatePermissions = new JButton("Update Permissions");
		btnUpdatePermissions.setIcon(new ImageIcon(ChangePermissionsGUI.class.getResource("/images/Mathematical_Symbols_24.png")));
		btnUpdatePermissions.setBounds(192, 430, 183, 33);
		contentPane.add(btnUpdatePermissions);
		
		btnCancel = new JButton("Cancel");
		btnCancel.setIcon(new ImageIcon(ChangePermissionsGUI.class.getResource("/images/cancel.PNG")));
		btnCancel.setBounds(403, 430, 183, 33);
		contentPane.add(btnCancel);
		
		lblNewFileName = new JLabel("Choose permission");
		lblNewFileName.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewFileName.setBounds(326, 225, 120, 16);
		contentPane.add(lblNewFileName);
		
		rdbtnjustme = new JRadioButton("Just me");
		rdbtnjustme.setBounds(326, 258, 109, 23);
		contentPane.add(rdbtnjustme);
		
		rdbtngruops = new JRadioButton("my Gruops members");
		rdbtngruops.setBounds(326, 284, 171, 23);
		contentPane.add(rdbtngruops);
		
		selected="0";
		
		rdbtneveryone = new JRadioButton("Everyone");
		rdbtneveryone.setBounds(326, 310, 109, 23);
		contentPane.add(rdbtneveryone);
		group = new ButtonGroup();
		group.add(rdbtnjustme);
		group.add(rdbtngruops);
		group.add(rdbtneveryone);
		
		setLocationRelativeTo(null);
		setVisible(true);
	}
	
	// Action Listeners
	
	public void addbtnHomeActionListener(ActionListener e)
	{
		btnHome.addActionListener(e);
	}
	
	public void addbtnManageFilesActionListener(ActionListener e)
	{
		btnManageFiles.addActionListener(e);
	}
	
	public void addbtnManageGroupsActionListener(ActionListener e)
	{
		btnManageGroups.addActionListener(e);
	}
	
	public void addbtnUpdatePermissionsActionListener(ActionListener e)
	{
		btnUpdatePermissions.addActionListener(e);
	}
	
	public void addbtnCancelActionListener(ActionListener e)
	{
		btnCancel.addActionListener(e);
	}

	
	public String getSelected(){
		if(rdbtnjustme.isSelected())
			selected="1";
		if(rdbtngruops.isSelected())
			selected="2";
		if(rdbtneveryone.isSelected())
			selected="3";
		return selected;
	}

}
