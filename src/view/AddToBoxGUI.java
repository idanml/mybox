package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.Choice;

import javax.swing.JLabel;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.JButton;
import javax.swing.ImageIcon;

public class AddToBoxGUI extends JFrame {

	private JPanel contentPane;
	private JButton btnAddToBox;
	private JButton btnCancel;
	private Choice choice;
	/**
	 * Create the frame. Windows that let you choose where you want to add the file inside your box.
	 */
	public AddToBoxGUI(Object F) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 429, 166);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		choice = new Choice();
		choice.setBounds(150, 14, 250, 20);
		contentPane.add(choice);
		ArrayList<String> folders = new ArrayList<String>(Arrays.asList(((String) F).split(" ")));
		for (int i = 1; i < folders.size(); i++) 
			choice.add((folders.get(i)).toString());
		
		
		
		
		JLabel label = new JLabel("Choose Location:");
		label.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label.setBounds(10, 14, 138, 16);
		contentPane.add(label);
		
		btnAddToBox = new JButton("Add to Box");
		btnAddToBox.setIcon(new ImageIcon(AddToBoxGUI.class.getResource("/images/Worker_loading_boxes_24.png")));
		btnAddToBox.setBounds(48, 84, 146, 33);
		contentPane.add(btnAddToBox);
		
		btnCancel = new JButton("Cancel");
		btnCancel.setIcon(new ImageIcon(AddToBoxGUI.class.getResource("/images/cancel.PNG")));
		btnCancel.setBounds(222, 84, 146, 33);
		contentPane.add(btnCancel);
		setLocationRelativeTo(null);
		setVisible(true);
	}
	
	public void addbtnAddToBoxActionListener(ActionListener e)
	{
		btnAddToBox.addActionListener(e);
	}
	
	public void addbtnCancelActionListener(ActionListener e)
	{
		btnCancel.addActionListener(e);
	}

	public Choice getChoice() {
		return choice;
	}

	public void setChoice(Choice choice) {
		this.choice = choice;
	}
	
	public String getFileLocationField() {
		return choice.getItem(choice.getSelectedIndex());

	}
	
	
	
	
}
