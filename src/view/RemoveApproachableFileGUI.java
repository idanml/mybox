package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.Toolkit;

import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.ImageIcon;

import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.ActionListener;
import java.awt.Color;

import javax.swing.JTextField;

import java.awt.List;
import java.awt.Checkbox;
import java.awt.Choice;
import java.util.ArrayList;
import java.util.Arrays;

public class RemoveApproachableFileGUI extends JFrame {

	private JPanel contentPane;
	private JButton btnHome;
	private JButton btnManageFiles;
	private JButton btnManageGroups;
	private JButton btnRemoveFile;
	private JButton btnCancel;
	private JLabel lblFiles;
	private Choice files;
	
	/**
	 * Create the frame.  In this window user can remove his sapprochable files.
	 */
	public RemoveApproachableFileGUI(Object F) {
		setResizable(false);
		setTitle("MyBox");
		setIconImage(Toolkit.getDefaultToolkit().getImage(RemoveApproachableFileGUI.class.getResource("/images/logo2.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("New label");
		label.setBounds(0, 0, 0, 0);
		contentPane.add(label);
		
		JLabel lblLogo = new JLabel("Logo");
		lblLogo.setIcon(new ImageIcon(RemoveApproachableFileGUI.class.getResource("/images/bar.png")));
		lblLogo.setBounds(0, 0, 794, 79);
		contentPane.add(lblLogo);
		
		btnHome = new JButton("Home");
		btnHome.setIcon(new ImageIcon(RemoveApproachableFileGUI.class.getResource("/images/School_Building_with_Flag_32 (1).png")));
		btnHome.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnHome.setBounds(10, 81, 252, 79);
		contentPane.add(btnHome);
		
		btnManageFiles = new JButton("Manage Files");
		btnManageFiles.setForeground(SystemColor.control);
		btnManageFiles.setBackground(SystemColor.activeCaptionBorder);
		btnManageFiles.setIcon(new ImageIcon(RemoveApproachableFileGUI.class.getResource("/images/Three_Document_Folders_32 (1).png")));
		btnManageFiles.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnManageFiles.setBounds(268, 81, 252, 79);
		contentPane.add(btnManageFiles);
		
		btnManageGroups = new JButton("Manage Groups");
		btnManageGroups.setIcon(new ImageIcon(RemoveApproachableFileGUI.class.getResource("/images/College_Classroom_32.png")));
		btnManageGroups.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnManageGroups.setBounds(528, 81, 252, 79);
		contentPane.add(btnManageGroups);
		
		btnRemoveFile = new JButton("Remove File");
		btnRemoveFile.setIcon(new ImageIcon(RemoveApproachableFileGUI.class.getResource("/images/Wastepaper_basket_24.png")));
		btnRemoveFile.setBounds(229, 430, 146, 33);
		contentPane.add(btnRemoveFile);
		
		btnCancel = new JButton("Cancel");
		btnCancel.setIcon(new ImageIcon(RemoveApproachableFileGUI.class.getResource("/images/cancel.PNG")));
		btnCancel.setBounds(403, 430, 146, 33);
		contentPane.add(btnCancel);
		
		lblFiles = new JLabel("Choose File to Remove:");
		lblFiles.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblFiles.setBounds(305, 254, 146, 16);
		contentPane.add(lblFiles);
		
		
		ArrayList<String> AppFiles = new ArrayList<String>(Arrays.asList(((String) F).split("@@")));
		AppFiles.remove(0);
		
		files = new Choice();
		files.setBounds(268, 276, 220, 79);
		contentPane.add(files);
		for (int i = 0; i < AppFiles.size(); i++) 
			files.add((AppFiles.get(i)).toString());
		setLocationRelativeTo(null);
		setVisible(true);
	}
	
	// Action Listeners
	
	public void addbtnHomeActionListener(ActionListener e)
	{
		btnHome.addActionListener(e);
	}
	
	public void addbtnManageFilesActionListener(ActionListener e)
	{
		btnManageFiles.addActionListener(e);
	}
	
	public void addbtnManageGroupsActionListener(ActionListener e)
	{
		btnManageGroups.addActionListener(e);
	}
	
	public void addbtnRemoveFileActionListener(ActionListener e)
	{
		btnRemoveFile.addActionListener(e);
	}
	
	public void addbtnCancelActionListener(ActionListener e)
	{
		btnCancel.addActionListener(e);
	}


	
	// Getters
	
	public Choice getFiles() {
		return files;
	}

	public void setFiles(Choice files) {
		this.files = files;
	}
	
	public String getFileField() {
		return files.getItem(files.getSelectedIndex());

	}
	
	
}
