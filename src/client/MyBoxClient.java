

package client;

import server.*;
import view.*;
import models.Envelope;
import models.Folders;
import models.Group;
import models.Request;
import models.User;
import ocsf.client.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.awt.Window;
import java.io.*;

import javax.swing.JOptionPane;

import controllers.*;


/**
 * This class overrides some of the methods defined in the abstract
 * superclass in order to give more functionality to the client.

 */
public class MyBoxClient extends ObservableClient
{
  //Instance variables **********************************************
  
  /**
   * The interface type variable.  It allows the implementation of 
   * the display method in the client.
   */
  ChatIF clientUI; 
  ArrayList<String> Ar;
  private Object currController;
  private User currUser = null;


  
  //Constructors ****************************************************
  
/**
   * Constructs an instance of the chat client.
   *
   * @param host The server to connect to.
   * @param port The port number to connect on.
   * @param clientUI The interface type variable.
   */
  
  public MyBoxClient(String host, int port) 

    throws IOException 
  {
    super(host, port); //Call the superclass constructor

    Ar = new ArrayList<>();
    openConnection();
  }
	private LoginGUI loginG;
	private ManageFilesController MFC;
  //Instance methods ************************************************
    
  /**
   * This method handles all data that comes in from the server.
   *
   * @param msg The message from the server.
   */
  public synchronized void handleMessageFromServer(Object message)  
  {
	
		if(message instanceof ArrayList<?> )
		{
			
			
			if( ((((ArrayList<Request>) message).get(0)) instanceof Request)){

				((AdminManageFilesController)currController).OpenGroups(message);
			}
			

			else

			((LoginController)currController).handleDBResult(message);

		}//if
	

	
	if(message instanceof String ){ 
		
	if(((String)message).equals("UserOrPassIncorrect") ){

		 ((LoginController)currController).handleDBResult(message);

	}
	
	if(((String)message).equals("RequestApproved") ){
 
		 ((GroupJoinRequestApprovingController)currController).handleDBResult(message);
	}
	
	
	if(((String)message).equals("RequestRejected") ){

		 ((GroupJoinRequestApprovingController)currController).handleDBResult(message);
	}
	
	if(((String)message).equals("NoRequest") ){
		((AdminManageFilesController)currController).OpenGroups(message);
	}
	
	
	
	if(((String)message).equals("NoUser") ){
		 JOptionPane.showMessageDialog(null,"No Such User!","Error", JOptionPane.ERROR_MESSAGE);

	}
	
	if(((String)message).equals("ForgotPassword") ){
		 JOptionPane.showMessageDialog(null,"Password Sent To Your Registered Email!","Error", JOptionPane.ERROR_MESSAGE);
		 ((ForgotPasswordController)currController).handleDBResult(message);
	}
	
	
	if(((String)message).equals("ForgotPasswordFalse") ){
		 JOptionPane.showMessageDialog(null,"No Such User!","Error", JOptionPane.ERROR_MESSAGE);
		 ((ForgotPasswordController)currController).handleDBResult(message);
	}
	
	if(((String)message).equals("UserOrPassIncorrectChange") ){

		 JOptionPane.showMessageDialog(null,"User Or Passwod Incorrect","Error", JOptionPane.ERROR_MESSAGE);
	
	}
	
	
	if(((String)message).equals("PasswordChange") ){

		 ((ChangePasswordController)currController).handleDBResult(message);

	}
	
	
	if(((String) message).startsWith("SendFolders") ) 
	{
		((ManageFilesController)currController).handleDBResult(message);
	}//if
	
	
	if(((String) message).startsWith("2SendFolders") )
	{
		((WatchApproachableFileListController)currController).handleDBResult3(message);
	}//if
	
	if(((String) message).startsWith("3SendFolders") ) 
	{
		((FileInfoEditController)currController).handleDBResult(message);
	}//if

	if(((String) message).startsWith("FileInfoEdit") ) 
	{  
		((ManageFilesController)currController).handleDBResult3(message);
	}
	

	
	
	if(((String)message).equals("AddFile") ){		
		 ((AddFileController)currController).handleDBResult(message);
	}
	
	if(((String)message).equals("AddFileFalse") ){
		 ((AddFileController)currController).handleDBResult(message);
	}
	
	

	
	
	if(((String)message).equals("AddToBox") ){		
		 ((AddToBoxController)currController).handleDBResult(message);
	}
	
	if(((String)message).equals("AddToBoxFalse") ){
		 ((AddToBoxController)currController).handleDBResult(message);
	}
	

	if(((String) message).equals("JoinGroupReq") )
	{  
		((JoinGroupsController)currController).handleDBResult(message);
	}
	
	if(((String) message).equals("JoinGroupReqFailed") )
	{  
		((JoinGroupsController)currController).handleDBResult(message);
	}
	
	if(((String) message).equals("LeaveGroupReq") )
	{  
		((LeaveGroupsController)currController).handleDBResult(message);
	}
	
	if(((String) message).equals("LeaveGroupReqFailed") )
	{  
		((LeaveGroupsController)currController).handleDBResult(message);
	}
	
	if(((String) message).startsWith("SendUnregGroups") )
	{  
		((ManageGroupsController)currController).handleDBResult2(message);
	}//if
	
	if(((String) message).startsWith("SendRegGroups") )
	{  
		((ManageGroupsController)currController).handleDBResult3(message);
	}//if
	
	if(((String) message).startsWith("5SendFolders") )
	{  
		
		((ManageFoldersController)currController).handleDBResult1(message);
	}//if
	
	if(((String) message).startsWith("6SendFolders") )
	{  
		
		((ManageFoldersController)currController).handleDBResult6(message);
	}//if
	
	
	if(((String) message).startsWith("SendFiles") ) 
	{  
		((DeleteFileController)currController).handleDBResult(message);
	}//if
	
	if(((String)message).equals("ChangePermission") ){	 
		 ((ChangePermissionsController)currController).handleDBResult(message);
	}
	
	
	if(((String)message).equals("ChangePermission1") ){	 
		 ((ChangePermissionsController)currController).handleDBResult(message);
	}
	
	if(((String)message).equals("ChangeDescription") ){	 
		 ((ChangeDescriptionController)currController).handleDBResult(message);
	}
	
	
	if(((String)message).equals("ChangeFileLocation") ){	 
		 ((ChangeFileLocationController)currController).handleDBResult(message);
	}
	
	
	if(((String)message).equals("Rename") ){	 
		 ((RenameController)currController).handleDBResult(message);
	}
	
	if(((String)message).equals("RenameFileFalse") ){
		 ((RenameController)currController).handleDBResult(message);
	}
	
	if(((String)message).equals("RenameAdmin") ){	 
		 ((AdminRenameController)currController).handleDBResult(message);
	}
	
	if(((String)message).equals("RenameAdminFileFalse") ){
		 ((AdminRenameController)currController).handleDBResult(message);
	}
	

	if(((String)message).equals("AdminChangeDescription") ){	 
		 ((AdminChangeDescriptionController)currController).handleDBResult(message);
	}
	
	if(((String) message).startsWith("GetApproachFiles") ) 
	{  
		((ManageFilesController)currController).handleDBResult2(message);
	}//if
	
	
	if(((String) message).startsWith("DeGetApproachFiles") ) 
	{  
		((DeleteFileController)currController).handleDBResult2(message);
	}//if
	
	
	if(((String)message).equals("DeleteApproachFile") ){		
		 ((RemoveApproachableFileController)currController).handleDBResult(message);
	}
	
	if(((String)message).equals("DeleteApproachFileFalse") ){
		 ((RemoveApproachableFileController)currController).handleDBResult(message);
	}
	
	if(((String)message).equals("OpenFile") ){
		 ((WatchApproachableFileListController)currController).handleDBResult2(message);
	}
	
	
	if(((String)message).equals("OpenFile2") ){

		 ExplorerController.handleDBResult2(message);
	}
	
	
	if(((String)message).equals("OpenFile3") ){
		 ((SearchResultController)currController).handleDBResult2(message);
	}
	
	
	
	if(((String)message).equals("OpenFileAdmin") ){
		 ((AdminManageFilesController)currController).handleDBResult2(message);
	}
	
	
	
	if(((String)message).equals("UpdateFile") ){

		((WatchApproachableFileListController)currController).UpdateFile(message);
	}
	
	
	
	if(((String)message).equals("UpdateFileFalse") ){

		((WatchApproachableFileListController)currController).UpdateFile(message);
	}
	
	
	
	if(((String)message).equals("UpdateFile2") ){

		((SearchResultController)currController).UpdateFile(message);
	}
	
	
	
	if(((String)message).equals("UpdateFileFalse2") ){

		((SearchResultController)currController).UpdateFile(message);
	}
	
	if(((String)message).equals("UpdateFile3") ){

		((AdminManageFilesController)currController).UpdateFile(message);
	}
	
	
	
	if(((String)message).equals("UpdateFileFalse3") ){

		((AdminManageFilesController)currController).UpdateFile(message);
	}
	
	if(((String) message).startsWith("DeleteFile") ) 
	{
		((HardDeleteFilesController)currController).handleDBResult(message);
	}//if
	
	

	
	if(((String) message).startsWith("AdminDeleteFile") ) 
	{
		((AdminManageFilesController)currController).DeleteAdmin(message);
	}//if
	
	if(((String) message).startsWith("GetSearchFiles") ) 
	{
		((SearchFileController)currController).ShowSearch(message);
	}//if
	

	if(((String) message).startsWith("GetGroups") ) 
	{      
		((ManageGroupsController)currController).handleDBResult(message);
	}//if
	
	
	
	if(((String) message).startsWith("GetFilesNotRoot") )
	{  
		ExplorerController.Handler(message);


	}
	
	if(((String) message).startsWith("GetFiles2Root") )
	{  
		((GoHome)currController).Handler2(message);
	
		

	}
	
	

	if(((String) message).startsWith("GetFilesForAdmin") ) //user name and password is found
	{      
		
		((AdminManageFilesController)currController).Reopen(message);
	}//if
	
	if(((String) message).startsWith("2GetFilesForAdmin") ) //user name and password is found
	{      
		
		((AdminRenameController)currController).Reopen(message);
		
	}//if
	
	if(((String) message).startsWith("3GetFilesForAdmin") ) //user name and password is found
	{      
	
		((AdminChangeDescriptionController)currController).Reopen(message);
	}//if
	
	if(((String) message).startsWith("Register") ) 
	{      
		String Got = message.toString();
		String arr[] = Got.split(" ", 2);

		String Send = arr[1];     
		
		
		
		((RegisterController)currController).Register(Send);
	}//if
	
	if(((String) message).startsWith("ManageFolders") ) 
	{      
		
		((ManageFilesController)currController).HandleDBResult4(message);
	}//if	
	
	
	
if(((String)message).equals("DeleteFolder") ){
	      
		
		((ManageFoldersController)currController).handleDBResult(message);
	}//if
	
	if(((String)message).equals("1DeleteFolder") ){
	      
		
		((ManageFoldersController)currController).HandleDBResult4(message);
	}//if
	
	if(((String)message).equals("AddFolder") ){

		((AddFolderController)currController).handleDBResult1(message);
	}//if
	
	if(((String)message).equals("AddFolderFalse") ){

		((AddFolderController)currController).handleDBResult1(message);
	}//if
	
	if(((String)message).equals("1AddFolder") ){

		((AddFolderController)currController).HandleDBResult4(message);
	}//if
	
	if(((String)message).equals("RenameFolder") ){

		((RenameFolderController)currController).handleDBResult(message);
	}//if
	
	if(((String)message).equals("RenameFolderFalse") ){

		((RenameFolderController)currController).handleDBResult(message);
	}//if
	
	if(((String)message).equals("1RenameFolder") ){

		((RenameFolderController)currController).HandleDBResult4(message);
	}//if
	
	if(((String)message).equals("ChangeFolderLocation") ){

		((ChangeFolderLocationController)currController).handleDBResult(message);
	}//if
	
	if(((String)message).equals("1ChangeFolderLocation") ){

		((ChangeFolderLocationController)currController).HandleDBResult4(message);
	}//if
	
	
	}
	notify();   
  }
  

  public void handleMessageFromClientUI(String message)
  {
    try
    {
    	sendToServer(message);
    }
    catch(IOException e)
    {
      clientUI.display
        ("Could not send message to server.  Terminating client.");
      quit();
    }
  }
  
  /**
   * This method terminates the client.
   */
  
  public Object getCurrObj() {
	return currController;
}
  
  public User getCurrUser() {
		return currUser;
	}


	public  void setCurrUser(User currUser) {
		this.currUser = currUser;
	}


public void setCurrObj(Object currObj) {
	this.currController = currObj;
}
  public void quit()
  {
    try
    {
      closeConnection();
    }
    catch(IOException e) {}
    System.exit(0);
  }
}
//End of ChatClient class
