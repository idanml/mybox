package client;

import models.LoginMod;
import models.MyBoxClientModel;
import controllers.LoginController;
import controllers.MyBoxClientController;
import view.LoginGUI;
import view.MyBoxClientGUI;

/**
 * The Main Class That starts MyBox Application.
 *
 */
public class MyBoxApp {

	public static MyBoxClient clien;

	public static void main(String[] args) {
		
		MyBoxClientGUI clientView = new MyBoxClientGUI();
		MyBoxClientModel clientModel = new MyBoxClientModel();
        MyBoxClientController clientController = new MyBoxClientController(clientView,clientModel);

	}

}
