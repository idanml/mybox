package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import client.MyBoxApp;
import models.Envelope;
import models.File;
import models.User;
import view.MainWindowGUI;
import view.ManageFilesGUI;
import view.ManageGroupsGUI;
import view. SearchFileGUI;
import view.SearchResultGUI;



/**
 * Controller of the search file case, handeles all the events on the relavent GUI.
 *
 */
public class SearchFileController extends  AbstractTransfer {
	
	private User user;
	private LoginController lc;
	private  SearchFileController sfc;
	private  SearchFileGUI sfg;
	
	
	/**
	 * The constructor of the class, construct the listeners and attributes of the event of rename file.
	 * @param user The instance of the logged in user.
	 * @param lc The login controller of the specific login of the user.
	 * @param sfg The relavent GUI.
	 */
	public  SearchFileController(User user, LoginController lc,  SearchFileGUI sfg) {
		this.user = user;
		this.lc = lc;
		this.sfc = this;
		this.sfg = sfg;
		sfg.addbtnHomeActionListener(new HomeListener());
		sfg.addbtnManageGroupsActionListener(new ManageGroupListener());
		sfg.addbtnSearchFileActionListener(new SearchFileListener());
		sfg.addbtnCancelActionListener(new CancelListener());
	}
	
	
	/**
	 * Inner class where button Home pressed , implements action listener which get you back to main window.
	 *
	 */
	class HomeListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			sfg.dispose();
			GoHome gh = new GoHome(user);
		}	
	}
	
	/**
	 * Inner class where button Manage Groups pressed , implements action listener which opens the manage groups window.
	 *
	 */
	class ManageGroupListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			sfg.dispose();
			ManageGroupsGUI mfg = new ManageGroupsGUI();
			ManageGroupsController mgc = new ManageGroupsController(user,lc,mfg);		
		}	
	}
	
	
	
	/**
	 * Inner class where button Search File pressed , implements action listener which connects the server to search the file.
	 *
	 */
	class SearchFileListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			String Use=user.getUserName();
			File FileL=new File (sfg.getFileNameField(), null, null, null, null);
			try{
				Envelope ev1 = new Envelope(FileL,"GetSearchFiles",Use);
				sendToServer(ev1);
				MyBoxApp.clien.setCurrObj(sfc);
			}
			catch(Exception e1){
				JOptionPane.showMessageDialog(null,"Erorr Connecting To Server!","Error", JOptionPane.ERROR_MESSAGE);       
			}
		}	
	}
	
	
	/**
	* Inner class where button Cancel pressed , implements action listener which cancel all the user selects and opens back the manage files window
	*
	*/	 
	class CancelListener implements ActionListener
	{

		public void actionPerformed(ActionEvent e) {
			sfg.dispose();
			ManageFilesGUI mfg = new ManageFilesGUI();
			ManageFilesController mfc = new ManageFilesController(user,lc,mfg);	
		}	
	}
	
	
	/**
	* Inner class where button Show Result pressed , implements action listener which opens the result window.
	* @param message Answer of the db as Object.
	*
	*/	 
	public void ShowSearch(Object message){
		sfg.dispose();
		SearchResultGUI srg = new SearchResultGUI(message);
		SearchResultController src = new SearchResultController(user,lc,srg);
		
		
	}
	
	
	
	
	
	
	
	
	
	
}