package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import models.User;
import view.MainWindowGUI;
import view.ManageFilesGUI;
import view.ManageGroupsGUI;
import view.SearchResultGUI;
import view.UpdateFileGUI;


/**
 * 
 * 
 *
 */
public class UpdateFileController extends  AbstractTransfer {

	
	private User user;
	private LoginController lc;
	private UpdateFileController ufc;
	private UpdateFileGUI ufg;
	
	public UpdateFileController(User user, LoginController lc, UpdateFileGUI ufg) {
		this.user = user;
		this.lc = lc;
		this.ufc = this;
		this.ufg = ufg;
		ufg.addbtnHomeActionListener(new HomeListener());
		ufg.addbtnManageGroupsActionListener(new ManageGroupListener());
		ufg.addbtnUpdateFileActionListener(new UpdateFileListener());
		ufg.addbtnCancelActionListener(new CancelListener());

	}
	
	/**
	 * Inner class where button Home pressed , implements action listener which get you back to main window.
	 *
	 */
	class HomeListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			ufg.dispose();
			GoHome gh = new GoHome(user);
		}	
	}
	
	
	/**
	 * Inner class where button Manage Groups pressed , implements action listener which opens the manage groups window.
	 *
	 */
	class ManageGroupListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			ufg.dispose();
			ManageGroupsGUI mfg = new ManageGroupsGUI();
			ManageGroupsController mgc = new ManageGroupsController(user,lc,mfg);		
		}	
	}
	
	
	/**
	 * Inner class where button Update File pressed , implements action listener which upload 
	 *
	 */
	class UpdateFileListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			
			java.io.File selectedFile;
			String loc;
			String fname="";
			
			JFileChooser chooser = new JFileChooser();
			int returnVal = chooser.showOpenDialog(null);
		    if(returnVal == JFileChooser.APPROVE_OPTION){
		    	selectedFile = chooser.getSelectedFile();
		    	fname = selectedFile.getName();
	  		   
	 		    loc= selectedFile.getPath();
		    }
			  if(JOptionPane.showConfirmDialog(null,"This Update Will Delete The Existing File\n Are You Sure ?",
					  "Update", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {  
				  JOptionPane.showMessageDialog(null,"Yes!","Error", JOptionPane.ERROR_MESSAGE); 
				  return;
				} else {
					  return;
				}
			  
			  
		}	
	}
	
	class CancelListener implements ActionListener
	{

		public void actionPerformed(ActionEvent e) {
			ufg.dispose();
			ManageFilesGUI mfg = new ManageFilesGUI();
			ManageFilesController mfc = new ManageFilesController(user,lc,mfg);	
		}	
	}
}
