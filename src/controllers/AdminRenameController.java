package controllers;

import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import client.MyBoxApp;
import models.Envelope;
import models.File;
import models.User;
import view.AdminManageFilesGUI;
import view.GroupJoinRequestApprovingGUI;
import view.MainWindowGUI;
import view.ManageFilesGUI;
import view.AdminRenameGUI;
import controllers.ManageFilesController.AddFileListener;
import controllers.ManageFilesController.DeleteFileListener;
import controllers.ManageFilesController.FileInfoEditListener;
import controllers.ManageFilesController.HomeListener;
import controllers.ManageFilesController.ManageFoldersListener;
import controllers.ManageFilesController.ManageGroupListener;
import controllers.ManageFilesController.SearchFileListener;
import controllers.ManageFilesController.WatchApprochableFilesListener;



/**
 * Controller of the rename file case at admin mode, handeles all the events on the relavent GUI.
 *
 */
public class AdminRenameController extends  AbstractTransfer {
	
	private User user;
	private LoginController lc;
	private AdminRenameGUI arg;
	private AdminRenameController arc; 
	private String fileName;

	
	/**
	 * The constructor of the class, construct the listeners and attributes of the event of rename file.
	 * @param user The instance of the logged in user.
	 * @param rg The relavent GUI.
	 * @param fileName The name of the file that gonna be changed.
	 */
	public AdminRenameController(User user, AdminRenameGUI arg, String fileName) {
		this.user = user;
		this.lc = lc;
		this.arc =this ;
		this.arg = arg;
		this.fileName=fileName;
		arg.addbtnManageFilesActionListener(new ManageFilesListener());
		arg.addbtnManageGroupsActionListener(new ManageGroupListener());
		arg.addbtnRenameActionListener(new RenameListener());
		arg.addbtnCancelActionListener(new CancelListener());
	}
	
	class ManageFilesListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {

		}	
	}
	
	class ManageGroupListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {

			
			
		}	
	}
	
	
	/**
	 * Inner class where button Rename File pressed , implements action listener which connects the server to change the file name.
	 * @author Netanel
	 *
	 */
	class RenameListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			if(arg.getFileNameField().equals("")){
				JOptionPane.showMessageDialog(null,"Enter new Name!","Error", JOptionPane.ERROR_MESSAGE);    
	  			  return;
			}
				
			 try{  
				  File FileL = new File(fileName, arg.getFileNameField(), null, null,null );
					 Envelope ev1 = new Envelope(FileL,"RenameAdmin","Admin");	 
					 sendToServer(ev1);
					 MyBoxApp.clien.setCurrObj(arc);
			  }
			  catch(Exception e1){
				  JOptionPane.showMessageDialog(null,"Erorr Connecting To Server!","Error", JOptionPane.ERROR_MESSAGE);    
				  
			  }
		}	
	}
	
	
	/**
	 *  Method to handle the answer of the db after trying to change the file name.
	 *  @param message Answer of the db as Object.
	 */
	 public void handleDBResult(Object message) {
		 String str = (String)message;

		 if(str.equals("RenameAdminFileFalse")){
			 JOptionPane.showMessageDialog(null,"File Name Already Exists!","Error", JOptionPane.ERROR_MESSAGE);
				// AFG.dispose();	 
			 }
		 else{
			 	JOptionPane.showMessageDialog(null,"Rename","Success", JOptionPane.INFORMATION_MESSAGE);
				File FileL = new File(null, null, null, null, null);
				 try{
					 Envelope ev1 = new Envelope(FileL,"GetFilesForAdmin2","Admin");	 
					 sendToServer(ev1);
					 MyBoxApp.clien.setCurrObj(arc);
					 }
				 catch(Exception e){
					 JOptionPane.showMessageDialog(null,"Erorr Connecting To Server!","Error", JOptionPane.ERROR_MESSAGE); 
				 }
			}
				 
	 }
	 
		/**
		*  Method to handle the answer of the db after pulling the file list for admin.
		*  Opens the Manage files window.
		*  @param message Answer of the db as Object.
		*/ 
	 public void Reopen(Object message)
	 {
	 	 arg.dispose();	
	 	 message = ((String) message).substring(17);
	 	 AdminManageFilesGUI amfg = new AdminManageFilesGUI(message);	 
	 	 AdminManageFilesController amfc = new AdminManageFilesController(user,amfg,message);		
	 }
	 
	 
		/**
		* Inner class where button Cancel pressed , implements action listener which cancel all the user selects and opens back the manage files window
		*
		*/	 
	 class CancelListener implements ActionListener
		{
		 	public void actionPerformed(ActionEvent e) {
				File FileL = new File(null, null, null, null, null);
				try{
					Envelope ev1 = new Envelope(FileL,"GetFilesForAdmin2","Admin");	 
					sendToServer(ev1);
					MyBoxApp.clien.setCurrObj(arc);	
				}
				catch(Exception e1){
					 JOptionPane.showMessageDialog(null,"Erorr Connecting To Server!","Error", JOptionPane.ERROR_MESSAGE);
				}
	
			}
		}
	
		
}	
