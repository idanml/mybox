package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import client.MyBoxApp;
import controllers.ManageFilesController.HomeListener;
import controllers.ManageFilesController.ManageGroupListener;
import models.Envelope;
import models.File;
import models.User;
import view.JoinGroupsGUI;
import view.LeaveGroupsGUI;
import view.MainWindowGUI;
import view.ManageFilesGUI;
import view.ManageGroupsGUI;
import view.SeeGroupMembershipGUI;

public class ManageGroupsController extends  AbstractTransfer {
	
	private User user;
	private LoginController lc;
	private ManageGroupsController mgc;
	private ManageGroupsGUI mgg;
	private ManageGroupsController TempL;
	
	public ManageGroupsController(User user, LoginController lc, ManageGroupsGUI mgg) {
		this.user = user;
		this.lc = lc;
		this.mgc = this;
		this.mgg = mgg;
		TempL=this;
		mgg.addbtnHomeActionListener(new HomeListener());
		mgg.addbtnManageFilesActionListener(new ManageFilesListener());
		mgg.addbtnJoinGroupsActionListener(new JoinGroupsListener());
		mgg.addbtnLeaveGroupsActionListener(new LeaveGroupsListener());
		mgg.addbtnSeeGroupMembershipActionListener(new SeeGroupMembershipListener());
	}
	
	
	/**
	 * Inner class where button Home pressed , implements action listener which get you back to main window.
	 *
	 */
	class HomeListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			mgg.dispose();
			GoHome gh = new GoHome(user);
		}	
	}

	
	
	/**
	 * Inner class where button Manage Files pressed , implements action listener which opens the manage files window.
	 *
	 */
	class ManageFilesListener implements ActionListener
	{

		public void actionPerformed(ActionEvent e) {
			mgg.dispose();
			ManageFilesGUI mfg = new ManageFilesGUI();
			ManageFilesController mfc = new ManageFilesController(user,lc,mfg);	
		}	
	}
	
	/**
	 * Inner class where button Join groups pressed , implements action listener which connect to server to pull all the unregistered groups of the user.
	 *
	 */
	class JoinGroupsListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			String Use=user.getUserName();
			try{
				Envelope en= new Envelope(null,"GetUnregGroups",Use);
				sendToServer(en);
				MyBoxApp.clien.setCurrObj(mgc);
			}
			catch(Exception e1){
				JOptionPane.showMessageDialog(null,"Erorr Connecting To Server!","Error", JOptionPane.ERROR_MESSAGE); 
			}
		
		}	
	}
	
	
	/**
	 *  Method to handle the answer of the db after trying to get unregregistered groups from db.
	 *  Opens Join Group window. 
	 *  @param message Answer of the db as Object.
	 */
	public void handleDBResult2(Object message) {
		System.out.println(message.toString());
		mgg.dispose();
		JoinGroupsGUI jgg = new JoinGroupsGUI(message);
		JoinGroupsController jgc = new JoinGroupsController(user,lc,jgg);
	}
	
	
	/**
	 * Inner class where button Join groups pressed , implements action listener which connect to server to pull all the registered groups of the user.
	 *
	 */
	class LeaveGroupsListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			String Use=user.getUserName();
			try{
				Envelope en= new Envelope(null,"GetRegGroups",Use);
				sendToServer(en);
				MyBoxApp.clien.setCurrObj(mgc);		
			}
			catch(Exception e1){
				JOptionPane.showMessageDialog(null,"Erorr Connecting To Server!","Error", JOptionPane.ERROR_MESSAGE);
			}
		}
	}
	
	
	/**
	*  Method to handle the answer of the db after trying to get regregistered groups from db.
	*  Opens Leave Group window. 
	*  @param message Answer of the db as Object.
	*/
	public void handleDBResult3(Object message) {
		mgg.dispose();
		LeaveGroupsGUI lgg = new LeaveGroupsGUI(message);
		LeaveGroupsController lgc = new LeaveGroupsController(user,lc,lgg);	
	}
	
	
	/**
	* Inner class where button Join groups pressed , implements action listener which connect to server to pull all the registered groups of the user.
	*
	*/
	class SeeGroupMembershipListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			String Use=user.getUserName();
			try{
				Envelope ev1 = new Envelope(null,"GetGroups",Use);
				sendToServer(ev1);
				MyBoxApp.clien.setCurrObj(getTempL());
			}
			catch(Exception e1){
				JOptionPane.showMessageDialog(null,"Erorr Connecting To Server!","Error", JOptionPane.ERROR_MESSAGE);
			}
		}	
	}
	

	/**
	*  Method to handle the answer of the db after trying to get regregistered groups from db.
	*  Opens See Group Membership window. 
	*  @param message Answer of the db as Object.
	*/
	 public void handleDBResult(Object message) { 
			mgg.dispose();
			SeeGroupMembershipGUI sgmg = new SeeGroupMembershipGUI(message);
			SeeGroupMembershipController sgmc = new SeeGroupMembershipController(user,lc,sgmg);		
	 }
	
	
	public ManageGroupsController getTempL() {
		return TempL;
	}
	
	
	

}
