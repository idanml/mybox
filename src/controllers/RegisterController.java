package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import models.Envelope;
import models.User;
import client.MyBoxApp;
import controllers.AddFileController.HomeListener;
import view.MainWindowGUI;
import view.RegisterGUI;


/**
 * Controller for register case, handles all the events on the relavent GUI.
 *
 *
 */
public class RegisterController extends  AbstractTransfer {
	
	private RegisterController rc;
	
	private RegisterGUI rg;

	
	/**
	 * 
	 * @param rg
	 */
	public RegisterController(RegisterGUI rg){
		
		this.rg=rg;
		this.rc=this;
		rg.addbtnRegisterActionListener(new RegisterListener());
		
			
	}
	
	/**
	 * 
	 * Inner class to handle the event which Register is pressed.
	 *
	 */
	class RegisterListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			
			if(rg.getFnameField().isEmpty()==true || rg.getLnameField().isEmpty()==true || rg.getUserField().isEmpty()==true || rg.getPasswordField().isEmpty()==true || rg.getEmailField().isEmpty()==true)
			{
				JOptionPane.showMessageDialog(null,"Missing Fields!","ERROR", JOptionPane.ERROR_MESSAGE);    
				return;				
			}
			
			User U = new User(rg.getFnameField(),rg.getLnameField(),rg.getUserField(),rg.getPasswordField(),rg.getEmailField(),null);

			 Envelope ev1 = new Envelope(U,"Register",""); 
				 sendToServer(ev1);
				 MyBoxApp.clien.setCurrObj(rc);
	
		}	
	}
	
	/**
	 * Method to handle the server comment.
	 * Show relavent message to the user if worng/right details were entered.
	 * @param message
	 */
	 public void Register(Object message) {
			String flag=message.toString();
				 
			if(flag.equals("2")){	 		 
					rg.dispose();
					JOptionPane.showMessageDialog(null,"You Are Registered!","Success", JOptionPane.INFORMATION_MESSAGE);    
			}
			
			
			if(flag.equals("0")){	
				JOptionPane.showMessageDialog(null,"UserName Already Exists!","ERROR", JOptionPane.ERROR_MESSAGE);    
				return;		
			}
			
			if(flag.equals("1")){
				JOptionPane.showMessageDialog(null,"Password Must Contain At List 6 Characters!","ERROR", JOptionPane.ERROR_MESSAGE);    
				return;		
			}
			
			
			if(flag.equals("3")){	
				JOptionPane.showMessageDialog(null,"Email Already Registered In The System!","ERROR", JOptionPane.ERROR_MESSAGE);    
				return;		
			}
			
			if(flag.equals("4")){	
				JOptionPane.showMessageDialog(null,"Wrong Email Address!","ERROR", JOptionPane.ERROR_MESSAGE);    
				return;		
			}
			
	
	
	
	 }
}
