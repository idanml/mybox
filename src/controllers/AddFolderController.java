package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import client.MyBoxApp;
import models.Envelope;
import models.Folders;
import models.User;
import view. AddFolderGUI;
import view.MainWindowGUI;
import view.ManageFilesGUI;
import view.ManageFoldersGUI;
import view.ManageGroupsGUI;


/**
 * This is controller of adding folder to my box, handles all the events on relavant GUI.
 *
 */
public class AddFolderController extends  AbstractTransfer {
	
	private User user;
	private LoginController lc;
	private  AddFolderController afc;
	private  AddFolderGUI afg;
	
	
	/**
 	* The constructor of the class, construct the listeners and the attributes for adding folder event.
 	* @param user The logged-in user instance
 	* @param lc The login controller of the specific user
 	* @param afg The relavent GUI 
 	*/
	public  AddFolderController(User user, LoginController lc,  AddFolderGUI afg) {
		this.user = user;
		this.lc = lc;
		this.afc = this;
		this.afg = afg;
		afg.addbtnHomeActionListener(new HomeListener());
		afg.addbtnManageGroupsActionListener(new ManageGroupListener());
		afg.addbtnAddFolderActionListener(new AddFolderListener());
		afg.addbtnCancelActionListener(new CancelListener());
	}

	
	/**
	 * Inner class where button Home pressed , implements action listener which get you back to main window
	 *
	 */
	class HomeListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			afg.dispose();
			GoHome gh = new GoHome(user);
		}	
	}
	
	
	/**
	 * Inner class where button Manage Groups pressed , implements action listener which opens the manage groups window
	 *
	 */
	class ManageGroupListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			afg.dispose();
			ManageGroupsGUI mfg = new ManageGroupsGUI();
			ManageGroupsController mgc = new ManageGroupsController(user,lc,mfg);		
		}	
	}
	
	
	/**
	 * Inner class where button Add Folder pressed , implements action listener which start a connection process to add the folder.
	 *
	 */
	class AddFolderListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			
			
			if(afg.isFolderFieldEmpty()==true)
			{
				JOptionPane.showMessageDialog(null,"Select Name.","Error", JOptionPane.ERROR_MESSAGE);
				return ;
			}
		
			String Use=user.getUserName();
			String foname=afg.getFolderNameField();
			String loc=afg.getFolderLoc();

				
						
			Folders newfold = new Folders(Use,foname,loc);
	
				Envelope ev1 = new Envelope(newfold,"AddFolder",Use);
				sendToServer(ev1);
				MyBoxApp.clien.setCurrObj(afc);
	
			
		}	
	}
	
	
	/**
	 * Method to handle the answer of the db after adding a folder.
	 * @param message Answer of the db as Object.
	 */
	public void handleDBResult1(Object message)
	{
		String Mess=(String)message;
		if(Mess.equals("AddFolderFalse")){
			JOptionPane.showMessageDialog(null,"Folder Name Already Exists In The System","ERROR", JOptionPane.ERROR_MESSAGE);
			return;
		}
		else{
		
		JOptionPane.showMessageDialog(null,"Folder Created.","OK", JOptionPane.INFORMATION_MESSAGE);
		afg.dispose();
		ManageFilesGUI mfg = new ManageFilesGUI();
		ManageFilesController mfc = new ManageFilesController(user,lc,mfg);
		
		}
	}
	
	
	
	public void handleDBResult(Object message)
	{
		String Use=user.getUserName();
		
		try{
			Envelope ev1 = new Envelope("4","GetFolders4",Use);
			sendToServer(ev1);
			MyBoxApp.clien.setCurrObj(afc);
		}
		catch(Exception e1){
			JOptionPane.showMessageDialog(null,"Erorr Connecting To Server!","Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	
	/**
	 * Method to handle the answer of the db after adding folder.
	 * Handle a result of folder list to open the manage folder with new folder list.
	 * @param message Answer of the db as Object.
	 */
	public void HandleDBResult4(Object message)
	{
		JOptionPane.showMessageDialog(null,"Folder Created.","OK", JOptionPane.INFORMATION_MESSAGE);
		afg.dispose();
		ManageFoldersGUI mfg = new ManageFoldersGUI(message);
		ManageFoldersController mfc = new ManageFoldersController(user,lc,mfg);
	}
	
	
	
	/**
	 * Inner class where button Cancel pressed , implements action listener which cancel all the user selects and opens back the manage files window
	 *
	 */
	class CancelListener implements ActionListener
	{

		public void actionPerformed(ActionEvent e) {
			afg.dispose();
			ManageFilesGUI mfg = new ManageFilesGUI();
			ManageFilesController mfc = new ManageFilesController(user,lc,mfg);	
		}	
	}
	
}	
