package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import models.Envelope;
import models.File;
import models.LoginMod;
import models.User;
import client.MyBoxApp;
import view.*;




/**
 * Controller to change the user password, handle all the events on the relavent GUI.
 *
 */
public class ChangePasswordController extends  AbstractTransfer{
	
	private ChangePasswordGUI cpg;
	private String username;
	private String oldPass;
	private String newPass;
	private String newPassConf;
	private ChangePasswordController tempL;
	
	
	
	/**
	 * This is the constructor of the class, construct all the listeners and attributes of the changing password event.
	 * @param cpg Relavent GUI.
	 */	
	public ChangePasswordController (ChangePasswordGUI cpg)
	{
		tempL = this;
		this.cpg=cpg;
		cpg.addbtnChangePasswordActionListener(new ChangeListener());
		cpg.addCancelActionListener(new CancelListener());
	}
	
	
	
	/**
	 * Inner class where button Change password pressed , implements action listener which connects the server to change the user password over the db.
	 *
	 */	
	class ChangeListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			username=cpg.getUsername().getText();
			oldPass=cpg.getOldPassword().getText();
			newPass=cpg.getNewPassword().getText();
			newPassConf=cpg.getConfirmPassword().getText();

			if(oldPass.equals("")|| username.equals("") || newPass.equals("") || newPassConf.equals("")){
   			  	JOptionPane.showMessageDialog(null,"Empty Fields!","Error", JOptionPane.INFORMATION_MESSAGE);    
   			  	return;
			 }

			if(newPassConf.equals(newPass))
	   		  {
	   			 try{ //set the user name and password and send to server
					User user = new User(null, null, null, null, null, null);
					user.setUserName(username);
					user.setPassword(newPass);
					Envelope ev1 = new Envelope(user,"ChangePassword"+" "+oldPass,username);
					sendToServer(ev1);
					MyBoxApp.clien.setCurrObj(getTempL());
	   			  }
	   			  catch(Exception e1){
	   				 JOptionPane.showMessageDialog(null,"Erorr Connecting To Server!","Error", JOptionPane.ERROR_MESSAGE);    
	   			  }
			
		      }
	   		  else{
	   			  JOptionPane.showMessageDialog(null,"Password Confirmation Inncorect!","Error", JOptionPane.INFORMATION_MESSAGE);    
	   			  return;
				 }

			}
		
	}
	
	
	
	
	/**
	 *  Method to handle the answer of the db after trying to change the user password.
	 *  @param message Answer of the db as Object.
	 */	
	 public void handleDBResult(Object message) {
		JOptionPane.showMessageDialog(null,"Password Changed!","Success", JOptionPane.INFORMATION_MESSAGE);
		cpg.dispose();
		LoginGUI logView = new LoginGUI();
		LoginMod logModel = new LoginMod();
		LoginController logController = new LoginController(logView,logModel);
	 }

	 
	 
	/**
	* Inner class where button Cancel pressed , implements action listener which cancel all the user selects and close the window.
	*
	*/
	class CancelListener implements ActionListener
	{

		public void actionPerformed(ActionEvent e) {
			cpg.dispose();
		}
		
	}
	
	
	public ChangePasswordController getTempL() {
		return tempL;
	}
	
	
}
