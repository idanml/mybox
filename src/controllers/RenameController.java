package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import client.MyBoxApp;
import models.Envelope;
import models.File;
import models.User;
import view.MainWindowGUI;
import view.ManageFilesGUI;
import view.ManageGroupsGUI;
import view. RenameGUI;


/**
 * Controller of the rename file case, handeles all the events on the relavent GUI.
 *
 */
public class RenameController extends  AbstractTransfer {
	
	private User user;
	private LoginController lc;
	private  RenameController rc;
	private  RenameGUI rg;
	private String fileName;
	
	
	/**
	 * The constructor of the class, construct the listeners and attributes of the event of rename file.
	 * @param user The instance of the logged in user.
	 * @param lc The login controller of the specific login of the user.
	 * @param rg The relavent GUI.
	 * @param fileName The name of the file that gonna be changed.
	 */
	public  RenameController(User user, LoginController lc,  RenameGUI rg, String fileName) {
		this.user = user;
		this.lc = lc;
		this.rc = this;
		this.rg = rg;
		this.fileName=fileName;
		rg.addbtnHomeActionListener(new HomeListener());
		rg.addbtnManageGroupsActionListener(new ManageGroupListener());
		rg.addbtnRenameFileActionListener(new RenameFileListener());
		rg.addbtnCancelActionListener(new CancelListener());
	}
	
	
	/**
	 * Inner class where button Home pressed , implements action listener which get you back to main window.
	 *
	 */
	class HomeListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			rg.dispose();
			GoHome gh = new GoHome(user);
		}	
	}
	
	
	/**
	 * Inner class where button Manage Groups pressed , implements action listener which opens the manage groups window.
	 *
	 */
	class ManageGroupListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			rg.dispose();
			ManageGroupsGUI mfg = new ManageGroupsGUI();
			ManageGroupsController mgc = new ManageGroupsController(user,lc,mfg);		
		}	
	}
	
	
	/**
	 * Inner class where button Rename File pressed , implements action listener which connects the server to change the file name.
	 * @author Netanel
	 *
	 */
	class RenameFileListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			if(rg.getFileNameField().equals("")){
				JOptionPane.showMessageDialog(null,"Enter new Name!","Error", JOptionPane.ERROR_MESSAGE);    
	  			  return;
			}
				
			 try{	  
				File FileL = new File(fileName, rg.getFileNameField(), null, null,null );
				String Use=user.getUserName();
				Envelope ev1 = new Envelope(FileL,"Rename",Use);	 
				sendToServer(ev1);
				MyBoxApp.clien.setCurrObj(rc);
				 
			  }
			  catch(Exception e1){
				  JOptionPane.showMessageDialog(null,"Erorr Connecting To Server!","Error", JOptionPane.ERROR_MESSAGE);      
			  }
		}	
	}
	
	
	/**
	 *  Method to handle the answer of the db after trying to change the file name.
	 *  @param message Answer of the db as Object.
	 */
	 public void handleDBResult(Object message) 
	 {
		 String Mess=(String) message;	 
		 if(Mess.equals("Rename"))
		 {
			 JOptionPane.showMessageDialog(null,"Renamed!","Success", JOptionPane.INFORMATION_MESSAGE);
			 rg.dispose();
			 ManageFilesGUI mfg = new ManageFilesGUI();
			 ManageFilesController mfc = new ManageFilesController(user,lc,mfg);	
		 }
		 else
		 {
			 JOptionPane.showMessageDialog(null,"There is Already A File With The Name In The System!","ERROR", JOptionPane.ERROR_MESSAGE);
			 return;
		 }
	 }
	
	 
	/**
	* Inner class where button Cancel pressed , implements action listener which cancel all the user selects and opens back the manage files window
	*
	*/	 
	class CancelListener implements ActionListener
	{

		public void actionPerformed(ActionEvent e) {
			rg.dispose();
			ManageFilesGUI mfg = new ManageFilesGUI();
			ManageFilesController mfc = new ManageFilesController(user,lc,mfg);	
		}
	}
}