package controllers;

import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import client.MyBoxApp;
import models.Envelope;
import models.File;
import models.User;
import view.AdminChangeDescriptionGUI;
import view.AdminManageFilesGUI;


/**
 * Controller for file description changing case on admin mode, handles all the event in the relavent GUI.
 *
 */
public class AdminChangeDescriptionController extends  AbstractTransfer{

	private User user;
	private LoginController lc;
	private AdminChangeDescriptionGUI acdg;
	private AdminChangeDescriptionController acdc; 
	private String fileName;

	
	/**
	 * 
	 * The constructor of the class, construct the listeners and the attributes for changing file description on admin mode event.
	 * @param user The logged-in user instance.
	 * @param atbg The relavent GUI.
	 * @param fileName The name of file gonna be changed.
	 */
	public AdminChangeDescriptionController(User user, AdminChangeDescriptionGUI acdg, String fileName) {
		this.user = user;
		this.lc = lc;
		this.acdc =this ;
		this.acdg = acdg;
		this.fileName=fileName;
		acdg.addbtnManageGroupsActionListener(new ManageGroupListener());
		acdg.addbtnChangeDescriptionActionListener(new ChangeDescriptionListener());
		acdg.addbtnCancelActionListener(new CancelListener());
	}
	
	
	class ManageGroupListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {

			
			
		}	
	}
	
	
	
	/**
	 * Inner class where button Change Desctription pressed , implements action listener which connect to the db and change the description.
	 *
	 */
	class ChangeDescriptionListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			if(acdg.getNewDescription().equals("")){
				JOptionPane.showMessageDialog(null,"Enter Description!","Error", JOptionPane.ERROR_MESSAGE);    
	  			  return;
			}
			 try{  
				  File FileL = new File(fileName, null, null, acdg.getNewDescription(),null );
				 Envelope ev1 = new Envelope(FileL,"AdminChangeDescription","admin");	 
				 sendToServer(ev1);
				 MyBoxApp.clien.setCurrObj(acdc);
			  }
			  catch(Exception e1){
		   JOptionPane.showMessageDialog(null,"Erorr Connecting To Server!","Error", JOptionPane.ERROR_MESSAGE);  
			  }	
		}
	}
	
	
	/**
	 *  Method to handle the answer of the db after trying to change the description.
	 *  @param message Answer of the db as Object.
	 */
	 public void handleDBResult(Object message) {
		String str = message.toString();
		File FileL = new File(null, null, null, null, null);
		JOptionPane.showMessageDialog(null,"Description Changed!","Success", JOptionPane.INFORMATION_MESSAGE);
		try{
			Envelope ev1 = new Envelope(FileL,"GetFilesForAdmin3","Admin");	 
			sendToServer(ev1);
			MyBoxApp.clien.setCurrObj(acdc);
		}
		catch(Exception e1){
			JOptionPane.showMessageDialog(null,"Erorr Connecting To Server!","Error", JOptionPane.ERROR_MESSAGE);
		}
	
	 }
	 
	 
	/**
	*  Method to handle the answer of the db after pulling the file list for admin.
	*  Opens the Manage files window.
	*  @param message Answer of the db as Object.
	*/ 
	 public void Reopen(Object message)
	 {
	 	 acdg.dispose();	
	 	 message = ((String) message).substring(17);
	 	 AdminManageFilesGUI amfg = new AdminManageFilesGUI(message);	 
	 	 AdminManageFilesController amfc = new AdminManageFilesController(user,amfg,message);		
	 }
	
	 
	 
	/**
	* Inner class where button Cancel pressed , implements action listener which cancel all the user selects and opens back the manage files window
	*
	*/
	class CancelListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			File FileL = new File(null, null, null, null, null);
			 try{
				 Envelope ev1 = new Envelope(FileL,"GetFilesForAdmin3","Admin");	 
				 sendToServer(ev1);
				 MyBoxApp.clien.setCurrObj(acdc);
			 }
			 catch(Exception e1)
			 {
				 JOptionPane.showMessageDialog(null,"Erorr Connecting To Server!","Error", JOptionPane.ERROR_MESSAGE);
			 }
				
					
		}
	}
}
	

