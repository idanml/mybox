package controllers;

import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import client.MyBoxApp;
import controllers.*;
import view.*;
import models.Envelope;
import models.File;
import models.User;
import view.SearchResultGUI;


/**
 * Controller class for SearchResult window, handle all the events in the relavent GUI.
 *
 */
public class SearchResultController extends  AbstractTransfer {
	
	private User user;
	private LoginController lc;
	private SearchResultController src;
	private SearchResultGUI srg;
	private String FileToUpdate=null;
	
	
	/**
	 * This is a constructor of the class, construct the listeners and attributes of the case.
	 * @param user The instance of the logged in user.
	 * @param lc The login controller of the specific login of the user.
	 * @param srg The relavent GUI.
	 */
	public SearchResultController(User user, LoginController lc, SearchResultGUI srg) {
		this.user = user;
		this.lc = lc;
		this.src = this;
		this.srg = srg;
		srg.addbtnHomeActionListener(new HomeListener());
		srg.addbtnManageGroupsActionListener(new ManageGroupListener());
		srg.addbtnReadFileActionListener(new ReadFileListener());
		srg.addbtnUpdateFileActionListener(new UpdateFileListener());
		srg.addbtnCancelActionListener(new CancelListener());
	}
	
	
	
	/**
	 * Inner class where button Home pressed , implements action listener which get you back to main window.
	 *
	 */
	class HomeListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			srg.dispose();
			GoHome gh = new GoHome(user);
		}	
	}
	
	class ManageGroupListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			srg.dispose();
			ManageGroupsGUI mgg = new ManageGroupsGUI();
			ManageGroupsController mgc = new ManageGroupsController(user,lc,mgg);

		}
		
	}
	
	
	/**
	 * Inner class where button Read File pressed , implements action listener which connect to server to download the file and open it.
	 *
	 */
	class ReadFileListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			
			 if(srg.isFileFieldEmpty()==true){
				 JOptionPane.showMessageDialog(null,"Please Select A File!","ERROR", JOptionPane.ERROR_MESSAGE);
				 return;
			 }
			
			
			new Thread() {
			    @Override
			    public void run() {
			        callMyFunction();
			    }
			}.start();
			
			File FileL2 = new File( srg.getFileField(), null, null, null, null);
			String Use=user.getUserName();
			
		    try{
		    	Envelope ev2 = new Envelope(FileL2,"OpenFileInClient2",Use);	    
		    	sendToServer(ev2);     
		    	MyBoxApp.clien.setCurrObj(src);
		    }
		    catch(Exception e1){
		    	JOptionPane.showMessageDialog(null,"Erorr Connecting To Server!","Error", JOptionPane.ERROR_MESSAGE);
		    }
		}	
	}
	
	
	/**
	 * Inner class where button Update File pressed , implements action listener which uploads a file to overwrite by the old file.
	 * Also,connect to server to update db.
	 *
	 */
	class UpdateFileListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			
			java.io.File selectedFile;
			String loc=null;
			String fname="";
			
			JFileChooser chooser = new JFileChooser();
			int returnVal = chooser.showOpenDialog(null);
		    if(returnVal == JFileChooser.APPROVE_OPTION){
		    	selectedFile = chooser.getSelectedFile();
		    	fname = selectedFile.getName();
		    	loc= selectedFile.getPath();
		    }
			if(JOptionPane.showConfirmDialog(null,"This Update Will Overwrite The Existing File\n Are You Sure ?",
					  "Update", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) { 
  
				try{	  
					File FileL = new File(null, null, null, null, null);
					FileL.setFName(fname);
					FileToUpdate=fname;
					String Use=user.getUserName();
					Envelope ev1 = new Envelope(FileL,"UpdateFile2",Use);					 
					sendToServer(ev1);
					testt.SimpleFileClient.main(FileToUpdate,loc);
					MyBoxApp.clien.setCurrObj(src);
				}
				catch(Exception e1){
				   JOptionPane.showMessageDialog(null,"Erorr Connecting To Server!","Error", JOptionPane.ERROR_MESSAGE);    
				}
				
			} else return;


		}	
	}
	
	
	/**
	 * Method to call the main in SimpleFileServer.
	 */
	public void callMyFunction(){
		  SendFileToClient.SimpleFileServer.main(null); 
	}

	

	
	/**
	* Inner class where button Cancel pressed , implements action listener which cancel all the user selects and opens back the manage files window
	*
	*/	 
	class CancelListener implements ActionListener
	{

		public void actionPerformed(ActionEvent e) {
			srg.dispose();
			ManageFilesGUI mfg = new ManageFilesGUI();
			ManageFilesController mfc = new ManageFilesController(user,lc,mfg);	
		}	
	}
	
	
	
	
	/**
	 *  Method to handle the answer of the db after trying to upload and update file.
	 *  Notify the user about un/successfully update.
	 *  @param message Answer of the db as Object.
	 */
	 public void UpdateFile(Object message) {;
	 String Mess = message.toString();
	 if(Mess.equals("UpdateFile2"))		 {
		 JOptionPane.showMessageDialog(null,"File Updated!","Success", JOptionPane.INFORMATION_MESSAGE);    
	 } 
	 else{
		 JOptionPane.showMessageDialog(null,"File Name Not Found In The Box\n Please Use \"Add File\" Option","ERROR", JOptionPane.ERROR_MESSAGE);
	 	 return; 
	 	}
	 }
	 
	 
	/**
	*  Method to handle the answer of the db after trying to opens file.
	*  Download the file on user desktop and opens it.
	*  @param message Answer of the db as Object.
	*/
	 public void handleDBResult2(Object message) {
		    
		 String fname = (srg.getFileField());
	  		try {
	  			String sourceFilePath = "C:/MyBoxClient/"+ fname;
	  			java.io.File file = new  java.io.File(sourceFilePath);
	  			Desktop.getDesktop().open(file);
	  		} 
	  		catch (Exception e1) {
	  			e1.printStackTrace();
	  			return;
	  		}

	 }
	
	
	
	
	
	
	
	

}
