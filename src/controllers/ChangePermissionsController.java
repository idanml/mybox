package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import client.MyBoxApp;
import controllers.RenameController.HomeListener;
import controllers.RenameController.ManageGroupListener;
import models.Envelope;
import models.File;
import models.User;
import view. ChangePermissionsGUI;
import view.MainWindowGUI;
import view.ManageFilesGUI;
import view.ManageGroupsGUI;


/**
 * Controller of the changing file permission events, handle all the events on the relavent GUI
 *
 */
public class ChangePermissionsController extends  AbstractTransfer {
	
	private User user;
	private LoginController lc;
	private  ChangePermissionsController cpc;
	private  ChangePermissionsGUI cpg;
	private String fileName;
	
	
	/**
	 * This is constructor of the class, construct the events listeners and attributes.
	 * @param user user The instance of the logged in user.
	 * @param lc The login controller of the specific login of the user.
	 * @param cpg The relavent GUI.
	 * @param fileName The name of the file that gonna be changed.
	 */
	public  ChangePermissionsController(User user, LoginController lc,  ChangePermissionsGUI cpg, String fileName) {
		this.user = user;
		this.lc = lc;
		this.cpc = this;
		this.cpg = cpg;
		this.fileName=fileName;
		cpg.addbtnHomeActionListener(new HomeListener());
		cpg.addbtnManageGroupsActionListener(new ManageGroupListener());
		cpg.addbtnUpdatePermissionsActionListener(new UpdatePermissionsListener());
		cpg.addbtnCancelActionListener(new CancelListener());
	}
	
	
	/**
	 * Inner class where button Home pressed , implements action listener which get you back to main window.
	 *
	 */
	class HomeListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			cpg.dispose();
			GoHome gh = new GoHome(user);
		}	
	}
	
	
	
	/**
	 * Inner class where button Manage Groups pressed , implements action listener which opens the manage groups window.
	 *
	 */
	class ManageGroupListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			cpg.dispose();
			ManageGroupsGUI mfg = new ManageGroupsGUI();
			ManageGroupsController mgc = new ManageGroupsController(user,lc,mfg);		
		}	
	}
	
	
	/**
	 * Inner class where button Update Permission pressed , implements action listener which connects the server to change the permission.
	 *
	 */
	class UpdatePermissionsListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			if(cpg.getSelected()=="0"){
				JOptionPane.showMessageDialog(null,"Choose Permission!","Error", JOptionPane.ERROR_MESSAGE);    
	  			return;
			}
			
			//Connect the server to change the permission over the db
			 try{  
				File FileL = new File(fileName, null, null, null, cpg.getSelected());
				String Use=user.getUserName();
				Envelope ev1 = new Envelope(FileL,"ChangePermission",Use);	 
				sendToServer(ev1);
				MyBoxApp.clien.setCurrObj(cpc);
				 
			  }
			  catch(Exception e1){
				  JOptionPane.showMessageDialog(null,"Erorr Connecting To Server!","Error", JOptionPane.ERROR_MESSAGE);      
			  	}
			 }	
	}
	
	
	/**
	 *  Method to handle the answer of the db after trying to change the permission.
	 *  @param message Answer of the db as Object.
	 */
	 public void handleDBResult(Object message) {
		 
		 
		 String Mess = message.toString();
		 if(Mess.equals("ChangePermission")){	 
		 JOptionPane.showMessageDialog(null,"Permission Changed!","Success", JOptionPane.INFORMATION_MESSAGE);
		 cpg.dispose();
			ManageFilesGUI mfg = new ManageFilesGUI();
			ManageFilesController mfc = new ManageFilesController(user,lc,mfg);	}
		 else{
			 JOptionPane.showMessageDialog(null,"This File Already Have This Permission!","Error", JOptionPane.ERROR_MESSAGE);
		     return;}
	 
	}
	 
	 
	/**
	* Inner class where button Cancel pressed , implements action listener which cancel all the user selects and opens back the manage files window
	*
	*/
	class CancelListener implements ActionListener
	{

		public void actionPerformed(ActionEvent e) {
			cpg.dispose();
			ManageFilesGUI mfg = new ManageFilesGUI();
			ManageFilesController mfc = new ManageFilesController(user,lc,mfg);	
		}
	}
}