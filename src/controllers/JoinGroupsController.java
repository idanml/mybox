package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import client.MyBoxApp;
import controllers.LeaveGroupsController.CancelListener;
import controllers.LeaveGroupsController.HomeListener;
import controllers.LeaveGroupsController.LeaveListener;
import controllers.LeaveGroupsController.ManageFilesListener;
import models.Envelope;
import models.File;
import models.User;
import view. JoinGroupsGUI;
import view.MainWindowGUI;
import view.ManageFilesGUI;
import view.ManageGroupsGUI;


/**
 * Controller class for sending group-join request case, handle all the events in the relavent GUI.
 *
 */
public class JoinGroupsController extends  AbstractTransfer {
	
	private User user;
	private LoginController lc;
	private  JoinGroupsController jgc;
	private  JoinGroupsGUI jgg;
	private String selectedGroup;
	private boolean flag;
	
	
	/**
	 * This is a constructor of the class, construct the listeners and attributes of the case.
	 * @param user The instance of the logged in user.
	 * @param lc The login controller of the specific login of the user.
	 * @param jgg The relavent GUI.
	 */
	public  JoinGroupsController(User user, LoginController lc,  JoinGroupsGUI jgg) {
		this.setFlag(false);
		this.user = user;
		this.lc = lc;
		this.jgc = this;
		this.jgg = jgg;
		this.selectedGroup=null;
		jgg.addbtnHomeActionListener(new HomeListener());
		jgg.addbtnManageFilesActionListener(new ManageFilesListener());
		jgg.addbtnJoinActionListener(new JoinListener());
		jgg.addbtnCancelActionListener(new CancelListener());
	}

	
	/**
	 * Inner class where button Home pressed , implements action listener which get you back to main window.
	 *
	 */
	class HomeListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			jgg.dispose();
			GoHome gh = new GoHome(user);
		}	
	}
	
	
	/**
	 * Inner class where button Manage Files pressed , implements action listener which opens the manage files window.
	 * @author Netanel
	 *
	 */
	class ManageFilesListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			jgg.dispose();
			ManageFilesGUI mfg = new ManageFilesGUI();
			ManageFilesController mgc = new ManageFilesController(user,lc,mfg);		
		}	
	}
	
	
	/**
	 *  Inner class where button Join Group pressed , implements action listener which connect the server to insert the request on the db
	 */
	class JoinListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			selectedGroup=jgg.getGroupChoice();
			String Use=user.getUserName();
			try{
				
				Envelope en = new Envelope(selectedGroup,"JoinGroup",Use);
				sendToServer(en);
				MyBoxApp.clien.setCurrObj(jgc);	


			}
			catch(Exception e1){
				JOptionPane.showMessageDialog(null,"Erorr Connecting To Server!","Error", JOptionPane.ERROR_MESSAGE);
			}
		}	
	}
	
	
	/**
	 *  Method to handle the answer of the db after trying to send the request.
	 *  @param message Answer of the db as Object.
	 */
	public void handleDBResult(Object message) 
	{
		
		if(((String) message).equals("JoinGroupReq") )
		{

			this.setFlag(true);

			//JOptionPane.showMessageDialog(null,"Request to join "+selectedGroup+"as been sent. Admin will respond as soon as possible.","Success", JOptionPane.INFORMATION_MESSAGE);

			jgg.dispose();
			ManageGroupsGUI mfg = new ManageGroupsGUI();
			ManageGroupsController mfc = new ManageGroupsController(user,lc,mfg);
		}
		else{
			setFlag(false);
			JOptionPane.showMessageDialog(null,"Already requested","Error", JOptionPane.ERROR_MESSAGE);
		}

	 }
	


	/**
	 * Inner class where button Cancel pressed , implements action listener which cancel all the user selects and opens back the manage files window
	 *
	 */	
	class CancelListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			jgg.dispose();
			ManageGroupsGUI mfg = new ManageGroupsGUI();
			ManageGroupsController mgc = new ManageGroupsController(user,lc,mfg);		
		}	
	}
	
	public boolean isFlag() {
		return flag;
	}


	public void setFlag(boolean flag2) {
		this.flag = flag2;
	}
}
