package controllers;

import java.awt.Window;

import javax.swing.JFrame;
import javax.swing.JMenuBar;

import client.MyBoxApp;
import view.MainWindowGUI;
import models.Envelope;
import models.User;
/**
 * 
 * This class gets all the files and folders in the root folder in order to open the 
 * main window with the updated files. 
 * 
 */
public class GoHome extends  AbstractTransfer{
	private GoHome gh;
	private static User U;
	
	public GoHome(User U){
		this.U=U;
		gh=this;
		
		

		String Use=U.getUserName();
		Envelope en = new Envelope ("","GetFilesInRootFolder",Use);
		sendToServer(en);
		MyBoxApp.clien.setCurrObj(gh);
		
		
		
		
	}
	
	
	
	/**
	 * This method pharse the files and folders and open a new main window
	 * @param message is the files and folders
	 */
	
	public static void Handler2(Object message) { 

		JFrame frame = new JFrame();
		JMenuBar bar = new JMenuBar();
		frame.setJMenuBar(bar);
		if(bar.getParent().getParent().getParent() instanceof JFrame){
			 ((Window) bar.getParent().getParent().getParent()).dispose();
		}
		     
		String[] parts = new String((String) message).split("@@",2);
		String string1 = parts[1];
		String string2 = parts[0];
		String remove1 = string1.substring(0,string1.indexOf(' ')+1);
		string1 = string1.substring(string1.indexOf(' ')+1);
		String remove2 = string2.substring(0,string2.indexOf(' ')+1);
		string2 = string2.substring(string2.indexOf(' ')+1);
  
		Object Fi=string1;
		Object Fo=string2;
		MainWindowGUI mwg = new MainWindowGUI(Fi,Fo,U);
		MainWindowController mwc = new MainWindowController(U, null, mwg);
	}
	
	
	
	
	
	
	
	

}
