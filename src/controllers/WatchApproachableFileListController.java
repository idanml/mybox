package controllers;

import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.Timer;

import client.MyBoxApp;
import controllers.SearchFileController.CancelListener;
import controllers.SearchFileController.HomeListener;
import controllers.SearchFileController.ManageGroupListener;
import controllers.SearchFileController.SearchFileListener;
import models.Envelope;
import models.File;
import models.User;
import view.AddToBoxGUI;
import view.AdminManageFilesGUI;
import view.MainWindowGUI;
import view.ManageFilesGUI;
import view.ManageGroupsGUI;
import view.UpdateFileGUI;
import view. WatchApproachableFileListGUI;
/**
 * Controller to controller the WAF window, handle all the events on relavent GUI.
 * 
 *
 */
public class WatchApproachableFileListController extends  AbstractTransfer {
	
	private User user;
	private LoginController lc;
	private  WatchApproachableFileListController waflc;
	private  WatchApproachableFileListGUI waflg;
	private String FileToUpdate=null;


	/**
	 * The constructor of the class, construct the listeners and attributes of the window of WAFL
	 * @param user The instance of the logged in user.
	 * @param lc The login controller of the specific login of the user.
	 * @param mfg The relavent GUI.
	 */
	public  WatchApproachableFileListController(User user, LoginController lc,  WatchApproachableFileListGUI waflg) {
		this.user = user;
		this.lc = lc;
		this.waflc = this;
		this.waflg = waflg;

		waflg.addbtnHomeActionListener(new HomeListener());
		waflg.addbtnManageGroupsActionListener(new ManageGroupListener());
		waflg.addbtnReadFileActionListener(new ReadFileListener());
		waflg.addbtnUpdateFileActionListener(new UpdateFileListener());
		waflg.addbtnAddToPersonalActionListener(new AddToPersonalListener());
		waflg.addbtnCancelActionListener(new CancelListener());
	}

	
	/**
	 * Inner class where button Home pressed , implements action listener which get you back to main window.
	 *
	 */
	class HomeListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			waflg.dispose();
			GoHome gh = new GoHome(user);
		}	
	}
	
	
	/**
	 * Inner class where button Manage Groups pressed , implements action listener which opens the manage groups window.
	 *
	 */
	class ManageGroupListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			waflg.dispose();
			ManageGroupsGUI mfg = new ManageGroupsGUI();
			ManageGroupsController mgc = new ManageGroupsController(user,lc,mfg);		
		}	
	}
	
	
	/**
	 * Inner class where button Read File pressed , implements action listener which connect to server to download the file and open it.
	 *
	 */
	class ReadFileListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			
			 if(waflg.isFileFieldEmpty()==true){
				 JOptionPane.showMessageDialog(null,"Please Select A File!","ERROR", JOptionPane.ERROR_MESSAGE);
				 return;
			 }

			new Thread() {
			    @Override
			    public void run() {
			        callMyFunction();
			    }
			}.start();
			
			File FileL2 = new File( waflg.getFileField(), null, null, null, null);
			
			try{
				String Use=user.getUserName();
				Envelope ev2 = new Envelope(FileL2,"OpenFileInClient",Use);	    
				sendToServer(ev2);     
				MyBoxApp.clien.setCurrObj(waflc);
			}
			catch(Exception e1){
				JOptionPane.showMessageDialog(null,"Erorr Connecting To Server!","Error", JOptionPane.ERROR_MESSAGE);
			}
		}	
	}
	
	
	/**
	 * Inner class where button Update File pressed , implements action listener which uploads a file to overwrite by the old file.
	 * Also,connect to server to update db.
	 *
	 */
	class UpdateFileListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			
			
			java.io.File selectedFile;
			String loc=null;
			String fname="";
			
			JFileChooser chooser = new JFileChooser();
			int returnVal = chooser.showOpenDialog(null);
		    if(returnVal == JFileChooser.APPROVE_OPTION)
		    {
		    	selectedFile = chooser.getSelectedFile();
		    	fname = selectedFile.getName();
		    	loc= selectedFile.getPath();

		    }
			if(JOptionPane.showConfirmDialog(null,"This Update Will Overwrite The Existing File\n Are You Sure ?",
					  "Update", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) 
			{ 
				  
				try{  
					File FileL = new File(null, null, null, null, null);
					FileL.setFName(fname);
					FileToUpdate=fname;
					String Use=user.getUserName();
					Envelope ev1 = new Envelope(FileL,"UpdateFile",Use);					 
					sendToServer(ev1);
					testt.SimpleFileClient.main(FileToUpdate,loc);
					MyBoxApp.clien.setCurrObj(waflc);
				}
				catch(Exception e1){
				   JOptionPane.showMessageDialog(null,"Erorr Connecting To Server!","Error", JOptionPane.ERROR_MESSAGE);    
				}
  
				  
			} else return;	
		}	
	}
	
	/**
	 * Inner class where button Add to personal box pressed , implements action listener which connect to server to pull available folders.
	 *
	 */
	class AddToPersonalListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			
			
			 if(waflg.isFileFieldEmpty()==true){
					JOptionPane.showMessageDialog(null,"Choose File!","Error", JOptionPane.ERROR_MESSAGE);    
				  return;}
			 
			  File FileL = new File(null, null, null, null, null);
			  String Use=user.getUserName();
		      
			  try{
				  Envelope ev1 = new Envelope(FileL,"GetFolders2",Use);	 
				  sendToServer(ev1);
				  MyBoxApp.clien.setCurrObj(waflc);
			  }
			  catch(Exception e1){
				  JOptionPane.showMessageDialog(null,"Erorr Connecting To Server!","Error", JOptionPane.ERROR_MESSAGE);   
			  }
		}	
	}
	
	
	
	/**
	 * Method to handle the answer of the db 
	 * Opens the file.
	 * @param message Answer of the db as Object.
	 */
	public void handleDBResult2(Object message) {
		    
		 String fname = (waflg.getFileField());
		 fname = fname.substring(0, fname.indexOf(" -"));
		 final int TIME_VISIBLE = 3000;
		 JOptionPane pane = new JOptionPane("Opening...", JOptionPane.INFORMATION_MESSAGE, JOptionPane.DEFAULT_OPTION, null, new Object[]{}, null);
		 JDialog dialog = pane.createDialog(null, "");
		 dialog.setModal(false);
		 dialog.setVisible(true);

		 try {
			 Thread.sleep(TIME_VISIBLE);
		 } catch (InterruptedException e) {
			 e.printStackTrace();
		 }
		 dialog.setVisible(false);

		 try {
			 String sourceFilePath = "C:/MyBoxClient/"+ fname;
			 java.io.File file = new  java.io.File(sourceFilePath);
			 Desktop.getDesktop().open(file);
		 } catch (Exception e1) {
			 e1.printStackTrace();
			 return;
		 }
	 }
	 
	 
	 
		/**
		 * Method to handle the answer of the db after trying to update a file to my box.
		 * Show relavent message.
		 * @param message Answer of the db as Object.
		 */
	 public void UpdateFile(Object message) {
	 String Mess = message.toString();
	 if(Mess.equals("UpdateFile"))		 {

		
		 JOptionPane.showMessageDialog(null,"File Updated!","Success", JOptionPane.INFORMATION_MESSAGE);    
	 }
	 
	 else{
		 JOptionPane.showMessageDialog(null,"File Name Not Found In The Box\n Please Use \"Add File\" Option","ERROR", JOptionPane.ERROR_MESSAGE);
	 	 return; 
	 	} 
	 }
	 
	 
	/**
	*  Method to handle the answer of the db after trying to pull available folders.
	*  Opens add to box window.
	*  @param message Answer of the db as Object.
	*/
	 public void handleDBResult3(Object message) {;
			AddToBoxGUI atbg = new AddToBoxGUI(message);
			AddToBoxController atbc = new AddToBoxController(user,waflg,atbg);
	 }
	 
	 
	 
		/**
		 * Method to handle the answer of the db after trying to add a file to my box.
		 * Show relavent message.
		 * @param message Answer of the db as Object.
		 */
	 public void handleDBResult(Object message) {
		 String Mess = message.toString();
		 if(Mess.equals("AddToBox"))		 {
			JOptionPane.showMessageDialog(null,"File Added To Your Box!","Success", JOptionPane.INFORMATION_MESSAGE);
			waflg.dispose();
			ManageFilesGUI mfg = new ManageFilesGUI();
			ManageFilesController mfc = new ManageFilesController(user,lc,mfg);	}
		 else{
			 JOptionPane.showMessageDialog(null,"File Already In Your Box!","ERROR", JOptionPane.ERROR_MESSAGE);
		 	 return;}
	 }
	
	 
	 /**
	  * Method to call the main at SimpleFileServer.
	  * Download a file.
	  */
	public void callMyFunction(){
		  SendFileToClient.SimpleFileServer.main(null); 
	}

	
	 
		/**
		 * Inner class where button Cancel pressed , implements action listener which cancel all the user selects and opens back the manage files window
		 *
		 */
	class CancelListener implements ActionListener
	{

		public void actionPerformed(ActionEvent e) {
			waflg.dispose();
			ManageFilesGUI mfg = new ManageFilesGUI();
			ManageFilesController mfc = new ManageFilesController(user,lc,mfg);	
		}	
	}


	
}