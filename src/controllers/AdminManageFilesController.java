package controllers;



import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import client.MyBoxApp;
import models.Envelope;
import models.File;
import models.Request;
import models.User;
import view.AdminChangeDescriptionGUI;
import view.AdminManageFilesGUI;
import view.AdminRenameGUI;
import view.GroupJoinRequestApprovingGUI;
import view.MainWindowGUI;
import view.ManageFilesGUI;
import view.RenameGUI;
import controllers.ManageFilesController.AddFileListener;
import controllers.ManageFilesController.DeleteFileListener;
import controllers.ManageFilesController.FileInfoEditListener;
import controllers.ManageFilesController.HomeListener;
import controllers.ManageFilesController.ManageFoldersListener;
import controllers.ManageFilesController.ManageGroupListener;
import controllers.ManageFilesController.SearchFileListener;
import controllers.ManageFilesController.WatchApprochableFilesListener;


/**
 * Controller of the manage files window at admin mode, handle all the events on the relavent GUI.
 * 
 *
 */
public class AdminManageFilesController extends  AbstractTransfer {
	
	private User user;
	private LoginController lc;
	private AdminManageFilesGUI amfg;
	private AdminManageFilesController amfc; 
	private ManageFilesGUI mfg;
	private File FileL;
	private Object FF;
	private String FileToUpdate=null;

	
	
	/**
	 * The constructor of the class, construct the listeners and attributes of the window of manage files
	 * @param user The instance of the logged in user.
	 * @param amfg The relavent GUI.
	 * @param F 
	 */
	public AdminManageFilesController(User user, AdminManageFilesGUI amfg,Object F) {
		this.user = user;
		this.lc = lc;
		this.amfc =this ;
		this.amfg = amfg;
		this.FF=F;

		amfg.addbtnManageGroupsActionListener(new ManageGroupListener());
		amfg.addbtneleteFileActionListener(new DeleteFileListener());
		amfg.addbtnOpenFileActionListener(new OpenFileListener());
		amfg.addbtnRenameActionListener(new RenameListener());
		amfg.addbtnChangeDescriptionActionListener(new ChangeDescriptionLIstener());
		amfg.addbtnUpdateFileActionListener(new UpdateFileListener());


	}
	

	/**
	 * Inner class where button Manage Groups pressed , implements action listener which connect to server to pull the request list.
	 *
	 */
	class ManageGroupListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {

			  try{ //send to server	  
				  File FileL = new File(null, null, null, null, null);
			
				 Envelope ev1 = new Envelope(FileL,"GetRequests","");	 
				 sendToServer(ev1);
				 MyBoxApp.clien.setCurrObj(amfc);
				 
			  }
			  catch(Exception e1){
		   JOptionPane.showMessageDialog(null,"Erorr Connecting To Server!","Error", JOptionPane.ERROR_MESSAGE);    
				  
			  }
		}	
	}
	
	
	/**
	 * Method to handle the answer of the db after trying to get the group requests list.
	 * Opens join approving window.
	 * @param message Answer of the db as Object.
	 */
	public void OpenGroups(Object message){
		
	  String Messa=message.toString();
	  if(Messa.equals("NoRequest"))
	  {
		JOptionPane.showMessageDialog(null,"No Requests For Now","Error", JOptionPane.ERROR_MESSAGE);  
		return;
	  }
		amfg.dispose();
		GroupJoinRequestApprovingGUI gjrag = new GroupJoinRequestApprovingGUI(message);
		GroupJoinRequestApprovingController gjrac = new GroupJoinRequestApprovingController(user,gjrag,message,FF);
	
	}
	
	
	
	

	/**
	 * Inner class where button Delete Files pressed , implements action listener which connect to server to delete the file from db.
	 *
	 */
	class DeleteFileListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {

			if(amfg.isFileFieldEmpty()==true){
				  JOptionPane.showMessageDialog(null,"Please Choose A File","Error", JOptionPane.ERROR_MESSAGE);  
				return;
			}
			
			if(JOptionPane.showConfirmDialog(null,"Going To Delete "+ amfg.getFilefield() +"\nAre You Sure ?",
					  "Update", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) { 
				
			try{  
				File FileL = new File(amfg.getFilefield(), null, null, null, null);
				String Use=user.getUserName();
				Envelope ev1 = new Envelope(FileL,"DeleteFileAdmin",Use);	 
				sendToServer(ev1);
				MyBoxApp.clien.setCurrObj(amfc); 
			  }
			  catch(Exception e1){
				  JOptionPane.showMessageDialog(null,"Erorr Connecting To Server!","Error", JOptionPane.ERROR_MESSAGE);      
			  }
			}
			
			else
				return;
			
		}	
	}
	
	
	/**
	 * Inner class where button Open File pressed , implements action listener which connect to server to download the file and open it.
	 *
	 */
	class OpenFileListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			
			 if(amfg.isFileFieldEmpty()==true){
				 JOptionPane.showMessageDialog(null,"Please Select A File!","ERROR", JOptionPane.ERROR_MESSAGE);
				 return;
			 }
			 
			new Thread() {
			    @Override
			    public void run() {
			        callMyFunction();
			    }
			}.start();

			File FileL2 = new File( amfg.getFilefield(), null, null, null, null);
			String Use=user.getUserName();
		    
			try{
		    	 Envelope ev2 = new Envelope(FileL2,"OpenFileInClient3",Use);	    
		    	 sendToServer(ev2);     
			     MyBoxApp.clien.setCurrObj(amfc);
		     }
		     catch(Exception e1){
		    	 JOptionPane.showMessageDialog(null,"Erorr Connecting To Server!","Error", JOptionPane.ERROR_MESSAGE);  
		     }

		}	
	}
	
	
	
	/**
	 * Method to call main fun at SimpleFileServer to download a file.
	 */
	public void callMyFunction(){
		  SendFileToClient.SimpleFileServer.main(null); 
	}
	
	
	/**
	 * Method to handle the answer of the db.
	 * Open the file at the user desktop.
	 * @param message Answer of the db as Object.
	 */
	 public void handleDBResult2(Object message) {
	    
		String fname = (amfg.getFilefield());
		 
		final int TIME_VISIBLE = 3000;
		JOptionPane pane = new JOptionPane("Opening...", JOptionPane.INFORMATION_MESSAGE, JOptionPane.DEFAULT_OPTION, null, new Object[]{}, null);
        JDialog dialog = pane.createDialog(null, "");
        dialog.setModal(false);
        dialog.setVisible(true);

        try {
				Thread.sleep(TIME_VISIBLE);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
        dialog.setVisible(false);
		 
		 
	  	try {
			String sourceFilePath = "C:/MyBoxClient/"+ fname;
	    	java.io.File file = new  java.io.File(sourceFilePath);
	    	Desktop.getDesktop().open(file);
		} catch (Exception e1) {
			e1.printStackTrace();
			return;
		} 
	 }
		
		/**
		 * Inner class where button Rename pressed , implements action listener which opens the admin rename window.
		 *
		 */
	 class RenameListener implements ActionListener
		{
			public void actionPerformed(ActionEvent e) {
				if(amfg.isFileFieldEmpty()==true){
					  JOptionPane.showMessageDialog(null,"Please Choose A File","Error", JOptionPane.ERROR_MESSAGE);  
					return;
				}
				String fileName= amfg.getFilefield();
				amfg.dispose();
				AdminRenameGUI arg = new AdminRenameGUI();
				AdminRenameController arc = new AdminRenameController(user,arg, fileName);
				
			}	
		}
		
	 
		/**
		 * Inner class where button Rename pressed , implements action listener which opens the admin change description window.
		 *
		 */
		class ChangeDescriptionLIstener implements ActionListener
		{
			public void actionPerformed(ActionEvent e) {
				if(amfg.isFileFieldEmpty()==true){
					  JOptionPane.showMessageDialog(null,"Please Choose A File","Error", JOptionPane.ERROR_MESSAGE);  
					return;
				}
				String fileName= amfg.getFilefield();
				amfg.dispose();
				AdminChangeDescriptionGUI acdg = new AdminChangeDescriptionGUI();
				AdminChangeDescriptionController acdc = new AdminChangeDescriptionController(user,acdg, fileName);
			}	
		}
	
		
		
		/**
		 * Inner class where button Update File pressed , implements action listener which connect to server to update file.
		 *
		 */	
	class UpdateFileListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {

			java.io.File selectedFile;
			String loc=null;
			String fname="";
			
			JFileChooser chooser = new JFileChooser();
			int returnVal = chooser.showOpenDialog(null);
		    if(returnVal == JFileChooser.APPROVE_OPTION){
		    	selectedFile = chooser.getSelectedFile();
		    	fname = selectedFile.getName();
		    	loc= selectedFile.getPath();
		    
		    }
			  if(JOptionPane.showConfirmDialog(null,"This Update Will Overwrite The Existing File\n Are You Sure ?",
					  "Update", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) 
			  { 
				  
				  
				  try{ //send to server	  
				    File FileL = new File(null, null, null, null, null);
					FileL.setFName(fname);
					FileToUpdate=fname;
					String Use=user.getUserName();
					 Envelope ev1 = new Envelope(FileL,"UpdateFile3",Use);					 
					 sendToServer(ev1);
					 testt.SimpleFileClient.main(FileToUpdate,loc);
					 MyBoxApp.clien.setCurrObj(amfc);

				  }
				  catch(Exception e1){
			   JOptionPane.showMessageDialog(null,"Erorr Connecting To Server!","Error", JOptionPane.ERROR_MESSAGE);    
					  
				  }
  
				  
				} else {
					  return;
				}
			  }	
	}
	
	
	
	/**
	 * Method to handle the answer of the db.
	 * Show a message to the admin - update/error.
	 * @param message Answer of the db as Object.
	 */
	 public void UpdateFile(Object message) {
	 String Mess = message.toString();
	 if(Mess.equals("UpdateFile3"))		 {
		 JOptionPane.showMessageDialog(null,"File Updated!","Success", JOptionPane.INFORMATION_MESSAGE);    
	 }
	 
	 else{
		 JOptionPane.showMessageDialog(null,"File Name Not Found In The Box\n Please Use \"Add File\" Option","ERROR", JOptionPane.ERROR_MESSAGE);
	 	 return; 
	 	}
	 }
	 
	
	
		/**
		 * Method to handle the answer of the db.
		 * Connect the server to pull the files list.
		 * @param message Answer of the db as Object.
		 */
	public void DeleteAdmin(Object message){
		
		File FileL = new File(null, null, null, null, null);
		 try{
			 Envelope ev1 = new Envelope(FileL,"GetFilesForAdmin","Admin");	 
			 sendToServer(ev1);
			 MyBoxApp.clien.setCurrObj(amfc);
		 }
		 catch(Exception e1){
			 JOptionPane.showMessageDialog(null,"Erorr Connecting To Server!","Error", JOptionPane.ERROR_MESSAGE);
		 }


	}
	
	
	/**
	 * Method to handle the answer of the db.
	 * Open the Admin manage files window.
	 * @param message Answer of the db as Object.
	 */
	public void Reopen(Object message)
	{
		 amfg.dispose();	
		 message = ((String) message).substring(17);
		 AdminManageFilesGUI amfg = new AdminManageFilesGUI(message);	 
		 AdminManageFilesController amfc = new AdminManageFilesController(user,amfg,message);		
		 JOptionPane.showMessageDialog(null,"File Deleted!","Success", JOptionPane.INFORMATION_MESSAGE);
	}
	
}
