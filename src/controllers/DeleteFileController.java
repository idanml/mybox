package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import client.MyBoxApp;
import models.*;
import view.*;
import controllers.*;
import controllers.FileInfoEditController.ManageFileslListener;


/**
 * Controller of deleting file case, handle all the events of deleting file in the relavent GUI
 *
 */
public class DeleteFileController extends  AbstractTransfer {
	
	private User user;
	private LoginController lc;
	private DeleteFileController dfc;
	private DeleteFileGUI dfg;
	private File FileL;
	private DeleteFileController TempL;
	
	
	/**
	 * The constructor of the class, construct the listeners and attributes of the event of deleting file.
	 * @param user The instance of the logged in user.
	 * @param lc The login controller of the specific login of the user.
	 * @param dfg The relavent GUI.
	 */
	public DeleteFileController(User user, LoginController lc, DeleteFileGUI dfg) {
		this.user = user;
		this.lc = lc;
		this.dfc = this;
		this.dfg = dfg;
		TempL=this;
		dfg.addbtnRemoveAFActionListener(new RemoveAFListener());
		dfg.addbtnHardDeleteeActionListener(new HardDeleteListener());
		dfg.addbtnHomeActionListener(new HomeListener());
		dfg.addbtnManageGroupsActionListener(new ManageGroupListener());
		dfg.addbtnManageFilesActionListener(new ManageFileslListener());
	}
	
	
	/**
	 * Inner class where button Home pressed , implements action listener which get you back to main window.
	 *
	 */
	class HomeListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			dfg.dispose();
			GoHome gh = new GoHome(user);
		}	
	}
	
	
	/**
	 * Inner class where button Manage Files pressed , implements action listener which get you to manage files window.
	 *
	 */
	class ManageFileslListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			dfg.dispose();
			ManageFilesGUI mfg = new ManageFilesGUI();
			ManageFilesController mfc = new ManageFilesController(user,lc,mfg);
		}
	}
	
	
	/**
	 * Inner class where button Remove Approachable Files pressed , implements action listener which opens the RAFD window.
	 *
	 */
	class RemoveAFListener implements ActionListener
	{

		public void actionPerformed(ActionEvent e) {
			String Use=user.getUserName();
			Envelope ev1 = new Envelope(null,"GetApproachFiles2",Use);
			sendToServer(ev1);
			MyBoxApp.clien.setCurrObj(dfc);
		}
	}
	
	
	/**
	 * Method to handle the answer of the db after trying to get the approchable files list
	 * @param message Answer of the db as Object.
	 */
	 public void handleDBResult2(Object message) {
		    dfg.dispose();
		    RemoveApproachableFileGUI rafg = new RemoveApproachableFileGUI(message);
		    RemoveApproachableFileController rafc = new RemoveApproachableFileController(user,lc,rafg);	
	 }

	
	
	/**
	 * Inner class where button Hard Delete pressed , implements action listener which opens the Hard Delete window.
	 *
	 */
	class HardDeleteListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			String Use=user.getUserName();
			Envelope ev1 = new Envelope(null,"GetFiles",Use);
			sendToServer(ev1);
			MyBoxApp.clien.setCurrObj(dfc);
		}

	
	}
	
	
	
	/**
	 * Inner class where button Manage Groups pressed , implements action listener which opens the manage groups window.
	 *
	 */
	class ManageGroupListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			dfg.dispose();
			ManageGroupsGUI mfg = new ManageGroupsGUI();
			ManageGroupsController mgc = new ManageGroupsController(user,lc,mfg);		
		}	
	}
	
	
	/**
	 * Method to handle the answer of the db after trying to get the approchable files list
	 * @param message Answer of the db as Object.
	 */
	public void handleDBResult(Object message) {
		dfg.dispose();
		HardDeleteFilesGUI hdfg = new HardDeleteFilesGUI(message);
		HardDeleteFilesController hdf = new HardDeleteFilesController(user,lc,hdfg);			
	 
	 }
	
	 
	 
	/**
	* Inner class where button Cancel pressed , implements action listener which cancel all the user selects and opens back the manage files window
	*
	*/
	public DeleteFileController getTempL() {
		return TempL;
	}
	
}