package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import client.MyBoxApp;
import controllers.SearchFileController.CancelListener;
import controllers.SearchFileController.HomeListener;
import controllers.SearchFileController.ManageGroupListener;
import controllers.SearchFileController.SearchFileListener;
import models.Envelope;
import models.File;
import models.User;
import view.MainWindowGUI;
import view.ManageFilesGUI;
import view.ManageGroupsGUI;
import view.RemoveApproachableFileGUI;

public class RemoveApproachableFileController extends  AbstractTransfer {
	
	private User user;
	private LoginController lc;
	private RemoveApproachableFileController rafc;
	private RemoveApproachableFileGUI rafg;
	
	public RemoveApproachableFileController(User user, LoginController lc, RemoveApproachableFileGUI rafg) {
		this.user = user;
		this.lc = lc;
		this.rafc = this;
		this.rafg = rafg;
		rafg.addbtnHomeActionListener(new HomeListener());
		rafg.addbtnManageGroupsActionListener(new ManageGroupListener());
		rafg.addbtnRemoveFileActionListener(new RemoveFileListener());
		rafg.addbtnCancelActionListener(new CancelListener());
		
	}

	
	/**
	 * Inner class where button Home pressed , implements action listener which get you back to main window.
	 *
	 */
	class HomeListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			rafg.dispose();
			GoHome gh = new GoHome(user);
		}	
	}
	
	
	/**
	 * Inner class where button Manage Groups pressed , implements action listener which opens the manage groups window.
	 *
	 */
	class ManageGroupListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			rafg.dispose();
			ManageGroupsGUI mfg = new ManageGroupsGUI();
			ManageGroupsController mgc = new ManageGroupsController(user,lc,mfg);		
		}	
	}
	
	
	/**
	 * Inner class where button Remove File pressed , implements action listener which connect the server to try delete the file from the db
	 *
	 */
	class RemoveFileListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			String fname = rafg.getFileField();
			fname = fname.substring(0, fname.indexOf(" "));
			String Use=user.getUserName();
			File FileL = new File(fname, null, null, null, null);
			try{
				Envelope ev1 = new Envelope(FileL,"DeleteApproachFile",Use);
				sendToServer(ev1);
				MyBoxApp.clien.setCurrObj(rafc);
			}
			catch(Exception e1){
				JOptionPane.showMessageDialog(null,"Erorr Connecting To Server!","Error", JOptionPane.ERROR_MESSAGE);    
			}
		}	
	}
	
	
	/**
	 * Method to handle the answer of the db after trying to delete the approchable file from the db
	 * @param message
	 */
	 public void handleDBResult(Object message) {
		 String Mess = message.toString();
		 if(Mess.equals("DeleteApproachFile")){
			JOptionPane.showMessageDialog(null,"File Deleted From Your Box!","Error", JOptionPane.INFORMATION_MESSAGE); 
			rafg.dispose();
			ManageFilesGUI mfg = new ManageFilesGUI();
			ManageFilesController mfc = new ManageFilesController(user,lc,mfg);
		 }
		 else{
			JOptionPane.showMessageDialog(null,"You Are The Owner Of This File, Use Hard Delete!","Error", JOptionPane.ERROR_MESSAGE); 
			return;
		 } 
	 }

	 
	/**
	* Inner class where button Cancel pressed , implements action listener which cancel all the user selects and opens back the manage files window
	*
	*/		
	class CancelListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			rafg.dispose();
			ManageFilesGUI mfg = new ManageFilesGUI();
			ManageFilesController mfc = new ManageFilesController(user,lc,mfg);	
		}	
	}
}
