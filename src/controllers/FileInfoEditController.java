package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import client.MyBoxApp;
import models.Envelope;
import models.File;
import models.User;
import view.ChangeDescriptionGUI;
import view.ChangeFileLocationGUI;
import view.ChangePermissionsGUI;
import view. FileInfoEditGUI;
import view.MainWindowGUI;
import view.ManageFilesGUI;
import view.ManageGroupsGUI;
import view.RenameGUI;



/**
 * Controller for changing file info window, handle the events on the relavent GUI.
 *
 */
public class FileInfoEditController extends  AbstractTransfer {
	
	private User user;
	private LoginController lc;
	private FileInfoEditController fiec;
	private FileInfoEditGUI fieg;
	private File FileL;
	
	
	/**
	 * This is the constructor of the class, construct the listeners and attributes of the GUI.
	 * @param user The instance of the logged in user.
	 * @param lc The login controller of the specific login of the user.
	 * @param fieg The relavent GUI.
	 */
	public  FileInfoEditController(User user, LoginController lc,  FileInfoEditGUI fieg) {
		this.user = user;
		this.lc = lc;
		this.fiec = this;
		this.fieg = fieg;
		fieg.addbtnHomeActionListener(new HomeListener());
		fieg.addbtnManageGroupsActionListener(new ManageGroupListener());
		fieg.addbtnManageFilesActionListener(new ManageFileslListener());
		fieg.addbtnRenameActionListener(new RenameListener());
		fieg.addbtnChangePermissionsActionListener(new ChangePermissionsListener());
		fieg.addbtnChangeDescriptionActionListener(new ChangeDescriptionListener());
		fieg.addbtnChangeFileLocationActionListener(new ChangeFileLocationListener());
	}

	
	
	/**
	 * Inner class where button Home pressed , implements action listener which get you back to main window.
	 *
	 */
	class HomeListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			fieg.dispose();
			GoHome gh = new GoHome(user);
		}	
	}
	
	
	/**
	 * Inner class where button Manage Files pressed , implements action listener which get you to manage files window.
	 *
	 */
	class ManageFileslListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			fieg.dispose();
			ManageFilesGUI mfg = new ManageFilesGUI();
			ManageFilesController mfc = new ManageFilesController(user,lc,mfg);
		}
	}
	
	
	/**
	 * Inner class where button Manage Groups pressed , implements action listener which opens the manage groups window.
	 *
	 */
	class ManageGroupListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			fieg.dispose();
			ManageGroupsGUI mfg = new ManageGroupsGUI();
			ManageGroupsController mgc = new ManageGroupsController(user,lc,mfg);		
		}	
	}
	
	
	/**
	 * Inner class where button Rename pressed , implements action listener which opens the rename window
	 * @author Netanel
	 *
	 */
	class RenameListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			 if(fieg.isFileFieldEmpty()==true){
				JOptionPane.showMessageDialog(null,"Choose File!","Error", JOptionPane.ERROR_MESSAGE);    
				return;
				}
			String fileName= fieg.getFilefield();
			fieg.dispose();
			RenameGUI cpg = new RenameGUI();
			RenameController mwc = new RenameController(user,lc,cpg, fileName);
		}	
	}
	
	
	/**
	 * Inner class where button Change Permission pressed , implements action listener which opens the Change Permission window
	 *
	 */
	class ChangePermissionsListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			if(fieg.isFileFieldEmpty()==true)
			{
				JOptionPane.showMessageDialog(null,"Choose File!","Error", JOptionPane.ERROR_MESSAGE);    
				return;
			}
			String fileName= fieg.getFilefield();
			fieg.dispose();
			ChangePermissionsGUI cpg = new ChangePermissionsGUI();
			ChangePermissionsController mwc = new ChangePermissionsController(user,lc,cpg, fileName);
		}	
	}
	
	
	
	/**
	 * Inner class where button Change Description pressed , implements action listener which opens the Change Description window
	 *
	 */
	class ChangeDescriptionListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			 if(fieg.isFileFieldEmpty()==true){
				JOptionPane.showMessageDialog(null,"Choose File!","Error", JOptionPane.ERROR_MESSAGE);    
				return;
			}
			String fileName= fieg.getFilefield();
			fieg.dispose();
			ChangeDescriptionGUI cdg = new ChangeDescriptionGUI();
			ChangeDescriptionController mwc = new ChangeDescriptionController(user,lc,cdg,fileName);
		}	
	}
	
	
	
	/**
	 * Inner class where button Change File Location pressed , implements action listener which connect the server to pull available locations on private box.
	 *
	 */
	class ChangeFileLocationListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			if(fieg.isFileFieldEmpty()==true){
				JOptionPane.showMessageDialog(null,"Choose File!","Error", JOptionPane.ERROR_MESSAGE);    
				return;
			}
			String Use=user.getUserName();
			try{
				Envelope ev1 = new Envelope(null,"GetFolders3",Use);
				sendToServer(ev1);
				MyBoxApp.clien.setCurrObj(fiec);
			}
			catch(Exception e1){
				JOptionPane.showMessageDialog(null,"Erorr Connecting To Server!","Error", JOptionPane.ERROR_MESSAGE);
			}
			
		}	
	}
	
	
	/**
	 * Method to handle the answer of the server 
	 * @param message Answer of the db as Object.
	 */
	public void handleDBResult(Object message) {
		fieg.dispose();
		ChangeFileLocationGUI cflg = new ChangeFileLocationGUI(message);
		ChangeFileLocationController cflc = new ChangeFileLocationController(user,lc,cflg,fieg.getFilefield());			
	 
	 }
}
