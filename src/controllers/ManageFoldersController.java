package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import client.MyBoxApp;
import controllers.ManageFilesController.HomeListener;
import controllers.ManageFilesController.ManageGroupListener;
import models.Envelope;
import models.File;
import models.Folders;
import models.User;
import view.AddFolderGUI;
import view.ChangeFolderLocationGUI;
import view.MainWindowGUI;
import view.ManageFilesGUI;
import view. ManageFoldersGUI;
import view.ManageGroupsGUI;
import view.RenameFolderGUI;

public class ManageFoldersController extends  AbstractTransfer {
	
	private User user;
	private LoginController lc;
	private  ManageFoldersController mfc;
	private  ManageFoldersGUI mfg;
	private Folders folder;
	
	public  ManageFoldersController(User user, LoginController lc,  ManageFoldersGUI mfg) {
		this.user = user;
		this.lc = lc;
		this.mfc = this;
		this.mfg = mfg;
		mfg.addbtnHomeActionListener(new HomeListener());
		mfg.addbtnManageFilesActionListener(new ManageFilesListener());
		mfg.addbtnManageGroupsActionListener(new ManageGroupListener());
		mfg.addbtnAddNewFolderActionListener(new AddFolderListener());
		mfg.addbtnbtnDeleteFolderActionListener(new DeleteFolderListener());
		mfg.addbtnRenameFolderActionListener(new RenameFolderListener());
		mfg.addbtnChangeFolderLocationActionListener(new ChangeFolderLocationListener());
	}

	
	/**
	 * Inner class where button Home pressed , implements action listener which get you back to main window.
	 *
	 */
	class HomeListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			mfg.dispose();
			GoHome gh = new GoHome(user);
		}	
	}

	/**
	 * Inner class where button Manage Files pressed , implements action listener which get you to manage files window.
	 *
	 */
	class ManageFilesListener implements ActionListener
	{

		public void actionPerformed(ActionEvent e) {
			mfg.dispose();
			ManageFilesGUI mfg = new ManageFilesGUI();
			ManageFilesController mfc = new ManageFilesController(user,lc,mfg);	
		}	
	}
	
	
	/**
	 * Inner class where button Manage Groups pressed , implements action listener which opens the manage groups window.
	 *
	 */
	class ManageGroupListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			mfg.dispose();
			ManageGroupsGUI mfg = new ManageGroupsGUI();
			ManageGroupsController mgc = new ManageGroupsController(user,lc,mfg);		
		}	
	}
	
	
	/**
	 * Inner class where button Add Folder pressed , implements action listener which pull the folders from the db.
	 *
	 */
	class AddFolderListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			
			String Use=user.getUserName();
			Envelope ev1 = new Envelope(null,"GetFolders5",Use);
			try{
				sendToServer(ev1);
				MyBoxApp.clien.setCurrObj(mfc);
			}
			catch(Exception e1){
				JOptionPane.showMessageDialog(null,"Erorr Connecting To Server!","Error", JOptionPane.ERROR_MESSAGE);
			}
		}	
	}
	
	
	/**
	 * Method to handle the answer of the server.
	 * Open the add file window with available locations list.
	 * @param message Answer of the db as Object.
	 */
	public void handleDBResult1(Object message)
	{
		mfg.dispose();
		AddFolderGUI afg = new AddFolderGUI(message);
		AddFolderController mgc = new AddFolderController(user,lc,afg);
	}
	
	
	/**
	 * Inner class where button Delete Folder pressed , implements action listener which pull the folders from the db.
	 *
	 */
	class DeleteFolderListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			
			if(mfg.isFolderFieldEmpty()==true){
				JOptionPane.showMessageDialog(null,"Choose Folder.","Error", JOptionPane.ERROR_MESSAGE);
				return;
			}
			
			String Use=user.getUserName();
			Use= Use.substring(0,1).toUpperCase() +Use.substring(1);
			String folderName= mfg.getSelectedFolder();
			String folderLoc="/"+Use+folderName;
			String folders[]=folderName.split("/");
			int count = folderName.length() - folderName.replace("/", "").length();
			folderName=folders[count-1];
			folderLoc=folderLoc.substring(0, folderLoc.length()-folderName.length()-1);
			Folders folder = new Folders(Use,folderName,folderLoc);
			try{
				Envelope ev1 = new Envelope(folder,"DeleteFolder",Use);
				sendToServer(ev1);
				MyBoxApp.clien.setCurrObj(mfc);
			}
			catch(Exception e1){
				JOptionPane.showMessageDialog(null,"Erorr Connecting To Server!","Error", JOptionPane.ERROR_MESSAGE);
			}
		}	
	}
	
	public void handleDBResult(Object message)
	{
		JOptionPane.showMessageDialog(null,"Folder Deleted.","OK", JOptionPane.INFORMATION_MESSAGE);
		mfg.dispose();
		
		ManageFilesGUI mfg = new ManageFilesGUI();
		ManageFilesController mfc = new ManageFilesController(user,lc,mfg);
	}
	
	public void HandleDBResult4(Object message)
	{
		JOptionPane.showMessageDialog(null,"Folder Deleted.","OK", JOptionPane.INFORMATION_MESSAGE);
		mfg.dispose();
		ManageFoldersGUI mfg = new ManageFoldersGUI(message);
		ManageFoldersController mfc = new ManageFoldersController(user,lc,mfg);
	}
	
	class RenameFolderListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			
			
			if(mfg.isFolderFieldEmpty()==true){
				JOptionPane.showMessageDialog(null,"Choose Folder.","Error", JOptionPane.ERROR_MESSAGE);
				return;
			}
			
			String Use=user.getUserName();
			Use= Use.substring(0,1).toUpperCase() +Use.substring(1);
			String folderName= mfg.getSelectedFolder();
			
			
			String folderLoc="/"+Use+folderName;
			String folders[]=folderName.split("/");
			int count = folderName.length() - folderName.replace("/", "").length();
			folderName=folders[count-1];
			
			folderLoc=folderLoc.substring(0, folderLoc.length()-folderName.length()-1);
			
			Folders folder = new Folders(Use,folderName,folderLoc);

			
			mfg.dispose();
			RenameFolderGUI rfg = new RenameFolderGUI();
			RenameFolderController mgc = new RenameFolderController(user,lc,rfg,folder);		
		}	
	}
	
	class ChangeFolderLocationListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			
			if(mfg.isFolderFieldEmpty()==true){
				JOptionPane.showMessageDialog(null,"Choose Folder.","Error", JOptionPane.ERROR_MESSAGE);
				return;
			}
			
			
			String Use=user.getUserName();
			String folderName= mfg.getSelectedFolder();
			
			
			String folderLoc="/"+Use+folderName;
			String folders[]=folderName.split("/");
			int count = folderName.length() - folderName.replace("/", "").length();
			folderName=folders[count-1];
			
			folderLoc=folderLoc.substring(0, folderLoc.length()-folderName.length()-1);
			
			folder = new Folders(Use,folderName,folderLoc);
			Envelope ev1 = new Envelope(folder,"GetFolders6",Use);
			sendToServer(ev1);
			MyBoxApp.clien.setCurrObj(mfc);
			
				
		}	
	}
	
	public void handleDBResult6(Object message)
	{
		mfg.dispose();
		ChangeFolderLocationGUI cflg = new ChangeFolderLocationGUI(message);
		ChangeFolderLocationController cflc = new ChangeFolderLocationController(user,lc,cflg,folder);	
	}
	
}
