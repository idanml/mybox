package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import client.MyBoxApp;
import models.Envelope;
import models.LoginMod;
import models.User;
import client.MyBoxApp;
import client.IObserve;
import client.CliMessage;

import java.io.IOException;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Observable;
import java.util.concurrent.Executors;

import javax.swing.JOptionPane;

import server.ServerGui;
import server.serverLogGui;
import view.AdminManageFilesGUI;
import view.ChangePasswordGUI;
import view.ExplorerGUI;
import view.ForgotPasswordGUI;
import view.LoginGUI;
import view.MainWindowGUI;
import view.RegisterGUI;


/**
 * This is the login controller, handles all the events on the Login GUI
 *
 */
public class LoginController extends  AbstractTransfer 
{

	private LoginGUI loginG;
	private LoginMod loginM;
	private LoginController tempL;
	private User user;
	private User U;
	public int LoginCounter=1;
	private static Object FilesInRoot = new ArrayList<String>();
	private static Object FoldersInRoot = new ArrayList<String>();
	public ArrayList<User> userArr = null;

	/**
	 * 
	 * @param lC
	 * @param lM
	 */
	
	public void setUser(ArrayList<User> u) {
		userArr = u;
	}
	
	public boolean userExist(String userName){
		for (int i =0 ;i<userArr.size();i++){
			if (userArr.get(i).getUserName().equals(userName))
				return true;
		}
		return false;
	}
	
	public boolean checkPassword(String userName,String passWord)
	{
		for (int i =0 ;i<userArr.size();i++){
			if (userArr.get(i).getUserName().equals(userName)){
				if (userArr.get(i).getPassword().equals(passWord))
				{
					return true;
				}
				else {
					return false;
				}
		
			}
			}
		return false;
	}
	
	public boolean checkIfAdmin(String userName)
	{
		if (userName.equals("Admin"))
					return true;
				else {
					return false;
				}
		
	}

	public LoginController (LoginGUI lC,LoginMod lM )
	{
		loginG = lC;
		loginM = lM;
		tempL = this;
		loginG.addLoginActionListener(new LoginListener());
		loginG.addbtnForgotYourPasswordActionListener(new forgotPassListener());
		loginG.addCancelActionListener(new CancelListener());
		loginG.addbtnChangePasswordActionListener(new changePassListener());
		loginG.addRegisterActionListener(new registerListener());
		
	}
	
	/**
	 * Inner class where button Register pressed , implements action listener
	 * 
	 */
	class registerListener implements ActionListener 
	{
		@Override
		public void actionPerformed(ActionEvent e) 
		{
			RegisterGUI rg = new RegisterGUI();	 //Open The register Window 
			RegisterController rc = new RegisterController(rg);
			
		}
	
	}//action
	
	/**
	 * 	Inner class where button Forgot Password pressed , implements action listener
	*
	*/
	class forgotPassListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent e)
		{
			ForgotPasswordGUI fpg = new ForgotPasswordGUI(); //Open the forgot password window
			ForgotPasswordController fpc = new ForgotPasswordController(fpg);	
		}
	}//action
	
	/**
	 * Inner class where button cancel pressed , implements action listener
	 *
	 */
	class CancelListener implements ActionListener 
	{
		@Override
		public void actionPerformed(ActionEvent e)
		{
		loginG.dispose();	//Closes the login window
		}	
	}//action
	
	/**
	 *  Inner class where button Change Password pressed , implements action listener
	 *
	 */
	class changePassListener implements ActionListener 
	{
        public void actionPerformed(ActionEvent ev)
        {
        	loginG.dispose(); 								 //Closes the login window
        	ChangePasswordGUI cpg = new ChangePasswordGUI(); //Opens the Change password window
        	ChangePasswordController cpc = new ChangePasswordController(cpg);
        }
	}//action
		
	/**
	 *  Inner class where button Login pressed , implements action listener
	 *
	 */
	class LoginListener implements ActionListener
	{
         public void actionPerformed(ActionEvent ev)
         {      		
        	 String pass = loginG.getPasswordField();	//Gets the password the user entered
        	 String user = loginG.getUserField();		//Gets the user name the user entered
        	 User U = new User(user,null,null,null,null,null);
        	 if(pass.equals("")|| user.equals("")) 		//If fields are empty , show error message
        	 {
        		 JOptionPane.showMessageDialog(null,"Empty Fields!","Error", JOptionPane.INFORMATION_MESSAGE);    
        		 return;								//return to the login window
        	 }//if
        	 else
        	 {
        			 
        		 try
        		 {										 //set the user name and password and send to server
        		   loginM.setPassword(pass);
        		   loginM.setUserName(user);
        		   Envelope en = new Envelope(loginM,"searchLogin",user);
        		   sendToServer(en);
        		   MyBoxApp.clien.setCurrObj(getTempL());	 
        		 }
        		 catch(Exception e){}
		      }//else
          }
	}//action
	
	/**
	 * This function handles the response from the server
	 * @param message is the message from the client
	 */
	 public void handleDBResult(Object message)
	 {	
		 if(message instanceof ArrayList<?>)					//Get from the server the list of files and folders
		 {
		  FilesInRoot=((ArrayList<String>) message).get(1); 	//String of Files In Root Folder
		  FoldersInRoot=((ArrayList<String>) message).get(2);	//String of Folders In Root Folder
		 }

		 String str = message.toString();
		 String euser = loginG.getUserField();					//save current user name

		 if(str.equals("UserOrPassIncorrect"))					//if user or password incorrect show warning message 
		 {														//and increase counter by 1
			 JOptionPane.showMessageDialog(null,"User Or Password Incorrect!\n" + LoginCounter + "(out of 3) try","Error", JOptionPane.ERROR_MESSAGE);   
			 LoginCounter++;
			 if (LoginCounter==4)								//if user entered wrong details 3 times
			 {													//Show warning message, close program and send notification to the administrator
				 JOptionPane.showMessageDialog(null,"3rd Login Try\nTerminating!","Error", JOptionPane.ERROR_MESSAGE); 
				 loginG.dispose();
				 Envelope en = new Envelope(loginM,"SendMail3Times",euser);
				 sendToServer(en);
				 LoginCounter=1;							//reset counter	
			 }//if counter			
		 }//if	 
		 else
		 {
			 User U = new User (euser,euser,euser, euser, euser, euser); //Set new user instance (only for the user name)
		
			 loginG.dispose();											 //Close login GUI

			 if(euser.equalsIgnoreCase("Admin"))						//Check if the user is Admin
			 {	
				 AdminManageFilesGUI amfg = new AdminManageFilesGUI(FilesInRoot);	 //Open Admin Main window GUI
				 AdminManageFilesController amfc = new AdminManageFilesController(U,amfg,FilesInRoot);
				 JOptionPane.showMessageDialog(null,"You Are Logged As Admin!","Admin", JOptionPane.INFORMATION_MESSAGE); 
			 }//if
			 else
			 {
				 GoHome gh = new GoHome(U);
			 }//else

			
		 } 
		
	 }
	 

	 
	/**
	 * get the login Gui 
	 * @return LoginGui1
	 */
	public LoginGUI getLoginG() {
		return loginG;
	}
	/**
	 * set login gui
	 * @param loginG
	 */
	public void setLoginG(LoginGUI loginG) {
		this.loginG = loginG;
	}
	
	
	public LoginMod getLoginM() {
		return loginM;
	}

	/**
	 * set log in model(entity)
	 * @param loginM
	 */
	public void setLoginM(LoginMod loginM) {
		this.loginM = loginM;
	}
	
	
	public LoginController getTempL() {
		return tempL;
	}


	/**
	 * Get the files in the root folder
	 * @return Files in the user's root folder
	 */
	public static  Object getFilesInRoot() {
		return FilesInRoot;
	}
	
	/**
	 * Get the folders in the root folder
	 * @return Folders in the user's root folder
	 */
	public static Object getFoldersInRoot() {
		return FoldersInRoot;
	}
	
	
	
	
}
