package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import client.MyBoxApp;
import models.Envelope;
import models.File;
import models.User;
import view.AddFileGUI;
import view.HardDeleteFilesGUI;
import view.MainWindowGUI;
import view.ManageFilesGUI;
import view.ManageGroupsGUI;


/**
 * Controller class of the hard deleting file case, handle all the events in the relavent GUI.
 *
 */
public class HardDeleteFilesController extends  AbstractTransfer {

	
	private User user;  															//Global Variables
	private LoginController lc;
	private HardDeleteFilesController hdfc;
	private HardDeleteFilesGUI hdfg;
	
	
	
	/**
	 * This is the constructor of the class, construct the listeners and attributes of the GUI.
	 * @param user The instance of the logged in user.
	 * @param lc The login controller of the specific login of the user.
	 * @param hdfg The relavent GUI.
	 */
	public HardDeleteFilesController(User user, LoginController lc, HardDeleteFilesGUI hdfg) 
	{
		this.user = user; 															//Build Listeners and sent variables
		this.lc = lc;
		this.hdfc = this;
		this.hdfg = hdfg;
		hdfg.addbtnDeleteFileActionListener(new HardDeleteListener());
		hdfg.addbtnCancelActionListener(new CancelListener());
		hdfg.addbtnHomeActionListener(new HomeListener());
		hdfg.addbtnManageGroupsActionListener(new ManageGroupListener());
	}
	
	
	/**
	 * Inner class where button Home pressed , implements action listener which get you back to main window.
	 *
	 */
	class HomeListener implements ActionListener 									//If clicked on "Home"
	{
		public void actionPerformed(ActionEvent e) 
		{
			hdfg.dispose();															//CLose current GUI	
			GoHome gh = new GoHome(user); 
		}//Action
	}//HomeListener
	
	
	/**
	 * Inner class where button Manage Groups pressed , implements action listener which opens the manage groups window.
	 *
	 */
	class ManageGroupListener implements ActionListener								//If clicked on "Manage Groups"
	{
		public void actionPerformed(ActionEvent e)
		{
			hdfg.dispose();															//CLose current GUI	
			ManageGroupsGUI mfg = new ManageGroupsGUI();							//Open Manage Groups Window 
			ManageGroupsController mgc = new ManageGroupsController(user,lc,mfg);		
		}//Action
	}//ManageGroupListener
	
	
	
	/**
	 * Inner class where button Hard Delete pressed , implements action listener which connect the server to remove the file form the db.
	 *
	 */
	class HardDeleteListener implements ActionListener 								//If clicked on "Hard Delete"
	{
		@Override
		public void actionPerformed(ActionEvent e)
		{
			  try{  
				  File FileL = new File(hdfg.getFileField(), null, null, null, null); //Build File with name only
				  String Use=user.getUserName();									 //Get current User name
				  Envelope ev1 = new Envelope(FileL,"DeleteFile",Use);	 			 //Build Envelop to send to the server
				  sendToServer(ev1);													//Send to server	
				  MyBoxApp.clien.setCurrObj(hdfc);									//Set current controller
				 
			      }//Try
			  catch(Exception e1)
			  {
				  JOptionPane.showMessageDialog(null,"Erorr Connecting To Server!","Error", JOptionPane.ERROR_MESSAGE);    //Error if failed	  
			  }//Catch	
		}//Action		
	}//HardDeleteListener
	
	
	
	/**
	 * Inner class where button Cancel pressed , implements action listener which cancel all the user selects and opens back the manage files window
	 *
	 */	
	class CancelListener implements ActionListener	
	{
		@Override
		public void actionPerformed(ActionEvent e) {
			hdfg.dispose();															//CLose current GUI	
			ManageFilesGUI mfg = new ManageFilesGUI();								//Open Manage Files Window 
			ManageFilesController mfc = new ManageFilesController(user,lc,mfg);
		}//Action		
	}//Cancel Listener
	
	
	

	/**
	 *  Method to handle the answer of the db after trying to change the location.
	 *  @param message Answer of the db as Object.
	 */
	 public void handleDBResult(Object message)
	 {																									 //Get response from server
		 JOptionPane.showMessageDialog(null,"File Deleted!","Success", JOptionPane.INFORMATION_MESSAGE); //show message confirmation
		 hdfg.dispose();																				//CLose current GUI	
		 ManageFilesGUI mfg = new ManageFilesGUI();													//Open Manage File Window 
		 ManageFilesController mfc = new ManageFilesController(user,lc,mfg);	 
	 }//handleDBResult
	

	
}//End Of Class
