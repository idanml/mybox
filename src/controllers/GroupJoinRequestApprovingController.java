package controllers;

import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import client.MyBoxApp;
import controllers.AdminManageFilesController.DeleteFileListener;
import controllers.AdminManageFilesController.ManageGroupListener;
import controllers.AdminManageFilesController.OpenFileListener;
import controllers.AdminManageFilesController.UpdateFileListener;
import models.Envelope;
import models.File;
import models.Group;
import models.Request;
import models.User;
import view.AdminManageFilesGUI;
import view.GroupJoinRequestApprovingGUI;
import view.ManageFilesGUI;

/**
 * Controller class to handle the approving requests to join event , handle all the event of the relavent GUI.
 *
 */
public class GroupJoinRequestApprovingController extends  AbstractTransfer{
	
	private User user;
	private GroupJoinRequestApprovingGUI gjrag;
	private GroupJoinRequestApprovingController gjrac; 
	private File FileL;
	private Object FF;
	private Object FFF;

	/**
	 * This is a constructor of the class, construct the listeners and attributes of the case.
	 * @param user The instance of the logged in user.
	 * @param gjrag The relavent GUI.
	 * @param F
	 * @param Fi
	 */
	public GroupJoinRequestApprovingController (User user, GroupJoinRequestApprovingGUI gjrag,Object F,Object Fi) {
		this.user = user;
		this.gjrag = gjrag;
		this.gjrac=this;
		this.FF =F ;
		this.FFF =Fi ;

		gjrag.addbtnManageFilesActionListener(new ManageFilesListener());
		gjrag.addbtnManageGroupsActionListener(new ManageGroupListener());
		gjrag.addbtnAprroveActionListener(new AprroveListener());
		gjrag.addbtnRejectActionListener(new RejectListener());
		gjrag.addbtnCancelActionListener(new CancelListener());

	}
		
	
	
	/**
	 * Inner class where button Manage Files pressed , implements action listener which opens the manage files window.
	 *
	 */
	class ManageFilesListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
				
			gjrag.dispose();
			AdminManageFilesGUI amfg = new AdminManageFilesGUI(FFF);	 
			AdminManageFilesController amfc = new AdminManageFilesController(user,amfg,FFF);
			}	
	}
		
		
		
		
		
	class ManageGroupListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
				
			}	
		}
		
		
	/**
	 * Inner class where button Approve pressed , implements action listener which connect the server to marked the request as approved
	 *
	 */	
	class AprroveListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			
			int IntType=0;	
			if(gjrag.isFileFieldEmpty()==true){
				JOptionPane.showMessageDialog(null,"Select A Request!" ,"Error", JOptionPane.ERROR_MESSAGE);  
				return;	 
			}
				
				
			String Request = gjrag.getRequestField();
			String req[] = Request.split("(  -  )|( : )");
			String Group=req[1];
			String Type=req[3];
			String User=req[5];
			Group.replaceAll("\\s","");
			Type.replaceAll("\\s","");
			User.replaceAll("\\s","");
				
			if(Type.equals("Join"))
				IntType=1;
			else
				IntType=2;
			
			Request RequestL = new Request(Group,IntType,User);//Will Be Group Details
			try{
				Envelope ev1 = new Envelope(RequestL,"ApproveJoinRequest",User);	 
				sendToServer(ev1);
				MyBoxApp.clien.setCurrObj(gjrac);
			}catch(Exception e1){
				JOptionPane.showMessageDialog(null,"Erorr Connecting To Server!","Error", JOptionPane.ERROR_MESSAGE);      
			}
		}	
	}
		
	
	
	/**
	 * Inner class where button Reject pressed , implements action listener which connect the server to marked the request as rejected
	 *
	 */
	class RejectListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			
			int IntType=0;	
			if(gjrag.isFileFieldEmpty()==true){
				 JOptionPane.showMessageDialog(null,"Select A Request!" ,"Error", JOptionPane.ERROR_MESSAGE);  
				 return;	 
			}
				
			String Request = gjrag.getRequestField();
			String req[] = Request.split("(  -  )|( : )");
			String Group=req[1];
			String Type=req[3];
			String User=req[5];
			Group.replaceAll("\\s","");
			Type.replaceAll("\\s","");
			User.replaceAll("\\s","");
					
			if(Type.equals("Join"))
				IntType=1;
			else
				IntType=2;
					
			Request RequestL = new Request(Group,IntType,User);//Will Be Group Details
			
			try{
				Envelope ev1 = new Envelope(RequestL,"RejectJoinRequest",User);	 
				sendToServer(ev1);
				MyBoxApp.clien.setCurrObj(gjrac);
				
			}catch(Exception e1){
				JOptionPane.showMessageDialog(null,"Erorr Connecting To Server!","Error", JOptionPane.ERROR_MESSAGE);      
			}

		}	
	}
		
	
	
	/**
	* Inner class where button Cancel pressed , implements action listener which cancel all the user selects and opens back the manage files window
	*
	*/
	class CancelListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			gjrag.dispose();
			AdminManageFilesGUI amfg = new AdminManageFilesGUI(FFF);	 
			AdminManageFilesController amfc = new AdminManageFilesController(user,amfg,FFF);
			}	
	}
	
	
	/**
	 *  Method to handle the answer of the db after trying to approve/reject the request.
	 *  @param message Answer of the db as Object.
	 */
	public void handleDBResult(Object message) {
		
		String Mess=(String)message;

		if(Mess.equals("RequestApproved")){
				
			JOptionPane.showMessageDialog(null,"Request Approved!" ,"Success", JOptionPane.INFORMATION_MESSAGE);  
			gjrag.dispose();
			AdminManageFilesGUI amfg = new AdminManageFilesGUI(FFF);	 
			AdminManageFilesController amfc = new AdminManageFilesController(user,amfg,FFF);
		}
		else{	
			JOptionPane.showMessageDialog(null,"Request Rejected!" ,"Success", JOptionPane.INFORMATION_MESSAGE); 
			gjrag.dispose();
			AdminManageFilesGUI amfg = new AdminManageFilesGUI(FFF);	 
			AdminManageFilesController amfc = new AdminManageFilesController(user,amfg,FFF);
 
		 }
	}

}

	
	
	
	
	
	


