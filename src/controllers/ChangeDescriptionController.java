package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import client.MyBoxApp;
import controllers.ChangePermissionsController.CancelListener;
import controllers.ChangePermissionsController.HomeListener;
import controllers.ChangePermissionsController.ManageGroupListener;
import controllers.ChangePermissionsController.UpdatePermissionsListener;
import models.Envelope;
import models.File;
import models.User;
import view. ChangeDescriptionGUI;
import view.MainWindowGUI;
import view.ManageFilesGUI;
import view.ManageGroupsGUI;


/**
 * Controller for changing description of a file, handle all the events of the relavent GUI.
 *
 */
public class ChangeDescriptionController extends  AbstractTransfer {
	
	private User user;
	private LoginController lc;
	private  ChangeDescriptionController cdc;
	private  ChangeDescriptionGUI cdg;
	private String fileName;

	
	
	/**
	 * The constructor of the class, construct all the listeners and attributed of the class.
	 * @param user The instance of the logged in user.
	 * @param lc The login controller of the specific login of the user.
	 * @param cdg The relavent GUI.
	 * @param fileName The name of the file that gonna be changed.
	 */
	public  ChangeDescriptionController(User user, LoginController lc,  ChangeDescriptionGUI cdg, String fileName) {
		this.user = user;
		this.lc = lc;
		this.cdc = this;
		this.cdg = cdg;
		this.fileName=fileName;
		cdg.addbtnHomeActionListener(new HomeListener());
		cdg.addbtnManageGroupsActionListener(new ManageGroupListener());
		cdg.addbtnChangeDescriptionActionListener(new ChangeDescriptionListener());
		cdg.addbtnCancelActionListener(new CancelListener());
	}
	
	
	/**
	 * Inner class where button Home pressed , implements action listener which get you back to main window.
	 *
	 */
	class HomeListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			cdg.dispose();
			GoHome gh = new GoHome(user);
		}	
	}
	
	
	
	/**
	 * Inner class where button Manage Groups pressed , implements action listener which opens the manage groups window.
	 *
	 */	
	class ManageGroupListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			cdg.dispose();
			ManageGroupsGUI mfg = new ManageGroupsGUI();
			ManageGroupsController mgc = new ManageGroupsController(user,lc,mfg);		
		}	
	}
	
	
	
	/**
	 * Inner class where button Change Description pressed , implements action listener which connects the server to change the description.
	 *
	 */
	class ChangeDescriptionListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			if(cdg.getDescriptionField().equals("")){
				JOptionPane.showMessageDialog(null,"Enter Description!","Error", JOptionPane.ERROR_MESSAGE);    
	  			  return;
			}
				
			 try{ //send to server	  
				  File FileL = new File(fileName, null, null, cdg.getDescriptionField(),null );
					String Use=user.getUserName();
				 Envelope ev1 = new Envelope(FileL,"ChangeDescription",Use);	 
				 sendToServer(ev1);
				 MyBoxApp.clien.setCurrObj(cdc);
				 
			  }
			  catch(Exception e1){
		   JOptionPane.showMessageDialog(null,"Erorr Connecting To Server!","Error", JOptionPane.ERROR_MESSAGE);    
				  
			  }
		}	
	}
	
	
	
	/**
	 *  Method to handle the answer of the db after trying to change the description.
	 *  @param message Answer of the db as Object.
	 */
	 public void handleDBResult(Object message) {
		 JOptionPane.showMessageDialog(null,"Description Changed!","Success", JOptionPane.INFORMATION_MESSAGE);
		 cdg.dispose();
			ManageFilesGUI mfg = new ManageFilesGUI();
			ManageFilesController mfc = new ManageFilesController(user,lc,mfg);	
	}
	 
	 
	 
		/**
		 * Inner class where button Cancel pressed , implements action listener which cancel all the user selects and opens back the manage files window
		 *
		 */
	class CancelListener implements ActionListener
	{

		public void actionPerformed(ActionEvent e) {
			cdg.dispose();
			ManageFilesGUI mfg = new ManageFilesGUI();
			ManageFilesController mfc = new ManageFilesController(user,lc,mfg);	
		}
	}
}