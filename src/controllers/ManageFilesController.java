package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import client.MyBoxApp;
import controllers.MainWindowController.ManageFilesListener;
import controllers.MainWindowController.ManageGroupListener;
import models.Envelope;
import models.File;
import models.Folders;
import models.User;
import view.AddFileGUI;
import view.DeleteFileGUI;
import view.FileInfoEditGUI;
import view.ManageFoldersGUI;
import view.SearchFileGUI;
import view.MainWindowGUI;
import view.ManageFilesGUI;
import view.ManageGroupsGUI;
import view.WatchApproachableFileListGUI;


/**
 * Controller of the manage files window, handle all the events on the relavent GUI.
 * Enter the add file window and pull the available folders in the user box.
 * Enter the file info edit window and pull all the files in the user box.
 * Enter delete file/search file/WatchApprochableFiles windows.
 * 
 *
 */
public class ManageFilesController extends  AbstractTransfer {
	
	private User user;
	private LoginController lc;
	private ManageFilesController mfc;
	private ManageFilesGUI mfg;
	private File FileL;
	private ManageFilesController TempL;

	
	/**
	 * The constructor of the class, construct the listeners and attributes of the window of manage files
	 * @param user The instance of the logged in user.
	 * @param lc The login controller of the specific login of the user.
	 * @param mfg The relavent GUI.
	 */
	public ManageFilesController(User user, LoginController lc, ManageFilesGUI mfg) {
		this.user = user;
		this.lc = lc;
		this.mfc =this ;
		this.mfg = mfg;
		TempL=this;
		mfg.addbtnHomeActionListener(new HomeListener());
		mfg.addbtnManageGroupsActionListener(new ManageGroupListener());
		mfg.addbtnAddFileActionListener(new AddFileListener());
		mfg.addbtnDeleteFileActionListener(new DeleteFileListener());
		mfg.addbtnSearchFilesActionListener(new SearchFileListener());
		mfg.addbtnFileInfoEditActionListener(new FileInfoEditListener());
		mfg.addbtnWatchApprochableFilesActionListener(new WatchApprochableFilesListener());
		mfg.addbtnManageFoldersActionListener(new ManageFoldersListener());

	}
	
	
	/**
	 * Inner class where button Home pressed , implements action listener which get you back to main window.
	 *
	 */
	class HomeListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			mfg.dispose();
			GoHome gh = new GoHome(user);
		}	
	}

	
	/**
	 * Inner class where button Manage Groups pressed , implements action listener which opens the manage groups window.
	 *
	 */
	class ManageGroupListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			mfg.dispose();
			ManageGroupsGUI mfg = new ManageGroupsGUI();
			ManageGroupsController mgc = new ManageGroupsController(user,lc,mfg);		
		}	
	}
	
	
	
	
	/**
	 * Inner class where button Add File pressed , implements action listener which pull the available folders from the db.
	 *
	 */
	class AddFileListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			String Use=user.getUserName();

				Envelope ev1 = new Envelope(FileL,"GetFolders",Use);
				sendToServer(ev1);
				MyBoxApp.clien.setCurrObj(getTempL());

		}	
	}
	
	
	
	/**
	 * Method to handle the answer of the db after trying to get the available folders list.
	 * Opens Add File window.
	 * @param message Answer of the db as Object.
	 */
	 public void handleDBResult(Object message) {
		 mfg.dispose();
		 AddFileGUI afg = new AddFileGUI(message);
		 AddFileController afc = new AddFileController(user,lc,afg);			
	 
	 }
	
	
		
	/**
	* Inner class where button Delete Files pressed , implements action listener which opens the Delete Files window.
	*
	*/	 
	class DeleteFileListener implements ActionListener
	{
	public void actionPerformed(ActionEvent e) {
			mfg.dispose();
			DeleteFileGUI dfg = new DeleteFileGUI();
			DeleteFileController dfc = new DeleteFileController(user,lc,dfg);		
		}	
	}
	
	
	
	/**
	 * Inner class where button Search File pressed , implements action listener which opens the Search File window.
	 *
	 */
	class SearchFileListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			mfg.dispose();
			SearchFileGUI sfg = new SearchFileGUI();
			SearchFileController sfc = new SearchFileController(user,lc,sfg);		
		}
	}
	
	
	
	/**
	 * Inner class where button Add File pressed , implements action listener which pull the files from the db.
	 *
	 */
	class FileInfoEditListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
		
			String Use=user.getUserName();
			

				Envelope ev1 = new Envelope(FileL,"GetFiles2",Use);
				sendToServer(ev1);
				MyBoxApp.clien.setCurrObj(mfc);

	
		}
	}
	
	
	/**
	 * Method to handle the answer of the db after trying to get the files list.
	 * Opens File Info Edit window.
	 * @param message Answer of the db as Object.
	 */
	public void handleDBResult3(Object message) {
		mfg.dispose();
		FileInfoEditGUI fieg = new FileInfoEditGUI(message);
		FileInfoEditController fiec = new FileInfoEditController(user,lc,fieg);	
	}
	
	

	/**
	 * Inner class where button Add File pressed , implements action listener which pull the approchable files from the db.
	 *
	 */
	class WatchApprochableFilesListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
		
			String Use=user.getUserName();

				Envelope ev1 = new Envelope(FileL,"GetApproachFiles",Use);			
				sendToServer(ev1);
				MyBoxApp.clien.setCurrObj(getTempL());

	
		}
	}
	
	
	/**
	* Method to handle the answer of the db after trying to get the approchable files list.
	* Opens Watch Approachable File window.
	* @param message Answer of the db as Object.
	*/
	public void handleDBResult2(Object message) {
		mfg.dispose();
		WatchApproachableFileListGUI waflg = new WatchApproachableFileListGUI(message);
		WatchApproachableFileListController waflc = new WatchApproachableFileListController(user,lc,waflg);	
	}

	 
	
	
	
	/**
	* Inner class where button Manage files pressed , implements action listener which opens the manage files window.
	*
	*/
	class ManageFoldersListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			String Use=user.getUserName();

				Envelope ev1 = new Envelope(FileL,"GetFolders4",Use);
				sendToServer(ev1);
				MyBoxApp.clien.setCurrObj(mfc);

			
		}
	}
	
	
	/**
	* Method to handle the answer of the db after trying to get the folders list.
	* Opens manage files window.
	* @param message Answer of the db as Object.
	*/
	public void HandleDBResult4(Object message)
	{
		mfg.dispose();
		ManageFoldersGUI mfg = new ManageFoldersGUI(message);
		ManageFoldersController mfc = new ManageFoldersController(user,lc,mfg);
	}
	

	public ManageFilesController getTempL() {
		return TempL;
	}
	
	
}
