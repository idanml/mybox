package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import view.LoginGUI;
import view.MainWindowGUI;
import view.MyBoxClientGUI;
import models.LoginMod;
import models.MyBoxClientModel;
import client.MyBoxClient;
import client.MyBoxApp;;

/**
 * This Class is a MyBox Client Controller which create MyBoxClient instance and check the port and host to server
 *
 */
public class MyBoxClientController extends  AbstractTransfer {
	
	private MyBoxClientGUI clientView;
	private MyBoxClientModel clientModel;
	/**
	 * constructor  
	 * @param clientView2 is gui that show the host and port
	 * @param clientModel is entity of client that include host and port
	 */
	public MyBoxClientController(MyBoxClientGUI cView,MyBoxClientModel cModel)
	{
		this.clientView = cView;
		this.clientModel = cModel;
		cView.addOKActionListener(new OKListener());
		cView.addCancelActionListener(new CancelListener());
	}

	/**
	 * check if the Input type correctly
	 * @return boolean
	 */
	public boolean checkInput() 
	{
		try
		{
			if(clientView.getHost().equals("") || clientView.getPort()==0)
			{
				clientView.displayWarnningMessage("please enter some fields!!");
				clientView.clearFields();
				return false;
			}
			return true;
		}
		catch (Exception e)
		{
			clientView.displayWarnningMessage("Error: Use of elegal charchters");
			return false;
		}
	}

	/**
	 * Inner class that handles when Button OK Pressed, implements ActiontListener
	 * 
	 *
	 */
	class OKListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent ev)
		{
			if(checkInput() == false)
				return;
			try 
			{
				String host = new String (clientView.getHost());
				int port = clientView.getPort();
				clientModel.setHost(host);
				clientModel.setPort(port);
				
				MyBoxApp.clien = new MyBoxClient(host,port); //singleton
				
				if(MyBoxApp.clien.isConnected())
				{
					clientView.dispose(); //remove the current window to display login window
				
					//create a new login controller
					LoginGUI logView = new LoginGUI();
					LoginMod logModel = new LoginMod();
					LoginController logController = new LoginController(logView,logModel);
				}
				else
				{
					clientView.displayWarnningMessage("Faild to connect. check IP and port!");
					clientView.clearFields();
				}
				
			} catch (NumberFormatException e) 
			{
				clientView.displayWarnningMessage("Faild to connect. check IP and port!");
				clientView.clearFields();
			
			} 
			catch (IOException e) 
			{
				clientView.displayWarnningMessage("Connection problem. check IP and Port.");
				
			}
		}
	}
	
	/**
	 *  Inner class that handles when Button cancel Pressed, implements ActiontListener
	 *
	 *
	 */
	class CancelListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) 
		{
			clientView.dispose();
			System.exit(1);
		}
	}
}
