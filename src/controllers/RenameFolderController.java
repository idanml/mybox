package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import client.MyBoxApp;
import models.Envelope;
import models.Folders;
import models.User;
import view.MainWindowGUI;
import view.ManageFilesGUI;
import view.ManageFoldersGUI;
import view.ManageGroupsGUI;
import view. RenameFolderGUI;



/**
 * Controller of the rename folder case, handeles all the events on the relavent GUI.
 *
 */
public class RenameFolderController extends  AbstractTransfer {
	
	private User user;
	private LoginController lc;
	private  RenameFolderController rfc;
	private  RenameFolderGUI rfg;
	private Folders fold;
	
	
	/**
	 * The constructor of the class, construct the listeners and attributes of the event of rename folder.
	 * @param user The instance of the logged in user.
	 * @param lc The login controller of the specific login of the user.
	 * @param rg The relavent GUI.
	 * @param folder The instance of folder with the folder needed to be changed.
	 */
	public  RenameFolderController(User user, LoginController lc,  RenameFolderGUI rfg,Folders folder) {
		this.user = user;
		this.lc = lc;
		this.rfc = this;
		this.rfg = rfg;
		this.fold = folder;
		rfg.addbtnHomeActionListener(new HomeListener());
		rfg.addbtnManageGroupsActionListener(new ManageGroupListener());
		rfg.addbtnRenameFolderActionListener(new RenameFolderListener());
		rfg.addbtnCancelActionListener(new CancelListener());
	}

	
	
	/**
	 * Inner class where button Home pressed , implements action listener which get you back to main window.
	 *
	 */
	class HomeListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			rfg.dispose();
			GoHome gh = new GoHome(user);
		}	
	}
	
	
	/**
	 * Inner class where button Manage Groups pressed , implements action listener which opens the manage groups window.
	 *
	 */
	class ManageGroupListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			rfg.dispose();
			ManageGroupsGUI mfg = new ManageGroupsGUI();
			ManageGroupsController mgc = new ManageGroupsController(user,lc,mfg);		
		}	
	}
	
	
	/**
	 * Inner class where button Rename Folder pressed , implements action listener which connects the server to change the folder name.
	 * @author Netanel
	 *
	 */
	class RenameFolderListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			String Use=user.getUserName();
			Use= Use.substring(0,1).toUpperCase() +Use.substring(1);
			String newname= rfg.getFolderNameField();
			
			if(newname==null){
				JOptionPane.showMessageDialog(null,"Enter name.","Error", JOptionPane.ERROR_MESSAGE);
				return;
			}
			fold.setFolderName(fold.getFolderName()+"!!"+newname);
			try{
				Envelope ev1 = new Envelope(fold,"RenameFolder",Use);
				sendToServer(ev1);
				MyBoxApp.clien.setCurrObj(rfc);
			}
			catch(Exception e1){
				 JOptionPane.showMessageDialog(null,"Erorr Connecting To Server!","Error", JOptionPane.ERROR_MESSAGE);  
			}
		}	
	}
	
	
	/**
	 *  Method to handle the answer of the db after trying to change the folder name.
	 *  @param message Answer of the db as Object.
	 */
	public void handleDBResult(Object message)
	{
		String Mess=(String)message;
		if(Mess.equals("RenameFolderFalse")){
			JOptionPane.showMessageDialog(null,"Folder Name Already Exists In The System","ERROR", JOptionPane.ERROR_MESSAGE);
			return;
		}
		else{
		
		JOptionPane.showMessageDialog(null,"Folder Name Changed.","OK", JOptionPane.INFORMATION_MESSAGE);
		rfg.dispose();
		ManageFilesGUI mfg = new ManageFilesGUI();
		ManageFilesController mfc = new ManageFilesController(user,lc,mfg);
		}
	}
	
	
	
	public void HandleDBResult4(Object message)
	{
		JOptionPane.showMessageDialog(null,"Folder Renamed.","OK", JOptionPane.INFORMATION_MESSAGE);
		rfg.dispose();
		ManageFoldersGUI mfg = new ManageFoldersGUI(message);
		ManageFoldersController mfc = new ManageFoldersController(user,lc,mfg);
	}
	
	
	 
	/**
	* Inner class where button Cancel pressed , implements action listener which cancel all the user selects and opens back the manage folders window
	*
	*/
	class CancelListener implements ActionListener
	{

		public void actionPerformed(ActionEvent e) {
			rfg.dispose();
			ManageFilesGUI mfg = new ManageFilesGUI();
			ManageFilesController mfc = new ManageFilesController(user,lc,mfg);	
		}	
	}
	
	
}