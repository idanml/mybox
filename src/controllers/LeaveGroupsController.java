package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import client.MyBoxApp;
import controllers.AddFolderController.HomeListener;
import controllers.AddFolderController.ManageGroupListener;
import models.Envelope;
import models.User;
import view. LeaveGroupsGUI;
import view.MainWindowGUI;
import view.ManageFilesGUI;
import view.ManageGroupsGUI;


/**
 * Controller class for sending group-leave request case, handle all the events in the relavent GUI.
 *
 */
public class LeaveGroupsController extends  AbstractTransfer {
	
	private User user;
	private LoginController lc;
	private  LeaveGroupsController lgc;
	private  LeaveGroupsGUI lgg;
	private String selectedGroup;
	
	
	/**
	 * This is a constructor of the class, construct the listeners and attributes of the case.
	 * @param user The instance of the logged in user.
	 * @param lc The login controller of the specific login of the user.
	 * @param lgg The relavent GUI.
	 */
	public  LeaveGroupsController(User user, LoginController lc,  LeaveGroupsGUI lgg) {
		this.user = user;
		this.lc = lc;
		this.lgc = this;
		this.lgg = lgg;
		this.selectedGroup=null;
		lgg.addbtnHomeActionListener(new HomeListener());
		lgg.addbtnManageFilesActionListener(new ManageFilesListener());
		lgg.addbtnLeaveActionListener(new LeaveListener());
		lgg.addbtnCancelActionListener(new CancelListener());
	}

	
	/**
	 * Inner class where button Home pressed , implements action listener which get you back to main window.
	 *
	 */
	class HomeListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			lgg.dispose();
			GoHome gh = new GoHome(user);
		}	
	}
	
	
	/**
	 * Inner class where button Manage Files pressed , implements action listener which opens the manage files window.
	 *
	 */
	class ManageFilesListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			lgg.dispose();
			ManageFilesGUI mfg = new ManageFilesGUI();
			ManageFilesController mgc = new ManageFilesController(user,lc,mfg);		
		}	
	}
	
	
	/**
	 *  Inner class where button Leave Group pressed , implements action listener which connect the server to insert the request on the db.
	 */
	class LeaveListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			selectedGroup=lgg.getGroupChoice();
			String Use=user.getUserName();
			try{
				Envelope en = new Envelope(selectedGroup,"LeaveGroup",Use);			
				sendToServer(en);
				MyBoxApp.clien.setCurrObj(lgc);	
			}
			catch(Exception e1){
				JOptionPane.showMessageDialog(null,"Erorr Connecting To Server!","Error", JOptionPane.ERROR_MESSAGE);
			}
		}	
	}
	
	
	/**
	 *  Method to handle the answer of the db after trying to send the request.
	 *  @param message Answer of the db as Object.
	 */
	public void handleDBResult(Object message) {
		if(((String) message).equals("LeaveGroupReq") )
		{
			JOptionPane.showMessageDialog(null,"Thank you and goodbye. Admin will respond as soon as possible.","Success", JOptionPane.INFORMATION_MESSAGE);
			lgg.dispose();
			ManageGroupsGUI mfg = new ManageGroupsGUI();
			ManageGroupsController mfc = new ManageGroupsController(user,lc,mfg);
		}
		else{
			JOptionPane.showMessageDialog(null,"Already requested","Error", JOptionPane.ERROR_MESSAGE);
		}
	 }
	
	
	/**
	 * Inner class where button Cancel pressed , implements action listener which cancel all the user selects and opens back the manage files window
	 *
	 */	
	class CancelListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			lgg.dispose();
			ManageGroupsGUI mfg = new ManageGroupsGUI();
			ManageGroupsController mgc = new ManageGroupsController(user,lc,mfg);		
		}	
	}
}