package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import client.MyBoxApp;
import controllers.UpdateFileController.CancelListener;
import controllers.UpdateFileController.HomeListener;
import controllers.UpdateFileController.ManageGroupListener;
import controllers.UpdateFileController.UpdateFileListener;
import models.Envelope;
import models.File;
import models.User;
import view.MainWindowGUI;
import view.ManageFilesGUI;
import view.ManageGroupsGUI;
import view. SeeGroupMembershipGUI;



/**
 * Controller class for See Group Membership case, handle all the events in the relavent GUI.
 *
 */
public class SeeGroupMembershipController extends  AbstractTransfer {
	
	private User user;
	private LoginController lc;
	private  SeeGroupMembershipController sgmc;
	private  SeeGroupMembershipGUI sgmg;
	
	
	/**
	 * This is a constructor of the class, construct the listeners and attributes of the case.
	 * @param user The instance of the logged in user.
	 * @param lc The login controller of the specific login of the user.
	 * @param sgmg The relavent GUI.
	 */
	public  SeeGroupMembershipController(User user, LoginController lc,  SeeGroupMembershipGUI sgmg) {
		this.user = user;
		this.lc = lc;
		this.sgmc = this;
		this.sgmg = sgmg;
		sgmg.addbtnHomeActionListener(new HomeListener());
		sgmg.addbtnManageFilesActionListener(new ManageFilesListener());
		sgmg.addbtnCancelActionListener(new CancelListener());
	}
	
	
	/**
	 * Inner class where button Home pressed , implements action listener which get you back to main window.
	 *
	 */
	class HomeListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			sgmg.dispose();
			GoHome gh = new GoHome(user);

		}	
	}
	
	
	/**
	 * Inner class where button Manage Files pressed , implements action listener which opens the manage files window.
	 *
	 */
	class ManageFilesListener implements ActionListener
	{

		public void actionPerformed(ActionEvent e) {
			sgmg.dispose();
			ManageFilesGUI mfg = new ManageFilesGUI();
			ManageFilesController mfc = new ManageFilesController(user,lc,mfg);	
		}
	}
	
	
	
	/**
	 * Inner class where button Cancel pressed , implements action listener which cancel all the user selects and opens back the manage files window
	 *
	 */	
	class CancelListener implements ActionListener
	{

		public void actionPerformed(ActionEvent e) {
			sgmg.dispose();
			ManageGroupsGUI mgg = new ManageGroupsGUI();
			ManageGroupsController mgc = new ManageGroupsController(user,lc,mgg);
		}
	}
}