package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import client.MyBoxApp;
import models.*;
import view.*;
import controllers.*;
import server.*;

import java.io.IOException;
import java.sql.ResultSet;
import java.util.Observable;
import java.util.concurrent.Executors;




/**
 * This is controller of upload and adding file to my box, handles all the events on relavant GUI.
 *
 */
public class AddFileController extends  AbstractTransfer{
	

	private User user;
	private LoginController lc;
	private AddFileController afc;
	private AddFileGUI afg;
	private java.io.File selectedFile;
	private String fname;
	

	private String desc;
	private String loc;
	private boolean flag;
	
	/**
 	* The constructor of the class, construct the listeners and the attributes for adding file event.
 	* @param user The logged-in user instance
 	* @param lc The login controller of the specific user
 	* @param afg The relavent GUI 
 	*/
	public AddFileController(User user, LoginController lc, AddFileGUI afg) {
		this.afg=afg;
		this.user = user;
		this.lc = lc;
		this.afc = this;
		setFlag(false);
		afg.addbtnHomeActionListener(new HomeListener());
		afg.addbtnAddFileActionListener(new AddFileListener());
		afg.addbtnCancelActionListener(new CancelListener());
		afg.addbtnChooseFileActionListener(new ChooseFileListener());
		afg.addbtnManageGroupsActionListener(new ManageGroupListener());
	}
	
	
	
	/**
	 * Inner class where button Home pressed , implements action listener which get you back to main window
	 *
	 */
	class HomeListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			afg.dispose();
			GoHome gh = new GoHome(user);
		}	
	}
	
	
	/**
	 * Inner class where button Choose File pressed , implements action listener which open for the user a file chooser window
	 *
	 */
	class ChooseFileListener implements ActionListener
	{

		public void actionPerformed(ActionEvent e) {
			JFileChooser chooser = new JFileChooser();
			int returnVal = chooser.showOpenDialog(null);
		    if(returnVal == JFileChooser.APPROVE_OPTION)
		    {
		    	selectedFile = chooser.getSelectedFile();
		    	fname = selectedFile.getName();
	 		    loc= selectedFile.getPath();
	 		   JOptionPane.showMessageDialog(null,loc,"Error", JOptionPane.ERROR_MESSAGE); 
		    }
		}
	}
	
	
	
	/**
	 * Inner class where button Manage Groups pressed , implements action listener which opens the manage groups window
	 *
	 */
	class ManageGroupListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			afg.dispose();
			ManageGroupsGUI mfg = new ManageGroupsGUI();
			ManageGroupsController mgc = new ManageGroupsController(user,lc,mfg);		
		}	
	}
	
	
	/**
	 * Inner class where button Cancel pressed , implements action listener which cancel all the user selects and opens back the manage files window
	 *
	 */
	class CancelListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			afg.dispose();
			ManageFilesGUI mfg = new ManageFilesGUI();
			ManageFilesController mfc = new ManageFilesController(user,lc,mfg);
		}
	}

	
	/**
	 * Inner class where button Add File pressed , implements action listener which start a connection process to add the file
	 *
	 */
	class AddFileListener implements ActionListener
	{

		public void actionPerformed(ActionEvent e) {
			System.out.println("1");
	       desc = afg.getFileDescriptionField();
	       
	       if( fname==null){
	    	   JOptionPane.showMessageDialog(null,"Choose File!","Error", JOptionPane.ERROR_MESSAGE); 
	    	   setFlag(false);
	    	   return;
	       }

	       if( desc.equals("")){
	    	   JOptionPane.showMessageDialog(null,"Enter Description!","Error", JOptionPane.ERROR_MESSAGE);
	    	   setFlag(false);
	    	   return;
	       }
	       
	       // Sending file to server
	       else
	       {
	    	try{ 	  
	    		File FileL = new File(null, null, null, null, null);
	    		FileL.setFName(fname);
	    		FileL.setDesc(desc);
	    		
	    		if(afg.getFileLocationField().equals("Empty"))
	    		{
	    			JOptionPane.showMessageDialog(null,"You Don't Have Folders!","Error", JOptionPane.ERROR_MESSAGE); 
	    			setFlag(false);
	    			return;
	    		}
	    		
	    		
	    		FileL.setPath(afg.getFileLocationField());
	    		
	    		
	    		System.out.println("2");
	    		String Use=user.getUserName();
	    		Envelope ev1 = new Envelope(FileL,"AddFile",Use);
	    		sendToServer(ev1);
	    		testt.SimpleFileClient.main(fname,loc);
	    		MyBoxApp.clien.setCurrObj(afc);
	    		System.out.println("3"); 
			  }
			  catch(Exception e1){
				  setFlag(false);
				  JOptionPane.showMessageDialog(null,"Erorr Connecting To Server!","Error", JOptionPane.ERROR_MESSAGE);    
			  }
	    	}
		}
	}
	
	
	/**
	 * Method to handle the answer of the db after adding a file.
	 * @param message Answer of the db as Object.
	 */
	 public void handleDBResult(Object message) {
		 String str = message.toString();
		 System.out.println("4");
		 if(str.equals("AddFileFalse")){ 
			 System.out.println("5");
			 setFlag(false);
			 JOptionPane.showMessageDialog(null,"File Name Already Exists!","Error", JOptionPane.ERROR_MESSAGE);	 
			 }
		 else{
			 System.out.println("6");
			 	setFlag(true);
			 	JOptionPane.showMessageDialog(null,"File Added","Success", JOptionPane.INFORMATION_MESSAGE);
			 	afg.dispose();
			 	ManageFilesGUI mfg = new ManageFilesGUI();
				ManageFilesController mfc = new ManageFilesController(user,lc,mfg);
		 	} 
	 }
		 

	/**
	 *  Getter for the this controller
	 * @return This controller.
	 */
	public AddFileController getTempL() {
		return afc;
	}
	
	public boolean getFlag()
	{
		return flag;
		
	}
	
	public User getUser() {
		return user;
	}


	public void setUser(User user) {
		this.user = user;
	}


	public String getFname() {
		return fname;
	}


	public void setFname(String fname) {
		this.fname = fname;
	}


	public String getDesc() {
		return desc;
	}


	public void setDesc(String desc) {
		this.desc = desc;
	}


	public String getLoc() {
		return loc;
	}


	public void setLoc(String loc) {
		this.loc = loc;
	}
	
	public void setFlag(boolean flag2) {
		this.flag = flag2;
	}
}


	

