package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;














import javax.swing.JOptionPane;

import client.MyBoxApp;
import models.Envelope;
import models.File;
import models.User;
import view.AddFileGUI;
import view.AddToBoxGUI;
import view.ManageFilesGUI;
import view.WatchApproachableFileListGUI;




/**
 * This is controller of adding file to my personal box, handles all the events on relavant GUI.
 *
 */
public class AddToBoxController extends  AbstractTransfer{


	private User user;
	private LoginController lc;
	private AddFileController afc;
	private AddToBoxGUI atbg;
	private AddToBoxController atbc;
	private WatchApproachableFileListGUI waflg;
	

	
	/**
	 * The constructor of the class, construct the listeners and the attributes for uploading and adding file event.
	 * @param user The logged-in user instance.
	 * @param lc The login controller of the specific user.
	 * @param atbg The relavent GUI.
	 */
	public  AddToBoxController(User user, WatchApproachableFileListGUI lc, AddToBoxGUI atbg) {
			this.atbg=atbg;
			this.user = user;
			this.waflg = lc;
			this.atbc = this;
			atbg.addbtnAddToBoxActionListener(new AddToBoxListener());
			atbg.addbtnCancelActionListener(new CancelListener());

		}
		
	
	
		/**
		 * Inner class where button Add To Box pressed , implements action listener which connect to the db and add the file
		 *
		 */
		class AddToBoxListener implements ActionListener
		{
			public void actionPerformed(ActionEvent e) {

				String path = (atbg.getFileLocationField()).toString();
				String Use=user.getUserName();
				String filename = waflg.getFileField();
				
				try{
					File FileL = new File (filename,path,null,null,null);
					Envelope ev1 = new Envelope(FileL,"AddToBox",Use);	 
				    sendToServer(ev1);
				    MyBoxApp.clien.setCurrObj(atbc);
				}
				catch(Exception e1){
					JOptionPane.showMessageDialog(null,"Erorr Connecting To Server!","Error", JOptionPane.ERROR_MESSAGE);    
				}
			}	
		}
		

		
		/**
		 * Inner class where button Cancel pressed , implements action listener which cancel all the user selects and opens back the manage files window
		 *
		 */
		class CancelListener implements ActionListener
		{
			public void actionPerformed(ActionEvent e) {
				atbg.dispose();
				ManageFilesGUI mfg = new ManageFilesGUI();
				ManageFilesController mfc = new ManageFilesController(user,lc,mfg);
			}
		}

		
	
		
		
		/**
		 * Method to handle the answer of the db after adding a file.
		 * @param message Answer of the db as Object.
		 */
		 public void handleDBResult(Object message) {
			String Mess = message.toString();
			if(Mess.equals("AddToBox"))		 {
				JOptionPane.showMessageDialog(null,"File Added To Your Box!","Success", JOptionPane.INFORMATION_MESSAGE);
				waflg.dispose();
				atbg.dispose();
				ManageFilesGUI mfg = new ManageFilesGUI();
				ManageFilesController mfc = new ManageFilesController(user,lc,mfg);	}
			else{
				JOptionPane.showMessageDialog(null,"File Already In Your Box!","ERROR", JOptionPane.ERROR_MESSAGE);
			 	return;
			 	}
		 }
			 
}

		



