package controllers;

import server.GoogleMail;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import client.MyBoxApp;
import view.ForgotPasswordGUI;
import view.LoginGUI;
import controllers.LoginController.LoginListener;
import controllers.LoginController.forgotPassListener;
import models.Envelope;
import models.LoginMod;
import models.User;

/**
 * Controller of the forgot password event, handle all the events on the relavent GUI
 *
 */
public class ForgotPasswordController extends  AbstractTransfer {
	
	private ForgotPasswordGUI FG;
	private String Uname;
	private ForgotPasswordGUI fpGui;
	private ForgotPasswordController fpc;
	
	/**
	 * This is the constructor of the class, construct the listeners and attributes of the GUI.
	 * @param fpg
	 */
	public ForgotPasswordController(ForgotPasswordGUI fpg) {
		this.fpc=this;
		FG=fpg;
		fpGui=fpg;
		fpGui.addRecoverMyPasswordActionListener(new RecoverMyPasswordListener());
	}
	
	
	/**
	 * Inner class where button Recover My Password pressed , implements action listener which send the user his password to his mail.
	 *
	 */
	class RecoverMyPasswordListener implements ActionListener {
		
		public void actionPerformed(ActionEvent e) {
	
  		  String user = FG.getUserField();	
  		 Envelope en = new Envelope("","ForgotPassword",user);
		 sendToServer(en);	
			MyBoxApp.clien.setCurrObj(fpc);
			}

		}
	
	
	
	public void handleDBResult(Object message)
	{
		
		fpGui.dispose(); 
		
	}

	}
	
	
	
	
	
	
	

