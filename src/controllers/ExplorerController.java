package controllers;

import java.awt.Desktop;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Pattern;

import javafx.scene.input.MouseEvent;

import javax.mail.Folder;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;

import ocsf.client.ObservableClient;
import client.MyBoxApp;
import models.Envelope;
import models.File;
import models.Folders;
import models.LoginMod;
import models.User;
import view.AddFileGUI;
import view.ExplorerGUI;
import view.LoginGUI;
import view.MainWindowGUI;
import controllers.LoginController.CancelListener;
import controllers.LoginController.LoginListener;
import controllers.LoginController.changePassListener;
import controllers.LoginController.forgotPassListener;
import controllers.MainWindowController;



/**
 * Controller to the explorer panel, handles all the events on the panel.
 * Open directory entry.
 * Open file.
 *
 */
public class ExplorerController  extends  AbstractTransfer{
	
	private MouseEvent event;
	private ExplorerGUI eg;
    private ExplorerController ec;
	private Object currController;
	private MainWindowController mwc;
	private MainWindowGUI mwg;
	public static String text1=null;
	private static User user;

	
	/**
	 * This is the constructor of the class, construct all the listeners and attributes of he panel.
	 * @param eg The relavent GUI panel.
	 * @param mwg Main Window GUI frame.
	 * @param user The logged-in user instance.
	 */
	public ExplorerController (ExplorerGUI eg,MainWindowGUI mwg,User user) {
		this.eg=eg;
		ec=this.ec;
		this.mwg=mwg;
		this.user = user;
	
		eg.addFolderButtonsActionListener(new GetFoldersListener());
		eg.addFilesButtonsActionListener(new GetFilesListener());
		
	}
	
	
	/**
	 * Inner class where File pressed , implements action listener which start a connection process to open the file.
	 *
	 */
	class GetFoldersListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() instanceof JButton) 
                text1 = ((JButton) e.getSource()).getText();
			
		
			if(JOptionPane.showConfirmDialog(null,"Going To Open This File\n Ok ?","Confirm",
					  JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) 
			{ 
					
				new Thread() {
					public void run() {
					callMyFunction();
					}
				}.start();

				File FileL2 = new File(text1, text1, text1, text1, text1);
				try{
					Envelope ev2 = new Envelope(FileL2,"OpenFileInClientFromExplorer",null);	
					sendToServer(ev2);     
					MyBoxApp.clien.setCurrObj(ec);
				}
				catch(Exception e1){
					JOptionPane.showMessageDialog(null,"Erorr Connecting To Server!","Error", JOptionPane.ERROR_MESSAGE);
				}
				

			  }
			  else{return;}

		}	
	}
	
	
	/**
	 * Inner class where Folder pressed , implements action listener which start a connection process to show the folder entry.
	 *
	 */
	class GetFilesListener implements ActionListener
	{
		private String text;
		public void actionPerformed(ActionEvent e) {
		
			mwg.dispose();
			if (e.getSource() instanceof JButton) 
                 text = ((JButton) e.getSource()).getText();

			
			models.Folders FolderL = new models.Folders(null,text,null);
			
			String Use=user.getUserName();
			try{
				Envelope ev1 = new Envelope(FolderL,"GetFilesAndFoledersInFolder",Use);
				sendToServer(ev1);
				MyBoxApp.clien.setCurrObj(ec);
			}
			catch(Exception e1){
				JOptionPane.showMessageDialog(null,"Erorr Connecting To Server!","Error", JOptionPane.ERROR_MESSAGE);
			}
		}
	}	
	

	/**
	 * Static method to handle the result from the db after trying to enter a folder.
	 * Change the explorer to the updated list of folders and files.
	 * @param message Answer of the db as Object.
	 */
	public static void Handler(Object message) {

		JFrame frame = new JFrame();
		JMenuBar bar = new JMenuBar();
		frame.setJMenuBar(bar);
		if(bar.getParent().getParent().getParent() instanceof JFrame){
			 ((Window) bar.getParent().getParent().getParent()).dispose();
		}
		     
		String[] parts = new String((String) message).split("@@",2);
		String string1 = parts[1];
		String string2 = parts[0];
		String remove1 = string1.substring(0,string1.indexOf(' ')+1);
		string1 = string1.substring(string1.indexOf(' ')+1);
		String remove2 = string2.substring(0,string2.indexOf(' ')+1);
		string2 = string2.substring(string2.indexOf(' ')+1);
  
		Object Fi=string1;
		Object Fo=string2;
		MainWindowGUI mwg = new MainWindowGUI(Fi,Fo,user);
		MainWindowController mwc = new MainWindowController(user, null, mwg);
	}


	
	
	
	
	/**
	 * Static method to handle the result from the db after trying to open a file.
	 * Open the at the user desktop.
	 * @param message Answer of the db as Object.
	 */
	 public static void handleDBResult2(Object message) {
		String fname = text1;
		
		 final int TIME_VISIBLE = 3000;
		  JOptionPane pane = new JOptionPane("Opening...", JOptionPane.INFORMATION_MESSAGE, JOptionPane.DEFAULT_OPTION, null, new Object[]{}, null);
         JDialog dialog = pane.createDialog(null, "");
         dialog.setModal(false);
         dialog.setVisible(true);

         			try {
						Thread.sleep(TIME_VISIBLE);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
                 dialog.setVisible(false);
		
		
	  	try {
			String sourceFilePath = "C:/MyBoxClient/"+ fname;
	    	java.io.File file = new  java.io.File(sourceFilePath);
	    	Desktop.getDesktop().open(file);
		} catch (Exception e1) {
			e1.printStackTrace();
			return;
		}

	 }
	 
	 /**
	  * Method to call the main at simple file server class.
	  * Download the file.
	  */
	 public void callMyFunction(){
			  SendFileToClient.SimpleFileServer.main(null); 
		}


}
