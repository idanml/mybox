package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import client.MyBoxApp;
import controllers.ChangePermissionsController.HomeListener;
import controllers.ChangePermissionsController.ManageGroupListener;
import controllers.ChangePermissionsController.UpdatePermissionsListener;
import models.Envelope;
import models.Folders;
import models.User;
import view. ChangeFolderLocationGUI;
import view.MainWindowGUI;
import view.ManageFilesGUI;
import view.ManageFoldersGUI;
import view.ManageGroupsGUI;


/**
 * Controller for relocate folder case , handle all the events on the specifc GUI.
 *
 */
public class ChangeFolderLocationController extends  AbstractTransfer {
	
	private User user;
	private LoginController lc;
	private  ChangeFolderLocationController cflc;
	private  ChangeFolderLocationGUI cflg;
	private Folders fold;
	
	
	/**
	 * The constructor of the class, construct the listeners and attributes of the event of changing file location.
	 * @param user The instance of the logged in user.
	 * @param lc The login controller of the specific login of the user.
	 * @param cflg The relavent GUI.
	 * @param fold The name of the folder that gonna be changed.
	 */
	public  ChangeFolderLocationController(User user, LoginController lc,  ChangeFolderLocationGUI cflg,Folders fold) {
		this.user = user;
		this.lc = lc;
		this.cflc = this;
		this.cflg = cflg;
		this.fold=fold;
		cflg.addbtnHomeActionListener(new HomeListener());
		cflg.addbtnManageGroupsActionListener(new ManageGroupListener());
		cflg.addbtnChangeLocationActionListener(new ChangeLocationListener());
		cflg.addbtnCancelActionListener(new CancelListener());
	}
	
	/**
	 * Inner class where button Home pressed , implements action listener which get you back to main window.
	 *
	 */
	class HomeListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			cflg.dispose();
			GoHome gh = new GoHome(user);
		}	
	}
	
	
	/**
	 * Inner class where button Manage Groups pressed , implements action listener which opens the manage groups window.
	 *
	 */
	class ManageGroupListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			cflg.dispose();
			ManageGroupsGUI mfg = new ManageGroupsGUI();
			ManageGroupsController mgc = new ManageGroupsController(user,lc,mfg);		
		}	
	}
	
	
	/**
	 * Inner class where button Change Location pressed , implements action listener which connects the server to change the location.
	 *
	 */
	class ChangeLocationListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
				String Use=user.getUserName();
				String folder= cflg.getSelectedFolder();
				
				if(folder==null){
					JOptionPane.showMessageDialog(null,"Select destination.","Error", JOptionPane.ERROR_MESSAGE);
					return;
				}
				fold.setFolderLocation(fold.getFolderLocation()+"!!"+folder);
				try{
					Envelope ev1 = new Envelope(fold,"ChangeFolderLocation",Use);
					sendToServer(ev1);
					MyBoxApp.clien.setCurrObj(cflc);
				}
				catch(Exception e1){
					JOptionPane.showMessageDialog(null,"Erorr Connecting To Server!","Error", JOptionPane.ERROR_MESSAGE); 
				}

			}				
	}	
	
	
	/**
	 *  Method to handle the answer of the db after trying to change the location.
	 *  @param message Answer of the db as Object.
	 */
	public void handleDBResult(Object message)
	{

		JOptionPane.showMessageDialog(null,"Folder Location Changed.","OK", JOptionPane.INFORMATION_MESSAGE);
		cflg.dispose();
		ManageFilesGUI mfg = new ManageFilesGUI();
		ManageFilesController mfc = new ManageFilesController(user,lc,mfg);
	}
	
	
	
	/**
	 *  Method to handle the answer of the db after succeed to change the location.
	 *  Opens manage folders window.
	 *  @param message Answer of the db as Object.
	 */
	public void HandleDBResult4(Object message)
	{
		JOptionPane.showMessageDialog(null,"Folder relocated.","OK", JOptionPane.INFORMATION_MESSAGE);
		cflg.dispose();
		ManageFoldersGUI mfg = new ManageFoldersGUI(message);
		ManageFoldersController mfc = new ManageFoldersController(user,lc,mfg);
	}
	
	
	/**
	 * Inner class where button Cancel pressed , implements action listener which cancel all the user selects and opens back the manage files window
	 *
	 */
	class CancelListener implements ActionListener
	{

		public void actionPerformed(ActionEvent e) {
			
			cflg.dispose();
			ManageFilesGUI mfg = new ManageFilesGUI();
			ManageFilesController mfc = new ManageFilesController(user,lc,mfg);	
		}
	}
}
	