package server;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Hashtable;

import models.User;

/**
 * This Class is a server Controller 
 *
 */
public class ServerController  {
	
	private ServerGui ServerView;
	private serverLogGui serverLogView;
	private ServerController temp;
	private Connection conn; 
	 private  ArrayList<String> userLog;
	 private String userName = "root";
	 private String password1 = "";
	 private String Defport = "5555";
	 private int port = 0;
	 private String Scheam = "jdbc:mysql://localhost/test";
	 private EchoServer sv;
	/**
	 * constructor
	 * @param SerGui is the start gui that open first when we open 
	 * the server - need to field port user name and password of workbench
	 * @param servLog show when client connect or disconnect to server
	 */
	public ServerController(ServerGui SerGui,serverLogGui servLog){
		ServerView = SerGui;
		serverLogView = servLog;
		temp = this;
		ServerView.setTextFieldPass(password1);
		ServerView.setTextFieldUser(userName);
		ServerView.setTextFieldPort(Defport);
		ServerView.setTextFieldscheam(Scheam);
		userLog = new ArrayList<String>();
		ServerView.addLoginActionListener(new LoginListener());
		serverLogView.addDisconnectedBottonActionListener(new DisconnectedListener());
	}
	
	/**
	 *  Inner class that handles when Button Login Pressed, implements ActiontListener
	 *
	 */
	class LoginListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			
			userName = ServerView.getTextUserName();
			password1 = ServerView.getTextPassword();
			Defport = ServerView.getTextPort();
			port = Integer.parseInt(Defport);
			Scheam = ServerView.getTextScheam();
			
			if(openConnectionDB()){
				 sv = new EchoServer(port);
				 sv.setConn(conn);
				 
				   try 
				    {
				      sv.listen(); //Start listening for connections
				      sv.setController(temp);
				      ServerView.dispose();
				      serverLogView.setVisible(true);
				      
				    } 
				    catch (Exception ex) 
				    {
				    	 ServerView.setWarningMessageVisibleTrue("ERROR - Could not listen for clients!");
				    }
				
			}
			
			
			
		}
	
	}
	/**
	 * openConnectionDB is method that check if the open Connection to DB
	 * @return boolean
	 */
	  public boolean openConnectionDB(){
			try 
			{
	          Class.forName("com.mysql.jdbc.Driver").newInstance();
	      } catch (Exception ex) {/* handle the error*/}
	      
	      try 
	      {
	           conn = DriverManager.getConnection(Scheam,userName,password1);
	          //Connection conn = DriverManager.getConnection("jdbc:mysql://192.168.3.68/test","root","Root");
	          System.out.println("SQL connection succeed");
	          return true;
	          
	   	} catch (SQLException ex) 
	   	    {/* handle any errors*/
	          ServerView.setWarningMessageVisibleTrue("SQLException: " + ex.getMessage());
	          ServerView.setWarningMessageVisibleTrue("SQLState: " + ex.getSQLState());
	          ServerView.setWarningMessageVisibleTrue("VendorError: " + ex.getErrorCode());
	          return false;
	          }
		  
	}
	  /**
	   * Inner class that handles when Button Logout Pressed, implements ActiontListener
	   *
	   */
	class DisconnectedListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
		System.exit(0);
			
		}
	
	}
	/**
	 * set the new log of Employee that connect or disconnect to server
	 * @param e1
	 * @param Task
	 */
	public void SetLog(String e1, String Task){
		ZonedDateTime zonedDateTime = ZonedDateTime.now();
        if(Task.equals("login")){
        	userLog.add(e1);
        	serverLogView.getTextArea().setForeground(Color.green);
        	serverLogView.getTextArea().append(zonedDateTime.toLocalTime()+" : " +" User " + e1 + "- connected\n");	
        }
        if(Task.equals("loginTry")){
        	userLog.add(e1);
        	serverLogView.getTextArea().setForeground(Color.red);
        	serverLogView.getTextArea().append(zonedDateTime.toLocalTime()+" : " +" User " + e1 + "- Tried to log in\n");	
        }
        
        if(Task.equals("login3Try")){
        	userLog.add(e1);
        	serverLogView.getTextArea().setForeground(Color.red);
        	serverLogView.getTextArea().append(zonedDateTime.toLocalTime()+" : " +" User " + e1 + "- Tried to log in 3 times\n");	
        }
        
        
        if(Task.equals("PasswordChange")){
        	serverLogView.getTextArea().setForeground(Color.green);
        	serverLogView.getTextArea().append(zonedDateTime.toLocalTime()+" : " +e1 + "\n");	
        	userLog.remove(e1);
        
        }	
        
        if(Task.equals("AddFile")){
        	serverLogView.getTextArea().setForeground(Color.green);
        	serverLogView.getTextArea().append(zonedDateTime.toLocalTime()+" : " +e1 + "\n");	
        	userLog.remove(e1);
        
        }	

        if(Task.equals("Requests")){
        	serverLogView.getTextArea().setForeground(Color.green);
        	serverLogView.getTextArea().append(zonedDateTime.toLocalTime()+" : " +e1 + "\n");	
        	userLog.remove(e1);
        
        }	
        
        if(Task.equals("DeleteFile")){
        	serverLogView.getTextArea().setForeground(Color.red);
        	serverLogView.getTextArea().append(zonedDateTime.toLocalTime()+" : " +e1 + "\n");	
        	userLog.remove(e1);
        
        }	
        
        
        
        
        if(Task.equals("UpdateFile")){
        	serverLogView.getTextArea().setForeground(Color.red);
        	serverLogView.getTextArea().append(zonedDateTime.toLocalTime()+" : " +e1 + "\n");	
        	userLog.remove(e1);
        
        }	
        
        if(Task.equals("Register")){
        	serverLogView.getTextArea().setForeground(Color.red);
        	serverLogView.getTextArea().append(zonedDateTime.toLocalTime()+" : " +e1 + "\n");	
        	userLog.remove(e1);
        
        }	
        
        
        if(Task.equals("logout")){
        	serverLogView.getTextArea().setForeground(Color.red);
        	serverLogView.getTextArea().append(zonedDateTime.toLocalTime()+" : " +" User " + e1 + "- Disconnected\n");	
        	userLog.remove(e1);
        
        }	
		
	}
	/************************************************Getters and setters***************************************/
	public ServerGui getServerView() {
		return ServerView;
	}

	public void setServerView(ServerGui serverView) {
		ServerView = serverView;
	}

	public serverLogGui getServerLogView() {
		return serverLogView;
	}

	public void setServerLogView(serverLogGui serverLogView) {
		this.serverLogView = serverLogView;
	}
	
	 public void setPassword1(String password1) {
			this.password1 = password1;
		}
	public void setUserName(String userName) {
		this.userName = userName;
	}



}
