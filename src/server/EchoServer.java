package server;



import java.awt.Desktop;
import java.awt.event.MouseAdapter;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.Socket;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.Scanner;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import ocsf.server.*;

import java.util.ArrayList;
import java.util.logging.Logger;
import java.sql.*;

import javax.imageio.ImageIO;
import javax.print.DocFlavor.STRING;
import javax.swing.JOptionPane;


import client.MyBoxApp;
import models.File;
import models.Folders;
import models.Group;
import models.LoginMod;
import models.Envelope;
import models.Request;
import models.User;
/**
 * This class overrides some of the methods in the abstract 
 * superclass in order to give more functionality to the server.
 *
 */
public class EchoServer extends AbstractServer 
{
	private Connection conn;
	private ServerController controller;
	public int LoginCounter=0;
	public String UserNameGlobal;
	ResultSet rs;
  //Class variables *************************************************
  /**
   * The default port to listen on.
   */
	final public static int DEFAULT_PORT = 5555;
  
  //Constructors ****************************************************
  
  /**
   * Constructs an instance of the echo server.
   *
   * @param port The port number to connect on.
   */
	public EchoServer(int port) 
	{
		super(port);
	}

  //Instance methods ************************************************

  /**
   * This method handles any messages received from the client.
   *
   * @param msg The message received from the client.
   * @param client The connection from which the message originated.
   */
	@SuppressWarnings("unchecked")
	public void handleMessageFromClient
    	(Object msg, ConnectionToClient client)
	{
		User user = null;
		Envelope ne=(Envelope) msg;
		String GetFilesRoot="";
	  
		try {
		  Statement stmt = conn.createStatement();  
		  if(ne.getTask().equals("searchLogin")) //Login
		  {
			  UserNameGlobal=((LoginMod)ne.getObject()).getUserName();//user name
			  System.out.println("Login");

			  ResultSet res = stmt.executeQuery("SELECT count(*) FROM users WHERE uname = '"+((LoginMod)ne.getObject()).getUserName()+"';"); //Check If username exists
			  res.next();
				 if ( res.getInt(1) == 0) { //If not exists  
					 System.out.printf("User %s Tried To Log In\n",res.getString(1));
					 client.sendToClient("NoUser");}
				 else{			  
					 ResultSet result = stmt.executeQuery("SELECT COUNT(*) FROM users WHERE uname= '"+((LoginMod)ne.getObject()).getUserName()+"' AND password='"+((LoginMod)ne.getObject()).getPassword()+"';");
					 result.next();
					 if ( result.getInt(1) == 0){ //If not exists   
						 controller.SetLog(((LoginMod)ne.getObject()).getUserName(),"loginTry");
						 client.sendToClient("UserOrPassIncorrect");}
					 else { //If  exists
				   	//Only For Root Folder!   
						 if(UserNameGlobal.equalsIgnoreCase("Admin"))
						 {
							 ArrayList<File> FilesRoot = new ArrayList<File>();
							 ResultSet resF = stmt.executeQuery("select fname from files;");
							 while(resF.next())
								 FilesRoot.add(new File (resF.getString(1)," "," ", " ", " "));
							 for (int i = 0; i < FilesRoot.size(); i++) 
								 GetFilesRoot=GetFilesRoot+(FilesRoot.get(i)).getFName()+" ";	   
						 }
						 else{	   
							 ArrayList<File> FilesRoot = new ArrayList<File>();
							 ResultSet resF = stmt.executeQuery("select filename from filesinfolders,filesinbox,files where  (ubox='"+UserNameGlobal+"'  AND loca='/"+UserNameGlobal+"/'  AND filename=finame  ) AND (owner='"+UserNameGlobal+"' AND location='/"+UserNameGlobal+"/' ) group by filename;");
				 
							 while(resF.next())
								 FilesRoot.add(new File (resF.getString(1)," "," ", " ", " "));
							 for (int i = 0; i < FilesRoot.size(); i++) 
								 GetFilesRoot=GetFilesRoot+(FilesRoot.get(i)).getFName()+" ";	
						 	}
						 ArrayList<Folders> FoldersRoot = new ArrayList<Folders>();
						 ResultSet resFo = stmt.executeQuery("select foname from folders where location='/"+UserNameGlobal+"/';");
						 while(resFo.next()){
							 FoldersRoot.add(new Folders (" ",resFo.getString(1)," "));
						 }
					 	String GetFoldesrRoot="";
						for (int i = 0; i < FoldersRoot.size(); i++) 
							GetFoldesrRoot=GetFoldesrRoot+(FoldersRoot.get(i)).getFolderName()+" ";	
					
						String re = "SELECT uname FROM users WHERE uname= '"+((LoginMod)ne.getObject()).getUserName()+"' AND password='"+((LoginMod)ne.getObject()).getPassword()+"'";
						rs = stmt.executeQuery(re);
				  
						while(rs.next()){
							user = new User(rs.getString(1), re, re, re, re,re);
						}
				  
						ArrayList<Object> LoginObj = new ArrayList<Object>();
						LoginObj.add(user);
						LoginObj.add(GetFilesRoot);
						LoginObj.add(GetFoldesrRoot);		
				  
						controller.SetLog(((LoginMod)ne.getObject()).getUserName(),"login");
						client.sendToClient(LoginObj);
					 }
				 }
	
		  }//end Login		  
		  
		  if(ne.getTask().equals("OpenFileInClient")){//Send file to client 
			  String fname = ((File)ne.getObject()).getFName();
			  String cl = client.toString();
			  ArrayList<String> ClientHost = new ArrayList<String>(Arrays.asList(((String) cl).split(" ")));  
			  System.out.println(ClientHost.get(0));
			  String ClientToSendoTo=ClientHost.get(0);
			  String ffname = fname.substring(0, fname.indexOf(" -"));
			  SendFileToClient.SimpleFileClient.main(ffname,"",ClientToSendoTo);
		
			  client.sendToClient("OpenFile");  
		  }
		  
		  
		  
		  if(ne.getTask().equals("OpenFileInClient3")){//Send file to client(Another Controller)- Same function
			  String fname = ((File)ne.getObject()).getFName();
			  String cl = client.toString();
			  ArrayList<String> ClientHost = new ArrayList<String>(Arrays.asList(((String) cl).split(" ")));  
			  System.out.println(ClientHost.get(0));
			  String ClientToSendoTo=ClientHost.get(0);
			  SendFileToClient.SimpleFileClient.main(fname,"",ClientToSendoTo);
		
			  client.sendToClient("OpenFileAdmin");  
		  }
		  
		  
		  
		  
		  if(ne.getTask().equals("OpenFileInClient2")){//Send file to client(Another Controller)- Same function
			  String fname = ((File)ne.getObject()).getFName();
			  String cl = client.toString();
			  ArrayList<String> ClientHost = new ArrayList<String>(Arrays.asList(((String) cl).split(" ")));  
			  System.out.println(ClientHost.get(0));
			  String ClientToSendoTo=ClientHost.get(0);
			  SendFileToClient.SimpleFileClient.main(fname,"",ClientToSendoTo);
		
			  client.sendToClient("OpenFile3");  
		  }
		  
		  
		  if(ne.getTask().equals("OpenFileInClientFromExplorer")){//Send file to client(Another Controller)- Same function
			  System.out.println("OpenFileInClientFromExplorer");
			  String fname = ((File)ne.getObject()).getFName();
			  String cl = client.toString();
			  ArrayList<String> ClientHost = new ArrayList<String>(Arrays.asList(((String) cl).split(" ")));  
			  System.out.println(ClientHost.get(0));
			  String ClientToSendoTo=ClientHost.get(0);
			  SendFileToClient.SimpleFileClient.main(fname,"",ClientToSendoTo);
		
			  client.sendToClient("OpenFile2");  
		  }
		  
		  
		  
		  
		  if(ne.getTask().equals("GetSearchFiles")){//Get the result of the search (files)
			  System.out.println("GetSearchFiles"); 
			  String UserNameGlobal= ne.getMess();
			  System.out.println(UserNameGlobal);
			  String fSearch=((File) ne.getObject()).getFName();
			  ArrayList<File> Scfile = new ArrayList<File>();
			  ResultSet result = null; 					 
			  result = stmt.executeQuery(
						 "Select * from filesinbox where filename LIKE '"+fSearch+"%';");
			   while(result.next()){
					 Scfile.add(new File (result.getString(1)," "," "," ", " "));
				}
				String GetSearchFiles="GetSearchFiles ";
				for (int i = 0; i < Scfile.size(); i++) 
						GetSearchFiles=GetSearchFiles+" "+(Scfile.get(i)).getFName();	
				client.sendToClient(GetSearchFiles);	  
		  }
		  
  
		  if(ne.getTask().equals("GetApproachFiles")){//Get approachable files for the user
			  System.out.println("GetApproachFiles");
			  String UserNameGlobal= ne.getMess();
			  System.out.println(UserNameGlobal);
			  
			  ArrayList<File> Apfile = new ArrayList<File>();
			  ResultSet result = null; 					 
			  result = stmt.executeQuery(
						 "select FilesInGroups.*,files.description from FilesInGroups,users,usersingroups,files  Where (usersingroups.uname='"+UserNameGlobal+"' AND users.uname='"+UserNameGlobal+"' AND usersingroups.uname=users.uname AND FilesInGroups.gname=usersingroups.gname AND files.fname=FilesInGroups.ffname) OR (files.permission='3' AND usersingroups.uname=users.uname AND FilesInGroups.gname=usersingroups.gname AND files.fname=FilesInGroups.ffname ) GROUP BY ffname;"); //get folders
			   while(result.next()){
					 Apfile.add(new File (result.getString(1),result.getString(2)," ", result.getString(3), " "));
				 }
				String GetApproachFiles="GetApproachFiles";
				for (int i = 0; i < Apfile.size(); i++) 
					GetApproachFiles=GetApproachFiles+"@@"+(Apfile.get(i)).getFName()+" - "+(Apfile.get(i)).getPath()+" - "+(Apfile.get(i)).getDesc();	

				client.sendToClient(GetApproachFiles);  
		  }
		  
		  
		  if(ne.getTask().equals("GetApproachFiles2")){//Get approachable files for the user that he can delete
			  System.out.println("GetApproachFiles2");
			  String UserNameGlobal= ne.getMess();
			  UserNameGlobal= UserNameGlobal.substring(0,1).toUpperCase() +UserNameGlobal.substring(1);
			  ArrayList<File> Apfile2 = new ArrayList<File>();
			  ResultSet result = null; 					 
			   result = stmt.executeQuery(
						 "select filesinbox.filename,files.description from filesinbox,files where ubox='"+UserNameGlobal+"' AND filesinbox.filename=files.fname;"); //get folders
			   while(result.next()){
					 Apfile2.add(new File (result.getString(1)," "," ", result.getString(2), " "));
				}
			
				 String GetApproachFiles="DeGetApproachFiles2";
				for (int i = 0; i < Apfile2.size(); i++) 
						GetApproachFiles=GetApproachFiles+"@@"+(Apfile2.get(i)).getFName()+ " - "+(Apfile2.get(i)).getDesc();	
				client.sendToClient(GetApproachFiles);	  
		  }
  
		  if(ne.getTask().equals("DeleteApproachFile")){//delete approachable file
			  System.out.println("DeleteApproachFile");
			  String UserNameGlobal= ne.getMess();
			  UserNameGlobal= UserNameGlobal.substring(0,1).toUpperCase() +UserNameGlobal.substring(1);
			  String fname = ((File)ne.getObject()).getFName();
	  
			  ResultSet result = null; 			
			  result = stmt.executeQuery("select count(*) from files where fname='"+fname+"' AND owner='"+UserNameGlobal+"';"); // Check if filename  exists
			  result.next();
			  if ( result.getInt(1) == 0) { // If not owner
				 stmt.executeUpdate("DELETE FROM FilesInBox WHERE filename='"+fname+"';");
				 stmt.executeUpdate("DELETE FROM filesinfolders WHERE finame='"+fname+"';");
				 client.sendToClient("DeleteApproachFile");
			 } else {//If owner		 
					  client.sendToClient("DeleteApproachFileFalse");
			 }  
		  }
		  
  
		  if(ne.getTask().equals("ChangePermission")){//Change permission for a file
		  System.out.println("ChangePermission");
			  
		   try 
			{ 
					 String fname = ((File)ne.getObject()).getFName();
					 String newper= ((File)ne.getObject()).getPermission();
					 String Oldper ="";
		
					 String UserNameGlobal= ne.getMess();
					 UserNameGlobal= UserNameGlobal.substring(0,1).toUpperCase() +UserNameGlobal.substring(1);
					  
					 ResultSet result = null; 			
					 result = stmt.executeQuery("select permission from files where fname='"+fname+"';"); // Check if filename  exists
					 while(result.next()){
							 Oldper =  result.getString(1);}
						 
					 if(Oldper.equals(newper)) {
							 client.sendToClient("ChangePermission1");	
					 }
					else if(newper.equalsIgnoreCase("3")){
							 ArrayList<String> gnames = new ArrayList<String>();
							 ResultSet GroupNames = null; 
							 
							 GroupNames = stmt.executeQuery("SELECT usersingroups.gname FROM usersingroups WHERE usersingroups.uname='"+UserNameGlobal+"';");//Get User Groups
							 while(GroupNames.next())
								 gnames.add(GroupNames.getString(1));
							 
							 for(int i=0;i<gnames.size(); i++)
								 stmt.executeUpdate(
										"INSERT INTO `FilesInGroups` " + " VALUES ( '"+fname+"', '"+gnames.get(i)+"');");//insert into FilesInGroups
							 
							 PreparedStatement ps;
							 ps = conn.prepareStatement("update files set permission = ? where fname = '"+fname+"';");
							 ps.setString(1, newper);
							 ps.executeUpdate();
							  
							  
							 if(newper.equals("1"))
							  {
								  stmt.executeUpdate("DELETE FROM FilesInGroups WHERE ffname='"+fname+"';");		  
							  }
							  
							  System.out.println("\nDB Updated\n");
							   
							  controller.SetLog(UserNameGlobal + ": " + "Change Permission - " + fname + " to " + newper,"ChangePermission");
							  client.sendToClient("ChangePermission");

					}	
					else{  
						PreparedStatement ps;
						ps = conn.prepareStatement("update files set permission = ? where fname = '"+fname+"';");
						ps.setString(1, newper);
						ps.executeUpdate();

						if(newper.equals("1"))
						{
						  stmt.executeUpdate("DELETE FROM FilesInGroups WHERE ffname='"+fname+"';");		  
						}
					  
						System.out.println("\nDB Updated\n");
					   
						controller.SetLog(UserNameGlobal + ": " + "Change Permission - " + fname + " to " + newper,"ChangePermission");
						client.sendToClient("ChangePermission");}
				} catch (SQLException e) {e.printStackTrace(); }
		  }
		  
		  
		  if(ne.getTask().equals("ChangeDescription")){//Change Description for a file
			  System.out.println("ChangeDescription");
			  
			   try 
				{

					 String fname = ((File)ne.getObject()).getFName();
					 String newdesc= ((File)ne.getObject()).getDesc();
		
					  String UserNameGlobal= ne.getMess();
					  UserNameGlobal= UserNameGlobal.substring(0,1).toUpperCase() +UserNameGlobal.substring(1);

					  PreparedStatement ps;
					  ps = conn.prepareStatement("update files set description = ? where fname = '"+fname+"';");
					  ps.setString(1, newdesc);
					  ps.executeUpdate();
					  
					  System.out.println("\nDB Updated\n");
					   
					  controller.SetLog(UserNameGlobal + ": " + "Change Description - " + fname + " to " + newdesc,"ChangeDescription");
					  client.sendToClient("ChangeDescription");

				} catch (SQLException e) {e.printStackTrace(); }
  
		  }
		  
		  
		  if(ne.getTask().equals("AdminChangeDescription")){//Change Description for a file (Admin GUI) Same function,this one also sends notification mail to users
			  System.out.println("ChangeDescription");
			  
			   try 
				{
				     // File file = new File();
					 String fname = ((File)ne.getObject()).getFName();
					 String newdesc= ((File)ne.getObject()).getDesc();
		
					  String UserNameGlobal= ne.getMess();
					  UserNameGlobal= UserNameGlobal.substring(0,1).toUpperCase() +UserNameGlobal.substring(1);

					  PreparedStatement ps;
					  ps = conn.prepareStatement("update files set description = ? where fname = '"+fname+"';");
					  ps.setString(1, newdesc);
					  ps.executeUpdate();
					  
					  System.out.println("\nDB Updated\n");
					   
					  ArrayList<String> UsersToSend = new ArrayList<String>();
					  ResultSet rresult = null; 					 
					  rresult = stmt.executeQuery("select ubox from filesinbox where filename='"+fname+"';"); //get folders
					  while(rresult.next())
								  UsersToSend.add(rresult.getString(1));
							  
					  for (int i=0;i<UsersToSend.size();i++) {
								final int j=i;
								 new Thread() {
									    @Override
									    public void run() {
									    	 SendNotificationEmail(UsersToSend.get(j),fname);
									    }
									}.start();
					   }
					   
					   controller.SetLog(UserNameGlobal + ": " + "Change Description - " + fname + " to " + newdesc,"ChangeDescription");
					   client.sendToClient("AdminChangeDescription");

				} catch (SQLException e) {e.printStackTrace(); }
  
		  }
		  
		  
		  if(ne.getTask().equals("Rename")){//Change Name for a file
			  System.out.println("Rename");
			  
			   try 
				{
					 String fname = ((File)ne.getObject()).getFName();
					 String newname= ((File)ne.getObject()).getPath();
		
					  String UserNameGlobal= ne.getMess();
					  UserNameGlobal= UserNameGlobal.substring(0,1).toUpperCase() +UserNameGlobal.substring(1);

					 ResultSet result = null; 					 
					 result = stmt.executeQuery("SELECT count(*) FROM files WHERE fname = '"+newname+"';"); // Check if filename already exists
					 result.next();
					 if ( result.getInt(1) == 0) { //If not exists, Add to DB
						 PreparedStatement ps;
						 ps = conn.prepareStatement("update filesinfolders set finame = ? where finame = '"+fname+"';");
						 ps.setString(1, newname);
						 ps.executeUpdate();

						 PreparedStatement ps1;
						 ps1 = conn.prepareStatement("update filesinbox set filename = ? where filename = '"+fname+"';");
						 ps1.setString(1, newname);
						 ps1.executeUpdate();

						 PreparedStatement ps2;
						 ps2 = conn.prepareStatement("update files set fname = ? where fname = '"+fname+"';");
						 ps2.setString(1, newname);
						 ps2.executeUpdate();

						 PreparedStatement ps3;
						 ps3 = conn.prepareStatement("update filesingroups set ffname = ? where ffname = '"+fname+"';");
						 ps3.setString(1, newname);
						 ps3.executeUpdate();
				 
					  
					  		try {
							String sourceFilePath = "C:/MyBox/"+ fname;
							String destinationFilePath = "C:/MyBox/"+ newname;
					    	java.io.File file = new  java.io.File(sourceFilePath);
					    	java.io.File file2 = new  java.io.File(destinationFilePath);
					    	file.renameTo(file2);
					  		} catch (Exception e1) {
							e1.printStackTrace();
							return;
					  		}
					  		
					  		System.out.println("\nDB Updated\n");
					   
					  		controller.SetLog(UserNameGlobal + ": " + "Rename - " + fname + " to " + newname,"Rename");
					  		client.sendToClient("Rename");
					 }
					 else { //If  exists, Show error message
						   System.out.println("\nFilename Already Exists...\n");
						   client.sendToClient("RenameFileFalse");
					 }

				} catch (SQLException e) {e.printStackTrace(); }
		  }

		  
		  if(ne.getTask().equals("RenameAdmin")){//Change Name for a file (Admin) - Same function,this one also sends notification mail to users
			  System.out.println("Rename");
			  
			   try 
				{
				   
	
					 String fname = ((File)ne.getObject()).getFName();
					 String newname= ((File)ne.getObject()).getPath();
		
					  String UserNameGlobal= ne.getMess();
					  UserNameGlobal= UserNameGlobal.substring(0,1).toUpperCase() +UserNameGlobal.substring(1);

					  ResultSet result = null; 					 
					  result = stmt.executeQuery("SELECT count(*) FROM files WHERE fname = '"+newname+"';"); // Check if filename already exists
					  result.next();
					 if ( result.getInt(1) == 0) { //If not exists, Add to DB
						  ArrayList<String> UsersToSend = new ArrayList<String>();
						  ResultSet rresult = null; 					 
						  rresult = stmt.executeQuery("select ubox from filesinbox where filename='"+fname+"';"); //get folders
						  while(rresult.next())
								  UsersToSend.add(rresult.getString(1));
							  
							 for (int i=0;i<UsersToSend.size();i++) {
								 final int j=i;
								  new Thread() {
									    @Override
									    public void run() {
									    	 SendNotificationEmail(UsersToSend.get(j),fname);
									    }
									}.start();
							    }

						 
							 PreparedStatement ps;
							 ps = conn.prepareStatement("update filesinfolders set finame = ? where finame = '"+fname+"';");
							 ps.setString(1, newname);
							 ps.executeUpdate();

							 PreparedStatement ps1;
							 ps1 = conn.prepareStatement("update filesinbox set filename = ? where filename = '"+fname+"';");
							 ps1.setString(1, newname);
							 ps1.executeUpdate();

							 PreparedStatement ps2;
							 ps2 = conn.prepareStatement("update files set fname = ? where fname = '"+fname+"';");
							 ps2.setString(1, newname);
							 ps2.executeUpdate();
							 
							 PreparedStatement ps3;
							 ps3 = conn.prepareStatement("update filesingroups set ffname = ? where ffname = '"+fname+"';");
							 ps3.setString(1, newname);
							 ps3.executeUpdate();
				 
					  		try {
					  			String sourceFilePath = "C:/MyBox/"+ fname;
					  			String destinationFilePath = "C:/MyBox/"+ newname;
					  			java.io.File file = new  java.io.File(sourceFilePath);
					  			java.io.File file2 = new  java.io.File(destinationFilePath);
					  			file.renameTo(file2);
					  		} catch (Exception e1) {
							e1.printStackTrace();
							return;
						}

					   System.out.println("\nDB Updated\n");
					   
					   controller.SetLog(UserNameGlobal + ": " + "Rename - " + fname + " to " + newname,"Rename");
					   client.sendToClient("RenameAdmin");
					 }
					 else { //If  exists, Show error message
						   System.out.println("\nFilename Already Exists...\n");
						   client.sendToClient("RenameAdminFileFalse");
					 }	 

				} catch (SQLException e) {e.printStackTrace(); }
  
		  }

		  
		  
		  if(ne.getTask().equals("ChangeFileLocation")){//Change File Location
			  System.out.println("ChangeFileLocation");
			  
			   try 
				{
					 String fname = ((File)ne.getObject()).getFName();
					 String newloc= ((File)ne.getObject()).getPath();
		
					  String UserNameGlobal= ne.getMess();
					  UserNameGlobal= UserNameGlobal.substring(0,1).toUpperCase() +UserNameGlobal.substring(1);

				
					  PreparedStatement ps;
					  ps = conn.prepareStatement("update files set location = ? where fname = '"+fname+"';");
					  ps.setString(1, newloc);
					  ps.executeUpdate();
					  PreparedStatement ps1;
					  ps1 = conn.prepareStatement("update filesinfolders set loca = ? where finame = '"+fname+"';");
					  ps1.setString(1, newloc);
					  ps1.executeUpdate();
					  
					  System.out.println("\nDB Updated\n");
					   
					  controller.SetLog(UserNameGlobal + ": " + "Change File Location  - " + fname + " to " + newloc,"ChangeFileLocation");
					  client.sendToClient("ChangeFileLocation");

				} catch (SQLException e) {e.printStackTrace(); }
		  }
		  
		  
		  
		  if(ne.getTask().equals("AddToBox")){//Add File to personal box of a user
			  System.out.println("AddToBox");
				 		  
			  String fname = ((File)ne.getObject()).getFName();
			  fname=fname.substring(0, fname.indexOf(" ")); 
			  String Loc = ((File)ne.getObject()).getPath();
			  String UserNameGlobal= ne.getMess();
			  UserNameGlobal= UserNameGlobal.substring(0,1).toUpperCase() +UserNameGlobal.substring(1);
			  ResultSet result = null; 
			  result = stmt.executeQuery("SELECT count(*) FROM FilesInBox WHERE filename = '"+fname+"' AND ubox = '"+UserNameGlobal+"';"); // Check if filename already exists
			  result.next();
			  if ( result.getInt(1) == 0) { //If not exists, Add to DB
					 	stmt.executeUpdate(
						"INSERT INTO `FilesInBox` " + " VALUES ( '"+fname+"', '"+UserNameGlobal+"');");
					 	
						stmt.executeUpdate(
								"INSERT INTO `FilesInFolders` " + " VALUES ( '"+fname+"', '"+Loc+"');");
						
					 	System.out.println("\nDB Updated\n");
					 	client.sendToClient("AddToBox");
			  } else { //If  exists, Show error message
					   	System.out.println("\nFilename Already Exists In Your Box...\n");
					   	client.sendToClient("AddToBoxFalse");
					 }
		  }
		  
		  

		  
		  if(ne.getTask().equals("JoinGroup"))//Join Group request
		  {
			  System.out.println("JoinGroup");
			  String UserNameGlobal= ne.getMess();
			  UserNameGlobal= UserNameGlobal.substring(0,1).toUpperCase() +UserNameGlobal.substring(1);
			  String group=(ne.getObject()).toString();
			  System.out.println(group);
			  ResultSet result = null; 
			  result = stmt.executeQuery("select count(*) from groupsrequests where gname='"+group+"' and uname='"+ UserNameGlobal+"';");
			  result.next();
			  System.out.println("number of requests:"+ result.getInt(1));
			  if ( result.getInt(1) == 0)
			  {
				  stmt.executeUpdate("INSERT INTO `groupsrequests` VALUES ('"+group+"','1','"+UserNameGlobal+"');");
				  client.sendToClient("JoinGroupReq");
			  }
			  else
				  client.sendToClient("JoinGroupReqFailed");  
		  }
		  
		  
		  
		  if(ne.getTask().equals("LeaveGroup"))//Leave Group request
		  {
			  System.out.println("LeaveGroup");
			  String UserNameGlobal= ne.getMess();
			  UserNameGlobal= UserNameGlobal.substring(0,1).toUpperCase() +UserNameGlobal.substring(1);
			  String group=(ne.getObject()).toString();
			  System.out.println(group);
			  ResultSet result = null; 
			  result = stmt.executeQuery("select count(*) from groupsrequests where gname='"+group+"' and uname='"+ UserNameGlobal+"';");
			  result.next();
			  if ( result.getInt(1) == 0)
			  {
				  stmt.executeUpdate("INSERT INTO `groupsrequests` VALUES ('"+group+"','2','"+UserNameGlobal+"');");
				  client.sendToClient("LeaveGroupReq");
			  }
			  else
				  client.sendToClient("LeaveGroupReqFailed");	  
		  }
		  
		  
		  if(ne.getTask().equals("GetRegGroups")) // Get registered groups and show only the unregistered
		  {
			  String UserNameGlobal= ne.getMess();
			  UserNameGlobal= UserNameGlobal.substring(0,1).toUpperCase() +UserNameGlobal.substring(1);
			  String uname = UserNameGlobal;
			  System.out.println("GetRegisteredGroups");
			  String SendRegGroups = new String();
			  ResultSet result = null;
			  result = stmt.executeQuery("SELECT distinct gname FROM usersingroups where uname LIKE '" + uname + "'");
			  SendRegGroups = "SendRegGroups";
			  while (result.next()){
				  SendRegGroups=SendRegGroups +" "+(result.getString(1));
			  }
			  
			  client.sendToClient(SendRegGroups);
		  }

		  
		  if(ne.getTask().equals("GetUnregGroups")) // Get unregistered groups and show only the registered
		  {
			  String UserNameGlobal= ne.getMess();
			  UserNameGlobal= UserNameGlobal.substring(0,1).toUpperCase() +UserNameGlobal.substring(1);
			  String uname = UserNameGlobal;
			  System.out.println("GetUnregisteredGroups");
			  String SendUnregGroups = new String();
			  ResultSet result = null;
			  result = stmt.executeQuery("SELECT distinct gname from groups WHERE gname NOT IN (SELECT distinct gname FROM usersingroups where uname LIKE '" + uname + "')");
			  SendUnregGroups = "SendUnregGroups";
			  while (result.next()){
				  SendUnregGroups=SendUnregGroups +" "+(result.getString(1));
			  }
			  System.out.println(SendUnregGroups);
			  client.sendToClient(SendUnregGroups);
		  }

		  
		  
		  if(ne.getTask().equals("GetRequests")) // Get Join or leave requests for admin's gui
		  {
			  System.out.println("GetRequests");
			  ResultSet result = null;
			  result = stmt.executeQuery("select count(*) from groupsrequests");
			  result.next();
			  if ( result.getInt(1) == 0){
				  client.sendToClient("NoRequest");
			  	   return;}
			  
			  else{
			  
			  ArrayList<Request> request = new ArrayList<Request>();

			   result = null;
			  result = stmt.executeQuery("select * from groupsrequests");

			  while (result.next()){
				  request.add(new Request (result.getString(1),result.getInt(2),result.getString(3)));	
			  }
			  
			  client.sendToClient(request);
			  return;
			  	}
			 }
		  
		  
		  
		  if(ne.getTask().equals("RejectJoinRequest")) // admin - reject request
		  {
			  String uname = ((Request) ne.getObject()).getUserName();
			  String gname = ((Request) ne.getObject()).getGroupName();
			  int type = ((Request) ne.getObject()).getRequestType();
		  
			  stmt.executeUpdate("DELETE FROM groupsrequests WHERE gname='"+gname+"' AND uname='"+uname+"';");
			  new Thread() {
			    @Override
			    public void run() {
			    	SendRejectEmail(uname,gname);
			    }
			}.start();

			client.sendToClient("RequestRejected");
			return;	  
		  }
		  
		  
		  if(ne.getTask().equals("ApproveJoinRequest")) // admin - approve request
		  {
			  String uname = ((Request) ne.getObject()).getUserName();
			  String gname = ((Request) ne.getObject()).getGroupName();
			  int type = ((Request) ne.getObject()).getRequestType();
			  
			  if(type==1){
				  	stmt.executeUpdate(
						"INSERT INTO `usersingroups` " + " VALUES ( '"+gname+"', '"+uname+"');");
				  	stmt.executeUpdate("DELETE FROM groupsrequests WHERE gname='"+gname+"' AND uname='"+uname+"';");
				  	new Thread() {	  
				  		@Override
				  		public void run() {
				  			SendApprovesEmail(uname,gname);
				  		}
				  	}.start();
			
				  	controller.SetLog(uname + ": " + "Joined Group - " + gname,"Requests");
		 
			  }

			  else{
				  stmt.executeUpdate("DELETE FROM usersingroups WHERE gname='"+gname+"' AND uname='"+uname+"';");
				  stmt.executeUpdate("DELETE FROM groupsrequests WHERE gname='"+gname+"' AND uname='"+uname+"';");
				  new Thread() {
					    @Override
					    public void run() {
					    	 SendApprovesEmail(uname,gname);
					    }
					}.start();
					
					controller.SetLog(uname + ": " + "Left Group - " + gname,"Requests");
			  }
				    
			  client.sendToClient("RequestApproved");  

		  }
		  
		  
		  
		  if(ne.getTask().equals("GetFolders")){//Get folder names
			  System.out.println("GetFolders");
			  String UserNameGlobal= ne.getMess();
			  UserNameGlobal= UserNameGlobal.substring(0,1).toUpperCase() +UserNameGlobal.substring(1);
			  ArrayList<Folders> folder = new ArrayList<Folders>();
			  ResultSet result = null; 					 
			  result = stmt.executeQuery("SELECT * FROM folders WHERE owner = '"+UserNameGlobal+"';"); //get folders
			  while(result.next()){
					 folder.add(new Folders (result.getString(1),result.getString(2),result.getString(3)));
			  }
			  String SendFolders="SendFolders";
			  for (int i = 0; i < folder.size(); i++) 
					SendFolders=SendFolders+" "+(folder.get(i)).getFolderName()+(folder.get(i)).getFolderOwner()+"/";

			  client.sendToClient(SendFolders);	  
		  }

		  
		  
		  if(ne.getTask().equals("GetFolders2")){//Get folder names - same function different controller
			  System.out.println("GetFolders2");
			  String UserNameGlobal= ne.getMess();
			  UserNameGlobal= UserNameGlobal.substring(0,1).toUpperCase() +UserNameGlobal.substring(1);
			  ArrayList<Folders> folder = new ArrayList<Folders>();
			  ResultSet result = null; 					 
			  result = stmt.executeQuery("SELECT * FROM folders WHERE owner = '"+UserNameGlobal+"';"); //get folders
			  while(result.next()){
					 folder.add(new Folders (result.getString(1),result.getString(2),result.getString(3)));
			  }
			  String SendFolders2="2SendFolders";
			  for (int i = 0; i < folder.size(); i++) 
					SendFolders2=SendFolders2+" "+(folder.get(i)).getFolderName()+(folder.get(i)).getFolderOwner()+"/";
			  client.sendToClient(SendFolders2);  
		  }
		  
		  
		  if(ne.getTask().equals("GetFolders3")){//Get folder names - same function different controller
			  System.out.println("GetFolders");
			  String UserNameGlobal= ne.getMess();
			  UserNameGlobal= UserNameGlobal.substring(0,1).toUpperCase() +UserNameGlobal.substring(1);
			  ArrayList<Folders> folder = new ArrayList<Folders>();
			  ResultSet result = null; 					 
			  result = stmt.executeQuery("SELECT * FROM folders WHERE owner = '"+UserNameGlobal+"';"); //get folders
			  while(result.next()){
				  folder.add(new Folders (result.getString(1),result.getString(2),result.getString(3)));
			  }
			  String SendFolders3="3SendFolders";
			  for (int i = 0; i < folder.size(); i++) 
					SendFolders3=SendFolders3+" "+(folder.get(i)).getFolderName()+(folder.get(i)).getFolderOwner()+"/";
			  client.sendToClient(SendFolders3);  
		  }
		  
		  
		  
		  if(ne.getTask().equals("GetFolders4")){//Get folder names for admin gui
			  String UserNameGlobal=ne.getMess();
			  UserNameGlobal= UserNameGlobal.substring(0,1).toUpperCase() +UserNameGlobal.substring(1);
			  String flag= (String) ne.getObject();
			  System.out.println("GetFolders");
			  ArrayList<Folders> folders = new ArrayList<Folders>();
			  ResultSet result = null; 					 
			  result = stmt.executeQuery("SELECT * FROM folders WHERE owner = '"+UserNameGlobal+"';"); //get folders
			  while(result.next()){
				  folders.add(new Folders(result.getString(3),result.getString(1),result.getString(2)));
				  }
			  String SendFolders;
			  if(flag=="1")
				  SendFolders="1DeleteFolder";
			  else
				  if(flag=="2")
					  SendFolders="1RenameFolder";
				  else
					  if(flag=="3")
						  SendFolders="1ChangeFolderLocation";
					  else
						  if(flag=="4")
							  SendFolders="1AddFolder";
						  else 
							  SendFolders="ManageFolders";
			  		
			  
				for (int i = 0; i < folders.size(); i++) {
					int count = folders.get(i).getFolderLocation().length() - folders.get(i).getFolderLocation().replace("/", "").length();
					if(count==2)
						SendFolders=SendFolders+" /"+(folders.get(i)).getFolderName()+"/";
					else
					{
						int n= UserNameGlobal.length()+1;
						String edited = folders.get(i).getFolderLocation().substring(n);
						SendFolders=SendFolders+" "+edited+(folders.get(i)).getFolderName()+"/";
					}
				}
				client.sendToClient(SendFolders);   
		  }
		  
		  

		  if(ne.getTask().equals("GetFolders5")){//Get folder names - same function different controller
			  System.out.println("GetFolders");
			  String UserNameGlobal= ne.getMess();
			  UserNameGlobal= UserNameGlobal.substring(0,1).toUpperCase() +UserNameGlobal.substring(1);
			  ArrayList<Folders> folder = new ArrayList<Folders>();
			  ResultSet result = null; 					 
				 result = stmt.executeQuery("SELECT * FROM folders WHERE owner = '"+UserNameGlobal+"';"); //get folders
				 while(result.next()){
					 folder.add(new Folders (result.getString(1),result.getString(2),result.getString(3)));
				 }
				 	String SendFolders="5SendFolders";
				 	SendFolders=SendFolders+" "+"/"+UserNameGlobal+"/";
					for (int i = 0; i < folder.size(); i++) 
						SendFolders=SendFolders+" "+(folder.get(i)).getFolderName()+(folder.get(i)).getFolderOwner()+"/";
					
				        client.sendToClient(SendFolders);
		  }
		  
		  
		  if(ne.getTask().equals("GetFolders6")){//Get folder names - same function different controller
			  System.out.println("GetFolders");
			  String UserNameGlobal= ne.getMess();
			  String folderName= ((Folders) ne.getObject()).getFolderName();
			  String folderLoc= ((Folders) ne.getObject()).getFolderLocation();
			  UserNameGlobal= UserNameGlobal.substring(0,1).toUpperCase() +UserNameGlobal.substring(1);
			  ArrayList<Folders> folder = new ArrayList<Folders>();
			  ResultSet result = null; 					 
			result = stmt.executeQuery("SELECT * FROM folders WHERE owner = '"+UserNameGlobal+"' AND( foname<>'"+folderName+"' AND location<>'"+folderLoc+"');"); //get folders
			 while(result.next()){
				 folder.add(new Folders (result.getString(1),result.getString(2),result.getString(3)));
				 }
			String SendFolders="6SendFolders";
			SendFolders=SendFolders+" "+"/"+UserNameGlobal+"/";
			for (int i = 0; i < folder.size(); i++) 
				SendFolders=SendFolders+" "+(folder.get(i)).getFolderName()+(folder.get(i)).getFolderOwner()+"/";
			
			client.sendToClient(SendFolders);  
		  }
		  
		  
		  
		  if(ne.getTask().equals("AddFile")){//Add File to database
			  System.out.println("AddFile");
			  
			   try 
				{
				     	
				   String fname = ((File)ne.getObject()).getFName();
				   String desc= ((File)ne.getObject()).getDesc();
				   String path = ((File)ne.getObject()).getPath();
				   ArrayList<String> gnames = new ArrayList<String>();
				   String UserNameGlobal= ne.getMess();
				   UserNameGlobal= UserNameGlobal.substring(0,1).toUpperCase() +UserNameGlobal.substring(1);

				   testt.SimpleFileServer.main(null);
				
					 
				   ResultSet result = null; 					 
				   result = stmt.executeQuery("SELECT count(*) FROM files WHERE fname = '"+fname+"';"); // Check if filename already exists
				   result.next();
				   if ( result.getInt(1) == 0) { //If not exists, Add to DB
					   	stmt.executeUpdate(
							"INSERT INTO `files` " + " VALUES ( '"+fname+"', '"+path+"', '"+UserNameGlobal+"' , '"+desc+"', '3');");
					   	
					   	stmt.executeUpdate(
								"INSERT INTO `FilesInBox` " + " VALUES ( '"+fname+"', '"+UserNameGlobal+"');");
					   
					 	stmt.executeUpdate(
								"INSERT INTO `filesinfolders` " + " VALUES ( '"+fname+"', '"+path+"');");
					 	
					 	System.out.println("\nDB Updated\n");
					 	controller.SetLog(UserNameGlobal + ": " + "Added File - " + fname,"AddFile");
					 	client.sendToClient("AddFile");
					 } else { //If  exists, Show error message
					   System.out.println("\nFilename Already Exists...\n");
					   client.sendToClient("AddFileFalse"); 
					 }
										 
					 ResultSet GroupNames = null; 
					 
					 GroupNames = stmt.executeQuery("SELECT usersingroups.gname FROM usersingroups WHERE usersingroups.uname='"+UserNameGlobal+"';");//Get User Groups
					 while(GroupNames.next())
						 gnames.add(GroupNames.getString(1));
					 
					 for(int i=0;i<gnames.size(); i++)
						 stmt.executeUpdate(
								"INSERT INTO `FilesInGroups` " + " VALUES ( '"+fname+"', '"+gnames.get(i)+"');");//insert into FilesInGroups

				} catch (SQLException e) {e.printStackTrace(); }
		  }
		  
		  
		  
 
		  if(ne.getTask().equals("UpdateFile")){//update existing file 
			  System.out.println("UpdateFile");
			  
			   try 
				{
					 String fname = ((File)ne.getObject()).getFName();

					  String UserNameGlobal= ne.getMess();
					  UserNameGlobal= UserNameGlobal.substring(0,1).toUpperCase() +UserNameGlobal.substring(1);

					 ResultSet result = null; 					 
					 result = stmt.executeQuery("SELECT count(*) FROM files WHERE fname = '"+fname+"';"); // Check if filename already exists
					 result.next();
					 if ( result.getInt(1) == 0) { //If not exists, Add to DB
						   System.out.println("\nFilename Not Exists...\n");
						   testt.SimpleFileServer.main(null);
						   client.sendToClient("UpdateFileFalse");
	
					 } else { //If  exists, Show error message
						 
						 try {
							 String sourceFilePath = "C:/MyBox/"+ fname;
					    	 java.io.File fileDelete = new  java.io.File(sourceFilePath);

					    	  fileDelete.delete();
						} catch (Exception e1) {
							e1.printStackTrace();
							return;
						}
						 
						 testt.SimpleFileServer.main(null);
						 System.out.println("\nDB Updated\n");
						  controller.SetLog(UserNameGlobal + ": " + "Updated File - " + fname,"UpdateFile");
						  client.sendToClient("UpdateFile");}
				} catch (SQLException e) {e.printStackTrace(); }
		  }
		  
		  
		  
		  
		  if(ne.getTask().equals("UpdateFile2")){//update existing file  - same function
			  System.out.println("UpdateFile2");
			  
			   try 
				{
					 String fname = ((File)ne.getObject()).getFName();

					  String UserNameGlobal= ne.getMess();
					  UserNameGlobal= UserNameGlobal.substring(0,1).toUpperCase() +UserNameGlobal.substring(1);

					  ResultSet result = null; 					 
					  result = stmt.executeQuery("SELECT count(*) FROM files WHERE fname = '"+fname+"';"); // Check if filename already exists
					  result.next();
					  if ( result.getInt(1) == 0) { //If not exists, Add to DB
						   System.out.println("\nFilename Not Exists...\n");
						   testt.SimpleFileServer.main(null);
						   client.sendToClient("UpdateFileFalse2");

					 } else { //If  exists, Show error message

						   try {
							String sourceFilePath = "C:/MyBox/"+ fname;
					    	java.io.File fileDelete = new  java.io.File(sourceFilePath);

					    	fileDelete.delete();
						} catch (Exception e1) {
							e1.printStackTrace();
							return;
						}

						testt.SimpleFileServer.main(null);
						System.out.println("\nDB Updated\n");
						 controller.SetLog(UserNameGlobal + ": " + "Updated File - " + fname,"UpdateFile");
						 client.sendToClient("UpdateFile2");}
					} catch (SQLException e) {e.printStackTrace(); }
		  }  
		  
		  
		  if(ne.getTask().equals("UpdateFile3")){//update existing file  - same function
			  System.out.println("UpdateFile");
			  
			   try 
				{
				   String fname = ((File)ne.getObject()).getFName();
				   String UserNameGlobal= ne.getMess();
				   UserNameGlobal= UserNameGlobal.substring(0,1).toUpperCase() +UserNameGlobal.substring(1);

					 ResultSet result = null; 					 
					 result = stmt.executeQuery("SELECT count(*) FROM files WHERE fname = '"+fname+"';"); // Check if filename already exists
					 result.next();
					 if ( result.getInt(1) == 0) { //If not exists, Add to DB
					 
						   System.out.println("\nFilename Not Exists...\n");
						   testt.SimpleFileServer.main(null);
						   client.sendToClient("UpdateFileFalse3");
						   return; 
					 } else { //If  exists, Show error message

						   try {
							String sourceFilePath = "C:/MyBox/"+ fname;
					    	java.io.File fileDelete = new  java.io.File(sourceFilePath);

					    	 fileDelete.delete();
						} catch (Exception e1) {
							e1.printStackTrace();
							return;
						}
						   
						   testt.SimpleFileServer.main(null); 
						   System.out.println("\nDB Updated\n");
						   controller.SetLog(UserNameGlobal + ": " + "Updated File - " + fname,"UpdateFile");
						   client.sendToClient("UpdateFile3");}

				} catch (SQLException e) {e.printStackTrace(); }
  
		  }
		  
		  
		  
		  
		  if(ne.getTask().equals("DeleteFolder")){//Delete folder
			  String UserNameGlobal= ne.getMess();
			  UserNameGlobal= UserNameGlobal.substring(0,1).toUpperCase() +UserNameGlobal.substring(1);
			  System.out.println("DeleteFolder");
			  Folders fold= (Folders) ne.getObject();
			  String foname=fold.getFolderName();
			  String flocation = fold.getFolderLocation();
			  String superloc= flocation+foname+"/";
			  stmt.executeUpdate("DELETE FROM folders WHERE foname='"+foname+"' AND location='"+flocation+"';"); 
			  ResultSet result = null; 	
			  result = stmt.executeQuery("Select fname from files where location='"+superloc+"';");
			  while(result.next())
			  {
				  stmt.executeUpdate("DELETE FROM filesinfolders WHERE finame='"+result.getString(1)+"';");
				  stmt.executeUpdate("DELETE FROM filesinbox WHERE filename='"+result.getString(1)+"';");
				  stmt.executeUpdate("DELETE FROM files WHERE fname='"+result.getString(1)+"';");
				  stmt.executeUpdate("DELETE FROM filesingroups WHERE ffname='"+result.getString(1)+"';");
			  }
			    System.out.println("\nFolder Deleted\n");	
				  controller.SetLog(UserNameGlobal + ": " + "Deleted Folder - " + foname,"DeleteFolder");
				  client.sendToClient("DeleteFolder");
		  }
		  
		  
		  
		  if(ne.getTask().equals("AddFolder")){//Add folder
			  String UserNameGlobal= ne.getMess();
			  UserNameGlobal= UserNameGlobal.substring(0,1).toUpperCase() +UserNameGlobal.substring(1);
			  System.out.println("AddFolder");
			  
			  Folders fold= (Folders)ne.getObject();
			  
			  String foname=fold.getFolderName();
			  String loc=fold.getFolderLocation();
			  
			  ResultSet res = stmt.executeQuery("SELECT count(*) FROM folders WHERE foname = '"+foname+"';"); 
			  res.next();
				 if ( res.getInt(1) == 0) { //If not exists  
				

					 stmt.executeUpdate("INSERT INTO `folders` (`foname`,`location`,`owner`) VALUES ('"+foname+"','"+loc+"','"+UserNameGlobal+"')");
					 System.out.println("\nFolder Created\n");	
			  
					 controller.SetLog(UserNameGlobal + ": " + "Created Folder - " + foname,"AddFolder");
					 client.sendToClient("AddFolder");
				 }
				 else
					 client.sendToClient("AddFolderFalse");
				 		return;
		  }
		  
		  
		  if(ne.getTask().equals("RenameFolder")){//Rename Folder
			  String UserNameGlobal= ne.getMess();
			  UserNameGlobal= UserNameGlobal.substring(0,1).toUpperCase() +UserNameGlobal.substring(1);
			  System.out.println("RenameFolder");
			  Folders fold= (Folders) ne.getObject();
			  String foname=fold.getFolderName();
			  String loc=fold.getFolderLocation();

			  String[] names= foname.split("!!");
			  String newname=names[1];
			  String oldname=names[0];
			  String oldsuperloc=loc+oldname+"/";
			  String newsuperloc=loc+newname+"/";
			  
			  ResultSet res = stmt.executeQuery("SELECT count(*) FROM folders WHERE foname = '"+newname+"';"); 
			  res.next();
				 if ( res.getInt(1) == 0) { //If not exists  

					 stmt.executeUpdate("update folders set foname = '"+newname+"' where foname = '"+oldname+"' AND location='"+loc+"';");
					 stmt.executeUpdate("update files set location ='"+newsuperloc+"'  where location = '"+oldsuperloc+"';");
					 stmt.executeUpdate("update filesinfolders set loca ='"+newsuperloc+"'  where loca = '"+oldsuperloc+"';");
			  
		 
					 System.out.println("\nFolder Name Changed\n");	
					 controller.SetLog(UserNameGlobal + ": " + "Folder renamed! - " + foname,"RenameFolder");
					 client.sendToClient("RenameFolder");
				 }
				 else{
					 client.sendToClient("RenameFolderFalse");
				 	return;
				 }
		  }
		  
		  
		  
		  if(ne.getTask().equals("ChangeFolderLocation")){//Change Folder location
			  String UserNameGlobal= ne.getMess();
			  UserNameGlobal= UserNameGlobal.substring(0,1).toUpperCase() +UserNameGlobal.substring(1);
			  System.out.println("ChangeFolderLocation");
			  Folders fold= (Folders) ne.getObject();
			  String foname=fold.getFolderName();
			  String loc=fold.getFolderLocation();
			  
			  String[] locs= loc.split("!!");
			  String newloc=locs[1];
			  String oldloc=locs[0];
			  String oldsuperloc=oldloc+foname+"/";
			  String newsuperloc=newloc+foname+"/";
			  stmt.executeUpdate("update folders set location = '"+newloc+"' where foname = '"+foname+"' AND location='"+oldloc+"';");
			  stmt.executeUpdate("update files set location ='"+newsuperloc+"'  where location = '"+oldsuperloc+"';");
			  stmt.executeUpdate("update filesinfolders set loca ='"+newsuperloc+"'  where loca = '"+oldsuperloc+"';");
			  
		 
			  System.out.println("\nFolder Location Changed\n");	
			  controller.SetLog(UserNameGlobal + ": " + "Folder relocated! - " + foname,"RelocateFolder");
			  client.sendToClient("ChangeFolderLocation");
		  }
		  

		  
		  if(ne.getTask().startsWith("ChangePassword")){//Change Password
			  System.out.println("ChangePasswod");
			  String newpass = ((User)ne.getObject()).getPassword();
				 String userC= ((User)ne.getObject()).getUserName();
				 String oldpass = ne.getTask().substring(15);

				 ResultSet res = stmt.executeQuery("SELECT count(*) FROM users WHERE uname = '"+userC+"';"); 
				 res.next();
				 if ( res.getInt(1) == 0) { //If not exists  
						 client.sendToClient("NoUser");}
				 else{
					 ResultSet result = stmt.executeQuery("SELECT COUNT(*) FROM users WHERE uname= '"+userC+"' AND password='"+oldpass+"';");
					 result.next();
					 if ( result.getInt(1) == 0){ //If not exists   
					      client.sendToClient("UserOrPassIncorrectChange");}
					 else { //If  exists
						 	PreparedStatement ps;
						 	ps = conn.prepareStatement("update users set password = ? where uname = '"+userC+"';");
						  	ps.setString(1, newpass);
						  	ps.executeUpdate();
						  	System.out.println("\nDB Updated\n");	
					 }
					  controller.SetLog(userC + ": Changed Password","PasswordChange");
					  client.sendToClient("PasswordChange");
					 }
			 }

		  
		  
		  if(ne.getTask().equals("GetFiles")){//Get files belongs to the user
			  System.out.println("GetFiles");
			  String UserNameGlobal= ne.getMess();
			  UserNameGlobal= UserNameGlobal.substring(0,1).toUpperCase() +UserNameGlobal.substring(1);
			  ArrayList<File> file = new ArrayList<File>();
			  ResultSet result = null; 					 
			  result = stmt.executeQuery("SELECT * FROM files WHERE owner = '"+UserNameGlobal+"';"); 
				 while(result.next()){
					 file.add(new File (result.getString(1),result.getString(2),result.getString(3),result.getString(4),result.getString(5)));
				 }
				 String SendFiles="SendFiles";
				 for (int i = 0; i < file.size(); i++) 
						SendFiles=SendFiles+" "+(file.get(i)).getFName();	
				 client.sendToClient(SendFiles);
		  }
	  
			



		  if(ne.getTask().equals("GetFiles2")){//Get files belongs to the user - same function
			  String UserNameGlobal= ne.getMess();
			  UserNameGlobal= UserNameGlobal.substring(0,1).toUpperCase() +UserNameGlobal.substring(1);
			  System.out.println("GetFiles");
			  ArrayList<File> file = new ArrayList<File>();
			  ResultSet result = null; 					 
			  result = stmt.executeQuery("SELECT * FROM files WHERE owner = '"+UserNameGlobal+"';"); 
				 while(result.next()){
					 file.add(new File (result.getString(1),result.getString(2),result.getString(3),result.getString(4),result.getString(5)));
				 }
				String SendFiles="FileInfoEdit";
				for (int i = 0; i < file.size(); i++) 
					SendFiles=SendFiles+" "+(file.get(i)).getFName();	
				 client.sendToClient(SendFiles);  
		  }


		  
		  
		  
			if(ne.getTask().equals("DeleteFile")){//Delete file - from DB and phisically from server
				  String UserNameGlobal= ne.getMess();
				  UserNameGlobal= UserNameGlobal.substring(0,1).toUpperCase() +UserNameGlobal.substring(1);
				  System.out.println("DeleteFile");
				  String fname = ((File)ne.getObject()).getFName();
				  String dfname = fname+"1";

				  java.io.File file = new  java.io.File("c:/MyBox/"+fname);
				  if (file.exists()){
					  file.delete();}

				  java.io.File file1 = new  java.io.File("c:/MyBox/"+dfname);
				  if (file1.exists()){
					  file1.delete();}
  
				   try {
						String sourceFilePath = "C:/MyBox/"+ fname;
				    	  java.io.File fileDelete = new  java.io.File(sourceFilePath);

				    	  fileDelete.delete();
					} catch (Exception e1) {
						e1.printStackTrace();
						return;
					}
					   
				  
				    stmt.executeUpdate("DELETE FROM filesinfolders WHERE finame='"+fname+"';");
				       
					stmt.executeUpdate("DELETE FROM filesingroups WHERE ffname='"+fname+"';");//Delete file from filesingroups

					stmt.executeUpdate("DELETE FROM FilesInBox WHERE filename='"+fname+"';");
				
					stmt.executeUpdate("DELETE FROM `files` " + " WHERE fname = '"+fname+"'");

					System.out.println("\nFile Deleted\n");	
					controller.SetLog(UserNameGlobal + ": " + "Deleted File - " + fname,"DeleteFile");
					client.sendToClient("DeleteFile");	  
			  }
			  
			  
	  
			  if(ne.getTask().equals("DeleteFileAdmin")){//Delete file - from DB and phisically from server - also sends notification email (Admin)
				  String UserNameGlobal= ne.getMess();
				  UserNameGlobal= UserNameGlobal.substring(0,1).toUpperCase() +UserNameGlobal.substring(1);
				  System.out.println("DeleteFileAdmin");
				  String fname = ((File)ne.getObject()).getFName();
				  String dfname = fname+"1";
				
				  java.io.File file = new  java.io.File("C:/MyBox/"+fname);
				  if (file.exists()){
					  file.delete();}

				  java.io.File file1 = new  java.io.File("C:/MyBox/"+dfname);
				  if (file1.exists()){
					  file1.delete();}
				  
				  
				  try {
						String sourceFilePath = "C:/MyBox/"+fname;
				    	  java.io.File fileDelete = new  java.io.File(sourceFilePath);

				    	  fileDelete.delete();
					} catch (Exception e1) {
						e1.printStackTrace();
						return;
					}
				  
				  
					 ArrayList<String> UsersToSend = new ArrayList<String>();
					 ResultSet rresult = null; 					 
					 rresult = stmt.executeQuery("select ubox from filesinbox where filename='"+fname+"';"); 
					 while(rresult.next())
							UsersToSend.add(rresult.getString(1));
						  
					 for (int i=0;i<UsersToSend.size();i++) {
							final int j=i;
							 new Thread() {
								    @Override
								    public void run() {
								    	 SendNotificationEmail(UsersToSend.get(j),fname);
								    }
								}.start();
   
					 }

				     stmt.executeUpdate("DELETE FROM filesinfolders WHERE finame='"+fname+"';");
				       
				     stmt.executeUpdate("DELETE FROM filesingroups WHERE ffname='"+fname+"';");//Delete file from filesingroups

				     stmt.executeUpdate("DELETE FROM FilesInBox WHERE filename='"+fname+"';");
						
				     stmt.executeUpdate("DELETE FROM `files` " + " WHERE fname = '"+fname+"'");

				     System.out.println("\nFile Deleted\n");	
					 controller.SetLog(UserNameGlobal + ": " + "Deleted File - " + fname,"DeleteFile");
					 client.sendToClient("AdminDeleteFile");
			  }
				  
			  
			  
			  
			  
			  
			  
			  
			  if(ne.getTask().equals("GetFilesInRootFolder")){ // Get the files and folders in the root folder
				  String UserNameGlobal= ne.getMess();
				  UserNameGlobal= UserNameGlobal.substring(0,1).toUpperCase() +UserNameGlobal.substring(1);
	
				  ArrayList<File> FilesRoot = new ArrayList<File>();
				  ResultSet resF = stmt.executeQuery("select filename from filesinfolders,filesinbox,files where  (ubox='"+UserNameGlobal+"'  AND loca='/"+UserNameGlobal+"/'  AND filename=finame  ) AND (owner='"+UserNameGlobal+"' AND location='/"+UserNameGlobal+"/' ) group by filename;");
			
				  while(resF.next())
					  FilesRoot.add(new File (resF.getString(1)," "," ", " ", " "));
				  
				  String GetFiles2Root="GetFiles2Root ";
				  for (int i = 0; i < FilesRoot.size(); i++) 
					  GetFiles2Root=GetFiles2Root+(FilesRoot.get(i)).getFName()+" ";	

				  ArrayList<Folders> FoldersRoot = new ArrayList<Folders>();
				   ResultSet resFo = stmt.executeQuery("select foname from folders where location='/"+UserNameGlobal+"/';");
				   
				   while(resFo.next())
					   FoldersRoot.add(new Folders (" ",resFo.getString(1)," "));
				   
					String Get2FoldersRoot="Get2FoldersRoot ";
					for (int i = 0; i < FoldersRoot.size(); i++) 
						Get2FoldersRoot=Get2FoldersRoot+(FoldersRoot.get(i)).getFolderName()+" ";	
	

					String RootFoldersAndFiles=GetFiles2Root+"@@"+Get2FoldersRoot;
					client.sendToClient(RootFoldersAndFiles); 
	
	  }
			  
	  
			  
			  
		  if(ne.getTask().equals("GetGroups")){ //Get groups
			  String UserNameGlobal= ne.getMess();
			  UserNameGlobal= UserNameGlobal.substring(0,1).toUpperCase() +UserNameGlobal.substring(1);
			  ArrayList<Group> group = new ArrayList<Group>();
			  System.out.println("GetGroups"); 
			  ResultSet res;		  
			  res = stmt.executeQuery("select UsersInGroups.gname,groups.description from UsersInGroups,users,groups where UsersInGroups.uname=users.uname AND UsersInGroups.uname='"+UserNameGlobal+"' AND UsersInGroups.gname=groups.gname;"); // Check if filename  exists
				 while(res.next()){
					 group.add(new Group (res.getString(1),res.getString(2)));
				 }
					 
				 String GetGroups="GetGroups";
					for (int i = 0; i < group.size(); i++) 
						GetGroups=GetGroups+"@@"+(group.get(i)).getGroupName()+" - "+(group.get(i)).getDescription();	
 
						 client.sendToClient(GetGroups);
			}
			  
		  
	
 
		  if(ne.getTask().equals("GetFilesAndFoledersInFolder")){ // Get files and folders inside a folder
			   String UserNameGlobal= ne.getMess();
			  UserNameGlobal= UserNameGlobal.substring(0,1).toUpperCase() +UserNameGlobal.substring(1);
			  Folders FolName=(Folders) ne.getObject();
			  String FolderName=FolName.getFolderName(); 
			  ArrayList<File> FilesNotRoot = new ArrayList<File>();
			   ResultSet resF = stmt.executeQuery("select filename from filesinfolders,filesinbox,files where  (ubox='"+UserNameGlobal+"'  AND loca LIKE '/%"+FolderName+"/'  AND filename=finame  ) AND (owner='"+UserNameGlobal+"' AND location LIKE '%/"+FolderName+"/' ) Group By filename;");
			   while(resF.next())
				   FilesNotRoot.add(new File (resF.getString(1)," "," ", " ", " "));
			 	String GetFilesNotRoot="GetFilesNotRoot ";
				for (int i = 0; i < FilesNotRoot.size(); i++) 
					GetFilesNotRoot=GetFilesNotRoot+(FilesNotRoot.get(i)).getFName()+" ";	
			   ArrayList<Folders> FoldersNotRoot = new ArrayList<Folders>();
			   ResultSet resFo = stmt.executeQuery("select foname from folders where location='/"+UserNameGlobal+"/"+FolderName+"/';");
			   while(resFo.next()){
				   FoldersNotRoot.add(new Folders (" ",resFo.getString(1)," "));
			   }
			 	String GetFoldesrNotRoot="GetFoldesrNotRoot ";
				for (int i = 0; i < FoldersNotRoot.size(); i++) 
					GetFoldesrNotRoot=GetFoldesrNotRoot+(FoldersNotRoot.get(i)).getFolderName()+" ";	
		
				String FoldersAndFiles=GetFilesNotRoot+"@@"+GetFoldesrNotRoot;
			  client.sendToClient(FoldersAndFiles); 

		  
		  }
		  
		  
		  
		  
		  if(ne.getTask().equals("GetFilesForAdmin")){ // get all files for Admin
			  System.out.println("GetFilesForAdmin"); 
			  String GetFilesAdmin="GetFilesForAdmin ";
			  ArrayList<File> FilesAdmin = new ArrayList<File>();
			   ResultSet resF = stmt.executeQuery("select fname from files;");
			   while(resF.next())
				   FilesAdmin.add(new File (resF.getString(1)," "," ", " ", " "));
				for (int i = 0; i < FilesAdmin.size(); i++) 
					GetFilesAdmin=GetFilesAdmin+(FilesAdmin.get(i)).getFName()+" ";	
				
				client.sendToClient(GetFilesAdmin); 
		  }
		  
		  
		  
		  
		  if(ne.getTask().equals("GetFilesForAdmin2")){// get all files for Admin - differenet controller
			  System.out.println("GetFilesForAdmin2"); 
	
			  String GetFilesAdmin="2GetFilesForAdmin ";
			  ArrayList<File> FilesAdmin = new ArrayList<File>();
			   ResultSet resF = stmt.executeQuery("select fname from files;");
			   while(resF.next())
				   FilesAdmin.add(new File (resF.getString(1)," "," ", " ", " "));
				for (int i = 0; i < FilesAdmin.size(); i++) 
					GetFilesAdmin=GetFilesAdmin+(FilesAdmin.get(i)).getFName()+" ";	
				client.sendToClient(GetFilesAdmin); 
		  }
		  
		  
		  
		  if(ne.getTask().equals("GetFilesForAdmin3")){// get all files for Admin - differenet controller
			  System.out.println("GetFilesForAdmin3"); 
			  String GetFilesAdmin="3GetFilesForAdmin ";
			  ArrayList<File> FilesAdmin = new ArrayList<File>();
			   ResultSet resF = stmt.executeQuery("select fname from files;");
			   while(resF.next())
				   FilesAdmin.add(new File (resF.getString(1)," "," ", " ", " "));
				for (int i = 0; i < FilesAdmin.size(); i++) 
					GetFilesAdmin=GetFilesAdmin+(FilesAdmin.get(i)).getFName()+" ";	
				
				client.sendToClient(GetFilesAdmin); 
		  }
		  
		  
		  
		  if(ne.getTask().equals("SendMail3Times")){ //Send email to admin if a user tried to log in 3 times
			  System.out.println("SendMail3Times"); 
			  String euser=ne.getMess();
			  server.GoogleMail.main(1,euser,"",conn);
			  return;
		  }
		  
		  
		  
		  
		  if(ne.getTask().equals("ForgotPassword")){ // Sends the user email with his password (forgot password)
			  System.out.println("ForgotPassword"); 
			  String euser=ne.getMess();
			  if(server.GoogleMail.main(2,euser,"",conn)==1)
				  client.sendToClient("ForgotPassword");
			  else
				  client.sendToClient("ForgotPasswordFalse");		
		  }
		  
		  
		  
		  
		  if(ne.getTask().equals("Register")){//Register new user
			  System.out.println("Register"); 
			  
			 String first = ((User) ne.getObject()).getFirstName();
			 String last = ((User) ne.getObject()).getLastName();
			 String uname = ((User) ne.getObject()).getUserName();
			 String password = ((User) ne.getObject()).getPassword();
			 String email = ((User) ne.getObject()).getEmail();
			 int flag=7;
			 String Send="Register "+flag;
 
			 ResultSet result = null; 
				
			 result = stmt.executeQuery("SELECT count(*) FROM users WHERE uname = '"+uname+"';"); 
			 result.next();
			 if ( result.getInt(1) != 0){  //If exists  
			    flag=0; // UserName Exists 
			    Send="Register "+flag;	
			 	client.sendToClient(Send); 
			 	return;	
			 }
			 
			 result = stmt.executeQuery("SELECT count(*) FROM users WHERE email = '"+email+"';"); 
			 result.next();
			 if ( result.getInt(1) != 0) { //If exists  
				 flag= 3; // Email Exists }
				 Send="Register "+flag;	
				 client.sendToClient(Send); 
				 return;
				 }
			 
			 if(email.contains("@")==false){
				 flag=4;
				 Send="Register "+flag;	
				 client.sendToClient(Send); 
				 return;
			  }

			  if(password.length()<6){
				
				 flag=1;// Password too short  
				 Send="Register "+flag;	
				 client.sendToClient(Send); 
				 return;
			  }
			  
			  
			  else {
	
				  stmt.executeUpdate(
							"INSERT INTO `users` " + " VALUES ('"+first+"','"+last+"', '"+uname+"', '"+password+"', '"+email+"', 'NONE' )");
				  System.out.println("\nDB Updated\n");
				  flag=2; // Updated
				  new Thread() {
							 @Override
							public void run() {
							SendRegisterEmail(uname,"");
							}
				  }.start();
	 
				 controller.SetLog(uname + ": Is now registered","Register");
			  } 

				Send="Register "+flag;
				client.sendToClient(Send); 
		  
		  }
		  
		  


         
  } catch (Exception x) {
      JOptionPane.showMessageDialog(null, "Unable to connect to the database", "Connection error", JOptionPane.ERROR_MESSAGE);
  }//outer try catch closed
	  
  
  }
  
  
  
  /**
   * This sends a notification email to the users 
   * @param uname is the user name
   * @param fname is the file name
   */
  public void SendNotificationEmail(String uname,String fname){
	  server.GoogleMail.main(4,uname,fname,conn);	  
  }
  
  /**
   * 
   * This sends a welcome email after the registeration
   * @param uname is the user name
   * @param fname not in use
   */
  public void SendRegisterEmail(String uname,String fname){
	  server.GoogleMail.main(3,uname,fname,conn);  
  }
  
  /**
   * 
   * This sends an email that notify the users about their requests status
   * @param uname is the user name
   * @param gname is the group name
   */
  public void SendApprovesEmail(String uname,String gname){
	  server.GoogleMail.main(5,uname,gname,conn);
  }
  
  public void SendRejectEmail(String uname,String gname){
	  server.GoogleMail.main(6,uname,gname,conn);
  }
  

  public Connection getConn() {
	return conn;
}
  
 
 public void setConn(Connection conn) {
	this.conn = conn;
}
 public void setController(ServerController controller) {
		this.controller = controller;
	}

  
/**
   * This method overrides the one in the superclass.  Called
   * when the server starts listening for connections.
   */
  protected void serverStarted()
  {
    System.out.println
      ("Server listening for connections on port " + getPort());
  }
  
  /**
   * This method overrides the one in the superclass.  Called
   * when the server stops listening for connections.
   */
  protected void serverStopped()
  {
    System.out.println
      ("Server has stopped listening for connections.");
  }
  

}
//End of EchoServer class   
