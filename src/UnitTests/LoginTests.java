package UnitTests;


import java.util.ArrayList;

import junit.framework.TestCase;

import view.LoginGUI;
import models.LoginMod;
import models.User;
import controllers.LoginController;

public class LoginTests extends TestCase{

	public ArrayList<User> userTest = new ArrayList<User>();
	
	public LoginController LC = null;	
	
	public void setUp() throws Exception {
		
		LoginGUI logView = new LoginGUI();
		LoginMod logModel = new LoginMod();
		
		LC = new LoginController(logView,logModel);
		userTest.add(new User("Alex", "Zakashansky", "Zakash", "ale4ko", "zakash2@gmail.com", null));
		userTest.add(new User("Tal", "Akerman", "Admin", "akermania", "akermania2@gmail.com", null));
		userTest.add(new User("Netanel", "Abakasis", "NatiBeks", "na2015", "natibeks2@gmail.com", null));
		LC.setUser(userTest);
	}

	

 		public void testuserExist(){
		String userName="Idan";
		//check if the user is exist
		assertFalse(LC.userExist(userName));
		userName="Zakash";
		//check if the user is exist
		assertTrue(LC.userExist(userName));
	}
 
	public void testPass(){
		String userName="Zakash";
		String password="11111";
		//check if the password correct
		assertFalse(LC.checkPassword(userName, password));
		//check if the password is't correct
		password="ale4ko";
		assertTrue(LC.checkPassword(userName, password));
		
		
	}
	
		public void testIfAdmin(){
	String userName="Zakash";
	//check if the user is exist
	assertFalse(LC.checkIfAdmin(userName));
	userName="Admin";
	//check if the user is exist
	assertTrue(LC.checkIfAdmin(userName));
		}
	

}
