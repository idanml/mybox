package models;

public class Request extends AbstractModel {
	
	private String gname;

	private int type;

	private String uname;

	
	public Request(String gname, int type, String uname) {
		super();
		this.gname = gname;
		this.type = type;
		this.uname = uname;

	}
	
	public String getGroupName() {
		return gname;
	}

	public void setGroupName(String gname) {
		this.gname = gname;
	}

	public int getRequestType() {
		return type;
	}

	public void setRequestType(int type) {
		this.type = type;
	}

	public String getUserName() {
		return uname;
	}

	public void setUserName(String uname) {
		this.uname = uname;
	}


	
	
}


	
