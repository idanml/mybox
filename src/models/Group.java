package models;

import java.util.ArrayList;



public class Group extends AbstractModel {
	
	private String groupName;
	private String description;

	
	public Group(String groupName,String description) {
		super();
		this.groupName = groupName;
		this.description = description;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	

}
