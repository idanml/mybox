package models;

import java.util.ArrayList;

public class Folders extends AbstractModel{
	private String FolderOwner;
	private String FolderName;
	private String FolderLocation;

	
	public Folders(String FolderOwner,String FolderName,String FolderLocation) {
		super();
		this.FolderOwner = FolderOwner;
		this.FolderName = FolderName;
		this.FolderLocation = FolderLocation;
	}

	public String getFolderName() {
		return FolderName;
	}

	public void setFolderName(String FolderName) {
		this.FolderName = FolderName;
	}

	public String getFolderOwner() {
		return FolderOwner;
	}

	public void setFolderOwner(String FolderOwner) {
		this.FolderOwner = FolderOwner;
	}
	
	public String getFolderLocation() {
		return FolderLocation;
	}

	public void setFolderLocation(String FolderLocation) {
		this.FolderLocation = FolderLocation;
	}

}
