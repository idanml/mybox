package models;

import java.util.ArrayList;

public class File extends AbstractModel {
	
	private String desc;
	private String uidOwner;
	private String name;
	private String dest;
	private String path;
	private String permission;

	public File(String name, String path,String uidOwner,String desc,
			String permission) {
		super();
		this.uidOwner = uidOwner;
		this.name = name;
		this.dest = dest;
		this.path = path;
		this.permission = permission;
		this.desc= desc ;
	}
	
	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	
	public String getUidOwner() {
		return uidOwner;
	}

	public void setUidOwner(String uidOwner) {
		this.uidOwner = uidOwner;
	}

	public String getFName() {
		return name;
	}

	public void setFName(String name) {
		this.name = name;
	}

	public String getDest() {
		return dest;
	}

	public void setDest(String dest) {
		this.dest = dest;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}


	public String getPermission() {
		return permission;
	}

	public void setPermission(String permission) {
		this.path = permission;
	}


	
}
