import java.io.IOException;
import client.MyBoxApp;
import client.MyBoxClient;
import controllers.*;
import models.*;
import view.*;
import fit.ActionFixture;

/**
 * Join group fit testing class.
 *
 */
public class JoinGroupTest extends ActionFixture {

	private JoinGroupsController jgc;
	private User user;
	private JoinGroupsGUI jgg;
	
	/**
	 * Start method for the test.
	 * Creating a connected client.
	 */
	
	public void startJoinTest(){
		try {
			MyBoxApp.clien = new MyBoxClient("localhost",5555);
		} catch (IOException e) {
			System.out.println("Faild to connect to server!");
			e.printStackTrace();
		}
		try {
			
		    Thread.sleep(500);
		} catch (InterruptedException e) {
		    e.printStackTrace();
		}

		user = new User(null,null,"Netanel","12345",null,null);
		jgg= new JoinGroupsGUI("SendUnregGroups Math");
		jgg.setVisible(false);
		jgc = new JoinGroupsController(user,null,jgg);
		
	}
	
	public void selectGroup(int idx){
		jgg.getGroupsChoice().select(idx);
	}
	
	public void addGroup(String group){
		jgg.getGroupsChoice().add(group);
	}
	
	public void emptyGroups(){ 
		jgg.getGroupsChoice().removeAll();
	}

	public void joinGroup(){
		
		jgg.joinButtonClicked();
		try {
		    Thread.sleep(500);
		} catch (InterruptedException e) {
		    e.printStackTrace();
		}
	}
	
	public boolean checkJoin(){
		return jgc.isFlag();
	}

	
}
