import java.io.IOException;

import models.User;
import view.AddFileGUI;
import client.MyBoxApp;
import client.MyBoxClient;
import controllers.AddFileController;
import fit.ActionFixture;

/**
 * Add file case test file.
 *
 */
public class AddFileTest extends ActionFixture {

	private AddFileController afc;
	private User user;
	private AddFileGUI afg;

	/**
	 * Create a connected client, also addfile interface to work with.
	 */
	public void startAddFileTest() {
		try {
			MyBoxApp.clien = new MyBoxClient("localhost",5555);
		} catch (IOException e) {
			System.out.println("Faild to connect to server!");
			e.printStackTrace();
		}
		
		try {
			
		    Thread.sleep(500);
		} catch (InterruptedException e) {
		    e.printStackTrace();
		}
		user = new User(null,null,"Netanel","12345",null,null);
		afg= new AddFileGUI("SendFolders /Netanel/Net/");
		afg.setVisible(false);
		
		afc = new AddFileController(user,null,afg);
	}

	public void fileName(String name){
		afc.setFname(name);
	}
	
	public void fileSrcPath(String path){
		afc.setLoc(path);
	}
	
	public void fileDestPath(String path){
		afg.getFiles().add(path);
		afg.getFiles().select(path);
	}
	
	public void fileDestPathSelect(int idx){
		afg.getFiles().select(idx);
	}

	public void fileDesc(String desc){
		afg.setFileDescriptionField(desc);
	}
	
	public void addFile(){
		
		afg.clickAddButton();
		try {
		    Thread.sleep(500);
		} catch (InterruptedException e) {
		    e.printStackTrace();
		}
	}
	
	public boolean checkFlag(){
		return afc.getFlag();
	}
	

}
